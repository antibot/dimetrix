jQuery(function($) {

    function proportion(from, to) {
        var ratio = proportionRatio(from, to, true);

        return [from[0] / ratio, from[1] / ratio];
    }

    function proportionRatio(from, to, fitInside) {
        return fitInside ? Math.max(from[0] / to[0], from[1] / to[1]) : Math.min(from[0] / to[0], from[1] / to[1]);
    }

// ---

    var DRAW = true;
    var DRAW_OFFSET_X = 0;
    var DRAW_OFFSET_Y = 300;

// ---

    var TYPE_NONE = 0;
    var TYPE_TEXT = 1;
    var TYPE_IMAGE = 2;

    var TYPE = TYPE_IMAGE;

// --- Text

    var FONT = "Raleway";
    var SIZE = 6;
    var TEXT = "D I M E T R I X";
    var COLOR = "black";

// --- Image

    var IMAGE = "images/14.ico";
    var IMAGE_MAX_WIDTH = 26;
    var IMAGE_MAX_HEIGHT = 26;

// ---

    var SCALE = 10;

    var width = 0;
    var height = 0;

    var stage = new Kinetic.Stage({
        container: "container",
        width: 500,
        height: 500
    });

    var layer = new Kinetic.Layer();

// --- Text

    if (TYPE === TYPE_TEXT) {
        var label = new Kinetic.Text({
            x: 0,
            y: 0,
            text: TEXT,
            fontSize: SIZE,
            fontFamily: FONT,
            fill: COLOR
        });

        width = label.getWidth();
        height = label.getHeight();

        stage.setWidth(width);
        stage.setHeight(height);

        layer.add(label);

        stage.add(layer);

        build();
    } else if (TYPE === TYPE_IMAGE) {
        var imageObject = new Image();
        imageObject.src = IMAGE;
        imageObject.onload = function() {
            var size = proportion([imageObject.width, imageObject.height], [IMAGE_MAX_WIDTH, IMAGE_MAX_HEIGHT]);

            console.log("size", size);

            var image = new Kinetic.Image({
                x: 0,
                y: 0,
                image: imageObject,
                width: size[0],
                height: size[1]
            });

            width = image.getWidth();
            height = image.getHeight();

            stage.setWidth(width);
            stage.setHeight(height);

            layer.add(image);

            stage.add(layer);

            build();
        };
    } else {

    }

// ---

    function build() {
        var canvas = document.getElementsByTagName("canvas")[0];
        var context = canvas.getContext("2d");

        var i, j;

        var minX = Number.MAX_VALUE, minY = Number.MAX_VALUE, maxX = 0, maxY = 0;

        var points = [];

        for (i = 0; i < width; i++) {
            for (j = 0; j < height; j++) {
                var pixel = context.getImageData(i, j, 1, 1).data;

                var r = pixel[0];
                var g = pixel[1];
                var b = pixel[2];
                var a = pixel[3];

                if (pixel) {
                    if ((r < 200 || g < 200 || b < 200) && a !== 0) {
                        var pointX = i * SCALE;
                        var pointY = j * SCALE;

                        minX = Math.min(minX, pointX);
                        minY = Math.min(minY, pointY);

                        points.push([pointX, pointY]);
                    }
                }
            }
        }

        var args = [];

        for (var i = 0; i < points.length; i++) {
            var point = points[i];
            var newX = point[0] - minX;
            var newY = point[1] - minY;

            maxX = Math.max(maxX, newX);
            maxY = Math.max(maxY, newY);

            if (DRAW) {
                $("<div>").width(1).height(1).css({"background": "#000", "position": "absolute", "left": (newX + DRAW_OFFSET_X), "top": (newY + DRAW_OFFSET_Y)}).appendTo("body");
            }

            args.push("[" + newX + ", " + newY + "]");
        }

        var result = "var points = [" + args.join(", ") + "];";

        console.log(result);

        console.log("size: maxX = " + maxX + " maxY = " + maxY + " points: " + args.length);
    }

});
