$.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));

function dec2hex(dec) {
    dec = Math.floor(dec);

    return dec > 15 ? dec.toString(16) : ("0" + dec.toString(16));
}
function decs2hex(r, g, b) {
    return '#' + dec2hex(r) + dec2hex(g) + dec2hex(b);
}

var FPS = function(debug) {
    var self = this;

    var startTime = Date.now();
    var currentTime = startTime;
    var frames = 0;
    var lastFrames = 0;

    self.exec = function() {
        frames++;
        currentTime = Date.now();
        if (currentTime - startTime >= 1000) {
            console.log('fps: ' + frames);

            debug.innerHTML = frames;

            lastFrames = frames;
            frames = 0;
            startTime = currentTime;
        }

        // ---

        return lastFrames;
    };

    return self;
};

(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for (var i = 0; i < vendors.length && !window.requestAnimationFrame; ++i) {
        window.requestAnimationFrame = window[vendors[i] + 'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[i] + 'CancelAnimationFrame'] || window[vendors[i] + 'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame) {
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() {
                callback(currTime + timeToCall);
            }, timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
    }

    if (!window.cancelAnimationFrame) {
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
    }
}());

function RANDOM(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function IS_NULL_ARRAY(arr) {
    arr = arr || [];

    if (!arr || !arr.length) {
        return true;
    }

    var i = 0, count = arr.length;

    for (; i < count; i++) {
        if (arr[i]) {
            return false;
        }
    }

    return true;
}

function NORMOLIZE_ARRAY(arr) {
    var result = new Float32Array();

    for (var i = 0; i < arr.lenght; i++) {
        if (Math.abs(arr[i]) > 0.01) {
            result.push(arr[i]);
        }
    }

    return arr;
}

function FILL_ARRAY(from, to) {
    var result = [];

    for (var i = from; i < to; i++) {
        result.push(i);
    }

    return result;
}

function GET_POINTS_SIZE(points) {
    points = points || [];

    if (!points.length) {
        return [0, 0];
    }

    var maxX = 0, maxY = 0, minX = Number.MAX_VALUE, minY = Number.MAX_VALUE;

    for (var i = 0; i < points.length; i++) {
        var point = points[i];

        minX = Math.min(minX, point[0]);
        minY = Math.min(minY, point[1]);

        maxX = Math.max(maxX, point[0]);
        maxY = Math.max(maxY, point[1]);
    }

    return [maxX - minX, maxY - minY];
}

function GET_POINTS_WIDTH(points) {
    return GET_POINTS_SIZE(points)[0];
}

function GET_POINTS_HEIGHT(points) {
    return GET_POINTS_SIZE(points)[1];
}
