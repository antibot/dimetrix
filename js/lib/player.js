(function() {

    console.log("Dancer.isSupported()", Dancer.isSupported());

    if (!Dancer.isSupported()) {
        return;
    }

    var dancer = null;
    var index = 0;
    var interval = 0;
    var attempts = 0;

    var AUDIO_FILES = [
        "songs/b29f4397810ad2",
        "songs/fa6b8ca9898146",
        "songs/db0ca193a26597",
        "songs/1",
        "songs/2",
        "songs/3",
        "songs/00f9b8941f1238",
        "songs/103b300b1dbf36",
        "songs/4a8dae03899a35",
        "songs/6",
        "songs/6bc6332560111d",
        "songs/7",
        "songs/9300dc26ce1ad4",
        "songs/_Alesso & Calvin Harris & Theo Hutchcraft - Under Control",
        "songs/_Avicii feat. Aloe Blacc - Wake Me Up",
        "songs/_Capital Cities - Safe And Sound",
        "songs/_Dj Sunnymega - Troublejust",
        "songs/_Jasper Forks - J???aime Le Diable",
        "songs/_John Newman - Love Me Again",
        "songs/_Klingande - Jubel",
        "songs/_Lana Del Rey - Young And Beautiful",
        "songs/a4adb65db6c61d",
        "songs/accd7e43f53c10",
        "songs/f917c2dc3c5f10"
    ];

    // ---

    Dancer.setOptions({
        flashSWF: 'js/dancer/lib/soundmanager2.swf',
        flashJS: 'js/dancer/lib/soundmanager2.js'
    });

    // ---

    function build() {
        attempts = 0;

        clearInterval(interval);

        dancer = new Dancer();
        dancer.loaded = function() {
            console.log("loaded");

            this.play();
        };
        dancer.load({src: AUDIO_FILES[index++ % AUDIO_FILES.length], codecs: ['mp3']}).effects(document.getElementById('graphics'), {});

        Dancer.isSupported() || dancer.loaded();

        console.log("Dancer.isSupported()", Dancer.isSupported(), "dancer.isLoaded()", dancer.isLoaded());

        !dancer.isLoaded() ? dancer.bind('loaded', dancer.loaded) : dancer.loaded();

        window.dancer = dancer;

        interval = setInterval(function() {
            if (dancer.isLoaded() && IS_NULL_ARRAY(dancer.getWaveform())) {
                if (attempts > 10) {
                    console.log("rebuild");

                    clearInterval(interval);

                    build();
                }

                attempts++;
            }
        }, 1000);
    }

    // ---

    build();

    // ---

    $(document).mousewheel(function(event) {
        if (dancer) {
            dancer.setVolume(event.deltaY > 0 ? Math.min(1, dancer.getVolume() + 0.1) : Math.max(0, dancer.getVolume() - 0.1));
        }
    });
})();