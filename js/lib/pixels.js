function GET_PIXELS(count) {
    count = count || 100;

    var pixels = [];

    for (i = 0; i < count; i++) {
        pixels[i] = {
            x: Math.random() * window.innerWidth,
            y: window.innerHeight / 2,
            toX: 0,
            toY: window.innerHeight / 2,
            color: Math.random() * 200 + 55,
            angle: Math.random() * Math.PI * 2,
            rotation: 0,
            rotationStep: 0,
            size: 0,
            toSize: RANDOM(window.POINT_MIN, window.POINT_MAX),
            r: 0,
            g: 0,
            b: 0,
            toR: Math.random() * 255,
            toG: Math.random() * 255,
            toB: Math.random() * 255,
            flightMode: 0,
            speedX: 0,
            speedY: 0
        };

        pixels[i].toX = pixels[i].x;
    }

    return pixels;
}