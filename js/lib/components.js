function GET_COMPONENTS(canvas, context, transitions_base, transitions_images, transitions_waveform, pixels) {
    if (!canvas || !context || !transitions_base || !transitions_images || !transitions_waveform || !pixels) {
        return [];
    }

    var PI2 = Math.PI * 2;

    var Universe = function Pixels() {
        return {
            type: 0,
            mx: 0,
            my: 0,
            impulsX: 0,
            impulsY: 0,
            impulsToX: 0,
            impulsToY: 0,
            startEffectsTime: new Date(),
            startEffectsWaveformTime: new Date(),
            indexesForBaseEffects: [0, 1, 2, 3, 5, 6, 7, 8, 10, 11],
            indexesForImagesEffects: [4, 9],
            indexesForWaveformEffects: FILL_ARRAY(12, 300),
            effectsIndex: 0,
            focusedParticleIndex: null,
            update: function() {
                this.impulsX = this.impulsX + (this.impulsToX - this.impulsX) / 30;
                this.impulsY = this.impulsY + (this.impulsToY - this.impulsY) / 30;

                // move to tox
                for (var i = 0; i < pixels.length; i++) {
                    var p = pixels[i];
                    p.x = p.x + (p.toX - p.x) / 10;
                    p.y = p.y + (p.toY - p.y) / 10;
                    p.size = p.size + (p.toSize - p.size) / 10;

                    p.r = p.r + (p.toR - p.r) / 10;
                    p.g = p.g + (p.toG - p.g) / 10;
                    p.b = p.b + (p.toB - p.b) / 10;
                }

                // update speed
                for (var i = 0; i < pixels.length; i++) {
                    var p = pixels[i];
                    // check for flightmode
                    var a = Math.abs(p.toX - this.mx) * Math.abs(p.toX - this.mx);
                    var b = Math.abs(p.toY - this.my) * Math.abs(p.toY - this.my);
                    var c = Math.sqrt(a + b);

                    if (p.flightMode !== 2) {
                        if (c < 120) {
                            if (p.flightMode === 0) {
                                var alpha = Math.atan2(p.y - this.my, p.x - this.mx) * 180 / Math.PI + Math.random() * 180 - 90;
                                p.degree = alpha;
                                p.degreeSpeed = Math.random() * 1 + 0.5;
                                p.frame = 0;
                            }
                            p.flightMode = 1;
                        } else {
                            p.flightMode = 0;
                        }
                    }

                    // random movement
                    if (p.flightMode === 0) {
                        // change position
                        p.toX += p.speedX;
                        p.toY += p.speedY;

                        // check for bounds
                        if (p.x < 0) {
                            p.x = window.innerWidth;
                            p.toX = window.innerWidth;
                        }
                        if (p.x > window.innerWidth) {
                            p.x = 0;
                            p.toX = 0;
                        }

                        if (p.y < 0) {
                            p.y = window.innerHeight;
                            p.toY = window.innerHeight;
                        }
                        if (p.y > window.innerHeight) {
                            p.y = 0;
                            p.toY = 0;
                        }
                    }

                    // seek mouse
                    if (p.flightMode === 1) {
                        p.toX = this.mx + Math.cos((p.degree + p.frame) % 360 * Math.PI / 180) * c;
                        p.toY = this.my + Math.sin((p.degree + p.frame) % 360 * Math.PI / 180) * c;
                        p.frame += p.degreeSpeed;
                        p.degreeSpeed += 0.01;
                    }

                    if (p.flightMode !== 2) {
                        // add impuls
                        p.toX += Math.floor(this.impulsX * p.size / 30);
                        p.toY += Math.floor(this.impulsY * p.size / 30);
                    }
                }

                // set an choord
                var r1 = Math.floor(Math.random() * pixels.length);
                var r2 = Math.floor(Math.random() * pixels.length);

                if (pixels[r1].flightMode !== 2) {
                    pixels[r1].size = RANDOM(window.FLICKER_MIN, window.FLICKER_MAX);
                }
                if (pixels[r2].flightMode !== 2) {
                    pixels[r2].size = RANDOM(window.FLICKER_MIN, window.FLICKER_MAX);
                }

                var dancer = window.dancer || {};
                var waveform = "getWaveform" in dancer ? (dancer.getWaveform() || []) : [];
                var hasWaveform = !IS_NULL_ARRAY(waveform);

                if (this.indexesForWaveformEffects.indexOf(this.effectsIndex) !== -1 && hasWaveform) {
                    if (new Date().getTime() - this.startEffectsWaveformTime.getTime() > (this.effectsIndex === this.indexesForWaveformEffects[0] ? 2500 : 50)) {
                        this.startEffectsWaveformTime = new Date();

                        if (transitions_waveform[Math.floor(Math.random() * transitions_waveform.length)]()) {
                            this.impulsX = 0;
                            this.impulsY = 0;
                        }

                        this.startEffectsTime = new Date();

                        this.effectsIndex++;
                    }
                } else {
                    if (new Date().getTime() - this.startEffectsTime.getTime() > 2000) {
                        this.startEffectsTime = new Date();

                        this.effectsIndex = (hasWaveform ? this.effectsIndex : ((this.effectsIndex > this.indexesForBaseEffects[this.indexesForBaseEffects.length - 1] && this.effectsIndex > this.indexesForImagesEffects[this.indexesForBaseEffects.length - 1]) ? 0 : this.effectsIndex));

                        this.impulsX = Math.random() * 800 - 400;
                        this.impulsY = -Math.random() * 400;

                        if (this.indexesForBaseEffects.indexOf(this.effectsIndex) !== -1) {
                            if (transitions_base[Math.floor(Math.random() * transitions_base.length)]()) {
                                this.impulsX = 0;
                                this.impulsY = 0;
                            }
                        } else if (this.indexesForImagesEffects.indexOf(this.effectsIndex) !== -1) {
                            if (transitions_images[Math.floor(Math.random() * transitions_images.length)]()) {
                                this.impulsX = 0;
                                this.impulsY = 0;
                            }
                        }

                        this.startEffectsWaveformTime = new Date();

                        this.effectsIndex++;
                    }
                }

                this.effectsIndex = (this.effectsIndex > (this.indexesForBaseEffects.length + this.indexesForImagesEffects.length + this.indexesForWaveformEffects.length) ? 0 : this.effectsIndex);
            },
            draw: function() {
                for (var i = 0; i < pixels.length; i++) {
                    if(i === this.focusedParticleIndex) {
                        continue;
                    }
                    
                    var pixel = pixels[i];

                    var x = pixel.x;
                    var y = pixel.y;

                    var size = pixel.size;
                    var color = decs2hex(pixel.r, pixel.g, pixel.b);

                    //var x = Math.floor(Math.random() * canvas.width);
                    //var y = Math.floor(Math.random() * canvas.width);

                    //var size = 10;
                    //var color = ((1 << 24) * Math.random() | 0).toString(16);

                    switch (this.type) {
                        case 0:
                            context.fillStyle = color;
                            context.beginPath();
                            context.arc(x, y, size, 0, PI2, false);
                            context.closePath();
                            context.fill();
                            break;
                        case 1:
                            context.strokeStyle = color;
                            context.beginPath();
                            context.arc(x, y, size, 0, PI2, true); // Outer circle
                            context.moveTo(x + 2 * size / 3, y);
                            context.arc(x, y, 2 * size / 3, 0, Math.PI, false);
                            context.moveTo(x - size / 6, y - size / 3);
                            context.arc(x - size / 3, y - size / 3, size / 6, 0, PI2, false);  // Left eye
                            context.moveTo(x + size / 2, y - size / 3);
                            context.arc(x + size / 3, y - size / 3, size / 6, 0, PI2, false);  // Right eye
                            context.closePath();
                            context.stroke();
                            break;
                        case 2:
                            context.fillStyle = color;
                            context.beginPath();
                            context.lineTo(x, y);
                            context.bezierCurveTo(x + 2, y - size, x + (size * 2), y, x, y + size);
                            context.lineTo(x, y);
                            context.bezierCurveTo(x + 2, y - size, x - (size * 2), y, x, y + size);
                            context.closePath();
                            context.fill();

                            break;
                        case 3:
                            context.fillStyle = color;
                            context.beginPath();
                            for (var j = 0; j <= 10; j++) {
                                var radius = (j % 2 > 0 ? size : size * .5);
                                var px = Math.cos(Math.PI * .5 + Math.PI / 5 * j) * radius;
                                var py = Math.sin(Math.PI * .5 + Math.PI / 5 * j) * radius;
                                context.lineTo(px + x, py + y);
                            }
                            context.closePath();
                            context.fill();

                            break;
                        case 4:
                            var x1 = Math.random() * size;
                            var y1 = Math.random() * size;
                            var x2 = Math.random() * size;
                            var y2 = Math.random() * size;
                            var x3 = Math.random() * size;
                            var y3 = Math.random() * size;

                            context.fillStyle = color;
                            context.beginPath();
                            context.moveTo(x + x1, y + y1);
                            context.lineTo(x + x2, y + y2);
                            context.lineTo(x + x3, y + y3);
                            context.closePath();
                            context.fill();

                            break;
                        case 5:
                            context.fillStyle = color;
                            context.beginPath();
                            context.arc(x, y, size, Math.random() * PI2, Math.random() * PI2, false);
                            context.closePath();
                            context.fill();
                            break;
                        case 6:

                            break;
                        case 7:

                            break;
                        case 8:

                            break;
                        case 9:

                            break;
                        default:
                            break;
                    }
                }

                if (this.focusedParticleIndex !== null) {
                    var pixel = pixels[this.focusedParticleIndex];

                    var x = pixel.x;
                    var y = pixel.y;

                    var size = pixel.size;
                    var color = decs2hex(pixel.r, pixel.g, pixel.b);

                    pixel.toY = window.innerHeight / 2;

                    context.fillStyle = color;
                    context.beginPath();
                    context.arc(x, y, size, 0, PI2, false);
                    context.closePath();
                    context.fill();
                }
            }
        };
    };

    return [new Universe()];
}