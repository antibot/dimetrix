jQuery(function($) {
    var information = $("#information");
    if ($.browser.device) {
        information.css({
            "width": "250px",
            "font-size": "16px"
        });
    }

    var messages = GET_MESSAGES();
    var messages_index = 0;

    var pixels = GET_PIXELS($.browser.device ? window.PIXELS_COUNT_MOBILE : window.PIXELS_COUNT_DESKTOP);

    var message = function(i) {
        i = i || 0;

        var pixel = pixels[i];

        pixel.flightMode = 2;
        pixel.toSize = $.browser.device ? window.PIXEL_INFORMATION_MOBILE : window.PIXEL_INFORMATION_DESKTOP;
        pixel.toX = pixel.toSize * 4;
        pixel.toY = window.innerHeight / 2;

        for (var j = 0; j < components.length; j++) {
            components[j].focusedParticleIndex = i;
        }

        focusedParticleIndex = i;

        information.css({
            "left": pixel.toX,
            "top": pixel.toY,
            "margin-top": -pixel.toSize + "px",
            "margin-left": pixel.toSize + "px"
        }).html(messages[messages_index++ % messages.length]).show();

        $(".partition").css("color", "rgb(" + Math.floor(pixel.r) + "," + Math.floor(pixel.g) + "," + Math.floor(pixel.b) + ")");
    };

    var focusedParticleIndex = null;

    var debug = document.getElementById('debug');

    var canvas = document.getElementById('graphics');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    $(window).resize(function(event) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    });

    var context = canvas.getContext('2d');

    var fps = new FPS(debug);

    // ---

    var contacts = $(".contacts");

    var transitions_base = GET_TRANSITIONS_BASE(pixels);
    var transitions_images = GET_TRANSITIONS_IMAGES(pixels);
    var transitions_waveform = GET_TRANSITIONS_WAVEFORM(pixels);
    var components = GET_COMPONENTS(canvas, context, transitions_base, transitions_images, transitions_waveform, pixels);

    console.log("pixels", pixels.length);

    // ---

    canvas.onmousemove = function(event) {
        var mouseX = event.pageX - canvas.offsetLeft;
        var mouseY = event.pageY - canvas.offsetTop;

        for (var i = 0; i < components.length; i++) {
            components[i].mx = mouseX;
            components[i].my = mouseY;
        }
    };

    canvas.onmousedown = function() {
        for (var i = 0; i < pixels.length; i++) {
            var pixel = pixels[i];

            if (pixel.flightMode === 1) {
                if (focusedParticleIndex !== null) {
                    pixels[focusedParticleIndex].flightMode = 0;
                    pixels[focusedParticleIndex].toSize = RANDOM(window.POINT_MIN, window.POINT_MAX);
                }

                message(i);

                break;
            }
        }
    };

    (function loop() {
        // --- clear

        //context.clearRect(0, 0, canvas.width, canvas.height);
		
		context.fillStyle = 'rgba(0, 0, 0, 0.3)';
		context.fillRect(0, 0, canvas.width, canvas.height);

        // ---

        var count = components.length, i;

        for (i = 0; i < count; i++) {
            components[i].update();
        }

        for (i = 0; i < count; i++) {
            components[i].draw();
        }

// ---

        if (focusedParticleIndex !== null) {
            var pixel = pixels[focusedParticleIndex];

            information.css({
                "left": pixel.toX,
                "top": pixel.toY
            });
        }

        // ---

        fps.exec();

        // --- loop

        requestAnimationFrame(loop);
    })();

    // ---

    setTimeout(function() {
        if (!information.is(":visible")) {
            message(0);
        }
    }, 3000);

    // ---

    contacts.click(function(event) {

    });
});