(function() {
    var method;
    var noop = function() {
    };
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }


    var information = document.getElementById('information');
    var index = 0;
    
    window.debug = function() {
        
        var text = (++index) + ') ';

        for (var i = 0; i < arguments.length; i++) {
            try {
                text += (arguments[i] + ' ');
            } catch (e) {

            }
        }
        information.innerHTML = information.innerHTML + '<br />' + text;
    };
}());
