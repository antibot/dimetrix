(function(window) {
    'use strict';

    // -- usage

    var interval;

    var audioVisualizator = new AudioVisualizator();

    debug("audioVisualizator: create");

    if (audioVisualizator) {
        if (audioVisualizator.instance()) {
            debug("audioVisualizator: instance mode", audioVisualizator.mode);

            audioVisualizator.sounds = function() {
                return ['sounds/b29f4397810ad2.mp3'];
            };
            audioVisualizator.loaded = function(success, current) {
                debug("audioVisualizator: loaded", success);

                if (success) {
                    document.onclick = function() {
                        debug("document.onclick PLAY");

                        audioVisualizator.play();
                    };
                    document.ontouchstart = function() {
                        debug("document.ontouchstart PLAY");

                        audioVisualizator.play();
                    };

                    setInterval(function() {
                        if (audioVisualizator.hasWaveform() && !audioVisualizator.nullWaveform()) {
                            var waveform = audioVisualizator.getWaveform();

                            debug("audioVisualizator: getWaveform array");
                        } else {
                            debug("audioVisualizator: getWaveform null");
                        }
                    }, 10 * 1000);
                } else {

                }
            };
            audioVisualizator.complete = function(current) {
                debug("audioVisualizator: complete callback");
            };
            audioVisualizator.error = function() {
                debug("audioVisualizator: error callback");
            };
            audioVisualizator.build();
        } else {
            debug("audioVisualizator: error instance");
        }
    } else {
        debug("audioVisualizator: error create");
    }

    // ---

    window.audioVisualizator = audioVisualizator;
}(window));