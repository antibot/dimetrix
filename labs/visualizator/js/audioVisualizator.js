(function(window) {
    window = window || {};

    'use strict';

    var browser = new BrowserDetect();

    /**
     * --- Callbacks
     * loaded
     * complete
     * error
     * 
     * --- Getters
     * sounds
     * 
     * --- Methods
     * instance() context/null
     * build() true/false
     * dispose()
     * getCurrentSound() url/null
     * hasCurrentSound() true/false
     * getDuration() int/-1
     * getPosition() int/-1
     * setPosition(position)
     * getVolume() int/-1
     * setVolume(volume)
     * play() true/false
     * stop() true/false
     * pause(); true/false
     * resume(); true/false
     * isPausing()
     * isPlaying()
     * getWaveform() array(512):[-2..2]
     * hasWaveform()  true/false
     * nullWaveform() true/false
     */
    window.AudioVisualizator = function() {
        var self = this;

        // --- Private

        var interval;
        var timeout;

        var _isPausing = false;
        var _isPlaying = false;

        var _volume = 1;

        var _lastPosition = 0;

        // --- Constants

        self.STATUS_NONE = 0;
        self.STATUS_PROGRESS = 1;
        self.STATUS_SUCCESS = 2;
        self.STATUS_ERROR = 3;

        self.MODE_NONE = 0;
        self.MODE_BUFFER = 1;
        self.MODE_MEDIA_ELEMENT = 2;
        self.MODE_FLASH = 3;

        // --- Options

        if (typeof AudioContext !== 'undefined' || typeof webkitAudioContext !== 'undefined') {
            self.mode = browser.safari ? self.MODE_BUFFER : self.MODE_MEDIA_ELEMENT;
        } else {
            if (swfobject.hasFlashPlayerVersion('10.0.0')) {
                self.mode = self.MODE_FLASH;
            } else {
                self.mode = self.MODE_NONE;
            }
        }

        //self.mode = self.MODE_BUFFER;

        self.asynchrone = false;

        debug('browser', browser, 'mode', self.mode);

        // --- Get/Set


        // --- Callbacks

        self.loaded = function(success, current) {
        };
        self.complete = function() {
        };
        self.error = function() {
        };

        // --- Getters

        self.sounds = function() {
            return [];
        };

        // --- Variables

        self.data = {};
        self.current = null;

        self.audioContext = null;
        self.audioAnalyser = null;
        self.audioSource = null;
        self.audioNodes = null;
        self.audioElement = null;
        self.flashElement = null;

        // --- Methods

        self.instance = function() { // complete
            if (!self.audioContext) {
                self.dispose();

                interval = setInterval(function() {

                    switch (self.mode) {
                        case self.MODE_NONE: // MODE_NONE

                            break;
                        case self.MODE_BUFFER: // MODE_BUFFER

                            break;
                        case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT
                        case self.MODE_FLASH: // MODE_FLASH
                            var position = self.getPosition();
                            var duration = self.getDuration();

                            if (position !== -1 && duration !== -1) {
                                if (position >= duration - 1 && position <= duration) {
                                    clearTimeout(timeout);
                                    if (self.complete) {
                                        timeout = setTimeout(function() {
                                            self.complete.call(self, self.current);
                                        }, 1000);
                                    }
                                }
                            }
                            break;
                        default:
                            break;
                    }

                }, 100);

                switch (self.mode) {
                    case self.MODE_NONE: // MODE_NONE

                        break;
                    case self.MODE_BUFFER: // MODE_BUFFER
                    case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT
                        try {
                            if (typeof AudioContext !== 'undefined') {
                                self.audioContext = new AudioContext();
                            } else if (typeof webkitAudioContext !== 'undefined') {
                                self.audioContext = new webkitAudioContext();
                            }

                            self.audioAnalyser = self.audioContext.createAnalyser();
                            //self.audioAnalyser.smoothingTimeConstant = 0.85;
                            //self.audioAnalyser.connect(self.audioContext.destination);

                            /*
                            self.audioNodes = {};

                            self.audioNodes.filter = self.audioContext.createBiquadFilter();
                            self.audioNodes.panner = self.audioContext.createPanner();
                            self.audioNodes.volume = ('createGain' in self.audioContext) ? self.audioContext.createGain() : ('createGainNode' in self.audioContext) ? self.audioContext.createGainNode() : null;
                            // var compressor = audioContext.createDynamicsCompressor();

                            self.audioNodes.filter.type = 1; // highpass
                            self.audioNodes.filter.frequency.value = 512;
                            self.audioNodes.panner.setPosition(0, 0, 0);
                            self.audioNodes.volume.gain.value = 1;

                            self.audioNodes.filter.connect(self.audioNodes.panner);
                            self.audioNodes.panner.connect(self.audioNodes.volume);
                            self.audioNodes.volume.connect(self.audioAnalyser);
                                        */
                        } catch (e) {
                            debug(e.message);

                            return null;
                        }
                        break;
                    case self.MODE_FLASH: // MODE_FLASH
                        try {
                            if (document.getElementById('audioVisualizatorContainer') === null) {
                                if (document.body !== null) {
                                    var div = document.createElement('div');
                                    div.id = 'audioVisualizatorContainer';
                                    document.body.appendChild(div);
                                }
                            }

                            var flashvars = {
                            };
                            var params = {
                            };
                            var attributes = {
                                id: 'audioVisualizatorObject',
                                name: 'audioVisualizatorObject'
                            };

                            swfobject.embedSWF('swf/audioVisualizator.swf', 'audioVisualizatorContainer', '1', '1', '10.0.0', 'swf/expressInstall.swf', flashvars, params, attributes);

                            self.audioContext = {};
                        } catch (e) {
                            return null;
                        }
                        break;

                    default:
                        break;
                }
            }

            return self.audioContext;
        };
        self.build = function(index) {
            self.stop();

            var sounds = self.sounds() || [];

            if (sounds && sounds.length) {
                index = Math.max(0, Math.min(index === undefined ? Math.floor(Math.random() * sounds.length) : index, sounds.length - 1));

                var url = sounds[index];

                debug('url = ' + url, 'index = ' + index);

                var load = true;

                if (self.data.hasOwnProperty(url)) {
                    switch (self.data[url].status) {
                        case 1:
                            load = false;
                            break;
                        case 2:
                            load = false;
                            break;
                        case 3:
                            self.data[url] = {url: url, status: self.STATUS_PROGRESS, buffer: []};
                            break;
                        default:
                            self.data[url] = {url: url, status: self.STATUS_PROGRESS, buffer: []};
                            break;
                    }
                } else {
                    self.data[url] = {url: url, status: self.STATUS_PROGRESS, buffer: []};
                }

                self.current = self.data[url];

                switch (self.mode) {
                    case self.MODE_NONE: // MODE_NONE

                        break;
                    case self.MODE_BUFFER: // MODE_BUFFER
                        if (load) {
                            var request = new XMLHttpRequest();
                            request.open('GET', url, true);
                            request.responseType = 'arraybuffer';
                            request.addEventListener('load', function(event) {
                                if (self.asynchrone) {
                                    self.audioContext.decodeAudioData(request.response, function(buffer) {
                                        self.data[url].buffer = buffer;
                                        self.data[url].status = self.STATUS_SUCCESS;

                                        if (self.loaded) {
                                            self.loaded.call(self, true, self.current);
                                        }
                                    });
                                } else {
                                    self.data[url].buffer = self.audioContext.createBuffer(request.response, false);
                                    self.data[url].status = self.STATUS_SUCCESS;

                                    if (self.loaded) {
                                        self.loaded.call(self, true, self.current);
                                    }
                                }

                            }, false);
                            request.addEventListener('error', function(event) {
                                self.data[url].buffer = [];
                                self.data[url].status = self.STATUS_ERROR;

                                if (self.loaded) {
                                    self.loaded.call(self, false, self.current);
                                }
                            }, false);
                            request.addEventListener('abort', function(event) {
                                self.data[url].buffer = [];
                                self.data[url].status = self.STATUS_ERROR;

                                if (self.loaded) {
                                    self.loaded.call(self, false, self.current);
                                }
                            }, false);
                            request.addEventListener('progress', function(event) {

                            }, false);

                            request.send();
                        } else {
                            if (self.loaded) {
                                self.loaded.call(self, true, self.current);
                            }
                        }
                        break;
                    case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT
                        if (load) {
                            self.audioElement = new Audio();
                            self.audioElement.loop = true;
                            self.audioElement.src = url;
                            self.audioElement.addEventListener('canplay', function() {
                                debug('MODE_MEDIA_ELEMENT canplay');
                            });
                            self.audioElement.addEventListener('canplaythrough', function() {
                                debug('MODE_MEDIA_ELEMENT canplaythrough');

                                self.data[url].buffer = [];
                                self.data[url].status = self.STATUS_SUCCESS;

                                if (self.loaded) {
                                    self.loaded.call(self, true, self.current);
                                }
                            });
                            self.audioElement.addEventListener('progress', function() {

                            });
                            self.audioElement.addEventListener('abort', function() {
                                debug('MODE_MEDIA_ELEMENT abort');

                                self.data[url].buffer = [];
                                self.data[url].status = self.STATUS_ERROR;

                                if (self.loaded) {
                                    self.loaded.call(self, false, self.current);
                                }
                            });

                            self.audioElement.load();
                        } else {
                            if (self.loaded) {
                                self.loaded.call(self, true, self.current);
                            }
                        }
                        break;
                    case self.MODE_FLASH:
                        /*
                         self.flashElement = document.getElementById('audioVisualizatorObject');
                         if(self.flashElement === null) {
                         var interval = setInterval(function() {
                         self.flashElement = document.getElementById('audioVisualizatorObject');
                         
                         if(self.flashElement) {
                         clearInterval(interval);
                         }
                         }, 1000);
                         } else {
                         
                         }
                         */

                        break;
                    default:
                        break;
                }
            }
        };
        self.dispose = function() { // complete
            try {
                clearInterval(interval);

                self.stop();

                // ---

                self.data = {};
                self.current = null;

                self.audioContext = null;
                self.audioAnalyser = null;
                self.audioSource = null;
                self.audioNodes = null;
                self.audioElement = null;
                self.flashElement = null;
            } catch (e) {

            }
        };
        self.getCurrentSound = function() { // complete
            return self.current && self.current.hasOwnProperty('url') ? self.current.url : null;
        };
        self.hasCurrentSound = function() { // complete
            return self.getCurrentSound() !== null;
        };
        self.getDuration = function() {
            switch (self.mode) {
                case self.MODE_NONE: // MODE_NONE

                    break;
                case self.MODE_BUFFER: // MODE_BUFFER
                    if (self.audioSource && self.audioSource.buffer && self.audioSource.buffer.length) {
                        return self.audioSource.buffer.duration;
                    }
                    break;
                case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT
                    if (self.audioElement) {
                        return self.audioElement.duration;
                    }
                    break;
                case self.MODE_FLASH: // MODE_FLASH
                    if (self.flashElement) {
                        try {
                            return self.flashElement._getDuration();
                        } catch (e) {

                        }
                    }
                    break;
                default:
                    break;
            }

            return -1;
        };
        self.getPosition = function() {
            switch (self.mode) {
                case self.MODE_NONE: // MODE_NONE

                    break;
                case self.MODE_BUFFER: // MODE_BUFFER
                    if (self.audioContext) {
                        return self.audioContext.currentTime;
                    }
                    break;
                case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT
                    if (self.audioElement) {
                        return self.audioElement.currentTime;
                    }
                    break;
                case self.MODE_FLASH: // MODE_FLASH
                    if (self.flashElement) {
                        try {
                            return self.flashElement._getPosition();
                        } catch (e) {

                        }
                    }
                    break;
                default:
                    break;
            }

            return -1;
        };
        self.setPosition = function(position) {
            _lastPosition = position;

            switch (self.mode) {
                case self.MODE_NONE: // MODE_NONE

                    break;
                case self.MODE_BUFFER: // MODE_BUFFER
                    if (self.current.hasOwnProperty('buffer') && self.current.buffer && self.current.buffer.length && self.audioContext) {
                        var p = self.isPlaying();

                        if (p) {
                            self.stop();
                        }

                        self.audioSource = self.audioContext.createBufferSource();
                        self.audioSource.buffer = self.current.buffer;
                        self.audioSource.loop = true;

                        if (self.audioSource) {
                            //self.audioSource.connect(self.audioNodes.filter);
                            self.audioSource.noteGrainOn(0, position, 0);

                            if (p) {
                                _isPlaying = true;
                                _isPausing = false;
                            } else {
                                self.audioSource.stop(0);
                            }

                            return true;
                        }
                    }
                    break;
                case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT
                    if (self.audioElement) {
                        self.audioElement.currentTime = position;
                    }
                    break;
                case self.MODE_FLASH: // MODE_FLASH
                    if (self.flashElement) {
                        try {
                            self.flashElement._setPosition(_volume);
                        } catch (e) {

                        }
                    }
                    break;
                default:
                    break;
            }
        };
        self.getVolume = function() { // complete
            switch (self.mode) {
                case self.MODE_NONE: // MODE_NONE

                    break;
                case self.MODE_BUFFER: // MODE_BUFFER

                    break;
                case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT

                    break;
                case self.MODE_FLASH: // MODE_FLASH

                    break;
                default:
                    break;
            }

            return _volume;
        };
        self.setVolume = function(volume) { // complete
            _volume = Math.max(0, Math.min(1, volume));

            switch (self.mode) {
                case self.MODE_NONE: // MODE_NONE

                    break;
                case self.MODE_BUFFER: // MODE_BUFFER
                    if (self.audioNodes && self.audioNodes.volume && self.audioNodes.volume.gain) {
                        self.audioNodes.volume.gain.value = _volume;
                    }
                    break;
                case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT
                    if (self.audioElement) {
                        self.audioElement.volume = _volume;
                    }
                    break;
                case self.MODE_FLASH: // MODE_FLASH
                    if (self.flashElement) {
                        try {
                            self.flashElement._setVolume(_volume);
                        } catch (e) {

                        }
                    }
                    break;
                default:
                    break;
            }
        };

        self.play = function() { // complete
            if (_isPlaying) {
                return false;
            }

            self.stop();

            _isPlaying = true;
            _isPausing = false;

            if (self.current && self.current.hasOwnProperty('url') && self.current.url && self.current.hasOwnProperty('status') && self.current.status === self.STATUS_SUCCESS) {
                switch (self.mode) {
                    case self.MODE_NONE: // MODE_NONE

                        break;
                    case self.MODE_BUFFER: // MODE_BUFFER
                        if (self.current.hasOwnProperty('buffer') && self.current.buffer && self.current.buffer.length && self.audioContext) {
                            self.audioSource = self.audioContext.createBufferSource();
                            self.audioSource.buffer = self.current.buffer;
                            self.audioSource.loop = true;

                            if (self.audioSource) {
                                //self.audioSource.connect(self.audioNodes.filter);
                                self.audioSource.start(0, 0);

                                return true;
                            }
                        }

                        break;
                    case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT 
                        if (self.audioContext) {
                            debug("play 1");

                            if (!self.audioElement) {
                                self.audioElement = new Audio();
                                self.audioElement.loop = true;
                                self.audioElement.src = self.current.url;
                            }

                            self.audioSource = self.audioContext.createMediaElementSource(self.audioElement);
                            self.audioSource.loop = true;

                            self.audioSource.connect(self.audioAnalyser);
            self.audioAnalyser.connect(self.audioContext.destination);

                            if (self.audioSource) {
                                debug("play 2");

                                //self.audioSource.connect(self.audioNodes.filter);
                                if (self.audioElement) {
                                    debug("play 3");

                                    self.audioElement.play();

                                    return true;
                                }
                            }
                        }
                        break;
                    case self.MODE_FLASH: // MODE_FLASH
                        if (self.flashElement) {
                            try {
                                self.flashElement._play();

                                return true;
                            } catch (e) {

                            }
                        }
                        break;
                    default:
                        break;
                }

                self.setVolume(_volume);
            }

            return false;
        };
        self.pause = function() { // complete
            if (!_isPlaying || _isPausing) {
                return false;
            }

            _lastPosition = self.getPosition();

            _isPausing = true;
            _isPlaying = false;

            switch (self.mode) {
                case self.MODE_NONE: // MODE_NONE

                    break;
                case self.MODE_BUFFER: // MODE_BUFFER
                    if (self.audioSource) {
                        //self.audioSource.disconnect(self.audioNodes.filter);
                        self.audioSource.stop(0);

                        return true;
                    }
                    break;
                case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT
                    if (self.audioElement) {
                        self.audioElement.pause();

                        return true;
                    }
                    break;
                case self.MODE_FLASH: // MODE_FLASH
                    if (self.flashElement) {
                        try {
                            self.flashElement._pause();

                            return true;
                        } catch (e) {

                        }
                    }
                    break;
                default:
                    break;
            }

            return false;
        };
        self.resume = function() { // complete
            if (_isPlaying || !_isPausing) {
                return false;
            }

            _isPausing = false;
            _isPlaying = true;

            switch (self.mode) {
                case self.MODE_NONE: // MODE_NONE

                    break;
                case self.MODE_BUFFER: // MODE_BUFFER
                    if (self.current.hasOwnProperty('buffer') && self.current.buffer && self.current.buffer.length && self.audioContext) {
                        //var position = Math.max(0, self.getPosition());

                        self.audioSource = self.audioContext.createBufferSource();
                        self.audioSource.buffer = self.current.buffer;
                        self.audioSource.loop = true;

                        if (self.audioSource) {
                            //self.audioSource.connect(self.audioNodes.filter);
                            self.audioSource.start(0, _lastPosition);

                            return true;
                        }
                    }

                    break;
                case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT
                    if (self.audioElement) {
                        self.audioElement.play();

                        return true;
                    }
                    break;
                case self.MODE_FLASH: // MODE_FLASH
                    if (self.flashElement) {
                        try {
                            self.flashElement._resume();

                            return true;
                        } catch (e) {

                        }
                    }
                    break;
                default:
                    break;
            }

            return false;
        };
        self.stop = function() { // complete
            if (!_isPlaying) {
                return false;
            }

            _lastPosition = 0;

            _isPlaying = false;
            _isPausing = false;

            switch (self.mode) {
                case self.MODE_NONE: // MODE_NONE

                    break;
                case self.MODE_BUFFER: // MODE_BUFFER
                    if (self.audioSource) {
                        //self.audioSource.disconnect(self.audioNodes.filter);
                        self.audioSource.stop(0);

                        return true;
                    }
                    break;
                case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT
                    if (self.audioSource) {
                        //self.audioSource.disconnect(self.audioNodes.filter);

                        if (self.audioElement) {
                            self.audioElement.pause();
                            //self.audioElement.currentTime = 0;
                            self.audioElement.src = '';
                            self.audioElement.load();

                            self.audioElement = null;

                            return true;
                        }
                    }

                    break;
                case self.MODE_FLASH: // MODE_FLASH
                    if (self.flashElement) {
                        try {
                            self.flashElement._stop();

                            return true;
                        } catch (e) {

                        }
                    }
                    break;
                default:
                    break;
            }

            return false;
        };
        self.getWaveform = function() { // complete [-2..2] 
            var result = [];

            switch (self.mode) {
                case self.MODE_NONE: // MODE_NONE

                    break;
                case self.MODE_BUFFER: // MODE_BUFFER
                case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT
                    if (self.audioAnalyser && self.audioAnalyser.frequencyBinCount) {
                        var bytesArray = new Uint8Array(self.audioAnalyser.frequencyBinCount);
                        self.audioAnalyser.getByteTimeDomainData(bytesArray); // bytesArray - [0..255]

                        for (var i = 0, count = bytesArray.length; i < count; i++) {
                            result[i] = ((bytesArray[i] - 128) / 64); // [-2..2]
                        }
                    }
                    break;
                case self.MODE_FLASH: // MODE_FLASH
                    if (self.flashElement) {
                        try {
                            result = self.flashElement._getWaveform() || [];
                        } catch (e) {

                        }
                    }
                    break;
                default:
                    break;
            }

            return result;
        };
        self.hasWaveform = function() { // complete
            var waveform = self.getWaveform(), count = waveform.length;

            return count > 0;
        };
        self.nullWaveform = function() { // complete
            if (self.hasWaveform()) {
                var waveform = self.getWaveform(), count = waveform.length, i = 0;

                for (; i < count; i++) {
                    if (waveform[i]) {
                        return false;
                    }
                }
            }

            return true;
        };
        self.isPausing = function() { // complete
            return _isPausing;
        };
        self.isPlaying = function() { // complete
            return _isPlaying;
        };
    };
})(window);
