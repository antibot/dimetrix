<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale = 1.0, maximum-scale = 1.0">

        <title></title>
        <link rel="stylesheet" type="text/css" href="css/reset.css" />
        <link rel="stylesheet" type="text/css" href="css/animate.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
    </head>
    <body>
        <div id="debug" class="debug"></div>

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/utils.js"></script>
        <script type="text/javascript" src="js/jquery.transit.js"></script>
        <script type="text/javascript" src="js/jquery.fittext.js"></script>
        <script type="text/javascript" src="js/jquery.lettering.js"></script>
        <script type="text/javascript" src="js/jquery.textillate.js"></script>
        <script type="text/javascript" src="js/jquery.slidely.js"></script>
        <script type="text/javascript" src="js/jquery.textly.js"></script>

        <script type="text/javascript" src="js/audio/audio.js"></script>

        <script type="text/javascript">
            var SOUND = '<?= isset($_REQUEST['sound']) && !empty($_REQUEST['sound']) ? ($_REQUEST['sound'] . '.mp3') : '1.mp3'; ?>';
            var FRAMES = [];
        </script>
        
        <script type="text/javascript" src="js/main.js"></script>

        <!--
        <script type="text/javascript" src="js/all.js"></script>
        -->

        <audio preload></audio>

        <div id="slides">
            <img src="frames/1.JPG" />
            <img src="frames/2.JPG" />
            <img src="frames/3.jpg" />
            <img src="frames/4.jpg" />  
            <img src="frames/5.jpg" /> 
            <img src="frames/6.jpg" /> 
            <img src="frames/7.jpg" /> 
            <img src="frames/8.jpg" /> 
            <img src="frames/9.jpg" /> 
            <img src="frames/10.jpg" /> 
            <img src="frames/11.jpg" /> 
            <img src="frames/12.jpg" /> 
            <img src="frames/13.jpg" /> 
        </div>

        <div class="table">
            <div class="tr">
                <div class="td">
                    <div id="texts">
                        <div>
                            <small>1 <b>ЯНВАРЯ</b> 2014</small><br /><br />ПРОГУЛКА ПО<br />"ВЕТРЕННОМУ ХОЛМУ"<br /><br /><small>ПОД ONE DIRECTION</small><br />
                        </div>
                        <div>
                            <small>2 <b>ЯНВАРЯ</b> 2014</small><br /><br />ПРОГУЛКА ПО<br />"ВЕТРЕННОМУ ХОЛМУ"<br /><br /><small>ПОД ONE DIRECTION</small><br />
                        </div>
                        <div>
                            <small>3 <b>ЯНВАРЯ</b> 2014</small><br /><br />ПРОГУЛКА ПО<br />"ВЕТРЕННОМУ ХОЛМУ"<br /><br /><small>ПОД ONE DIRECTION</small><br />
                        </div>
                        <div>
                            <small>4 <b>ЯНВАРЯ</b> 2014</small><br /><br />ПРОГУЛКА ПО<br />"ВЕТРЕННОМУ ХОЛМУ"<br /><br /><small>ПОД ONE DIRECTION</small><br />
                        </div>
                        <div>
                            <small>5 <b>ЯНВАРЯ</b> 2014</small><br /><br />ПРОГУЛКА ПО<br />"ВЕТРЕННОМУ ХОЛМУ"<br /><br /><small>ПОД ONE DIRECTION</small><br />
                        </div>
                        <div>
                            <small>6 <b>ЯНВАРЯ</b> 2014</small><br /><br />ПРОГУЛКА ПО<br />"ВЕТРЕННОМУ ХОЛМУ"<br /><br /><small>ПОД ONE DIRECTION</small><br />
                        </div>
                        <div>
                            <small>7 <b>ЯНВАРЯ</b> 2014</small><br /><br />ПРОГУЛКА ПО<br />"ВЕТРЕННОМУ ХОЛМУ"<br /><br /><small>ПОД ONE DIRECTION</small><br />
                        </div>
                        <div>
                            <small>8 <b>ЯНВАРЯ</b> 2014</small><br /><br />ПРОГУЛКА ПО<br />"ВЕТРЕННОМУ ХОЛМУ"<br /><br /><small>ПОД ONE DIRECTION</small><br />
                        </div>
                        <div>
                            <small>9 <b>ЯНВАРЯ</b> 2014</small><br /><br />ПРОГУЛКА ПО<br />"ВЕТРЕННОМУ ХОЛМУ"<br /><br /><small>ПОД ONE DIRECTION</small><br />
                        </div>
                        <div>
                            <small>10 <b>ЯНВАРЯ</b> 2014</small><br /><br />ПРОГУЛКА ПО<br />"ВЕТРЕННОМУ ХОЛМУ"<br /><br /><small>ПОД ONE DIRECTION</small><br />
                        </div>
                        <div>
                            <small>11 <b>ЯНВАРЯ</b> 2014</small><br /><br />ПРОГУЛКА ПО<br />"ВЕТРЕННОМУ ХОЛМУ"<br /><br /><small>ПОД ONE DIRECTION</small><br />
                        </div>
                        <div>
                            <small>12 <b>ЯНВАРЯ</b> 2014</small><br /><br />ПРОГУЛКА ПО<br />"ВЕТРЕННОМУ ХОЛМУ"<br /><br /><small>ПОД ONE DIRECTION</small><br />
                        </div>
                        <div>
                            <small>13 <b>ЯНВАРЯ</b> 2014</small><br /><br />ПРОГУЛКА ПО<br />"ВЕТРЕННОМУ ХОЛМУ"<br /><br /><small>ПОД ONE DIRECTION</small><br />
                        </div>
                        <div>
                            "МИНУТТА С ЛЮБОВЬЮ"<br /><br /><small>ПОД ONE DIRECTION</small><br />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="actions"></div>

    </body>
</html>