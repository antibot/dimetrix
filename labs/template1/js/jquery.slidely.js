(function($) {

    "use strict";

    /**
     * effect:
     * 
     * zoomIn,
     * zoomOut,
     * 
     * zoomInRotate,
     * zoomOutRotate,
     * 
     * leftTop,
     * rightTop,
     * leftBottom,
     * rightBottom,
     * 
     * leftRightTop,
     * leftRightCenter, 
     * leftRightBottom,
     * 
     * rightLeftTop,
     * rightLeftCenter,
     * rightLeftBottom,
     * 
     * topBottomLeft,
     * topBottomCenter,
     * topBottomRight,
     * 
     * bottomTopLeft,
     * bottomTopCenter,
     * bottomTopRight,
     * 
     * random
     * 
     * @param {object} config
     */
    $.fn.slidely = function(config) {
        config = config || {};

        // ---

        var self = this;

        // --- 

        var _effectsList = [
            'zoomIn',
            'zoomOut',
            'zoomInRotate',
            'zoomOutRotate',
            'leftTop',
            'rightTop',
            'leftBottom',
            'rightBottom',
            'leftRightTop',
            'leftRightCenter',
            'leftRightBottom',
            'rightLeftTop',
            'rightLeftCenter',
            'rightLeftBottom',
            'topBottomLeft',
            'topBottomCenter',
            'topBottomRight',
            'bottomTopLeft',
            'bottomTopCenter',
            'bottomTopRight',
            'random'
        ];

        var _config = {
            container: 'body',
            preload: 5,
            randomize: false,
            reverse: false,
            rotate: false,
            boost: false,
            boostPoints: [],
            delay: 0,
            sleep: 0,
            fadeInSpeed: 1500,
            fadeOutSpeed: 1500,
            animationSpeedMin: 8000,
            animationSpeedMax: 14000,
            minScale: 1.2,
            maxScale: 1.5,
            minAngle: 0,
            maxAngle: 10,
            loop: true,
            autoplay: true,
            effects: [],
            loopEffects: true,
            loaded: function() {
            },
            beforeFadeIn: function() {
            },
            afterFadeIn: function() {
            },
            beforeFadeOut: function() {
            },
            afterFadeOut: function() {
            },
            start: function() {
            },
            end: function() {
            }
        };

        // ---

        $.extend(_config, config);

        // --- variables

        var _images = [];
        var _container = $(_config.container);
        var _preload = _config.preload;
        var _randomize = _config.randomize;
        var _reverse = _config.reverse;
        var _rotate = _config.rotate;
        var _boost = _config.boost;
        var _boostPoints = _config.boostPoints || [];
        var _delay = _config.delay;
        var _sleep = _config.sleep;
        var _fadeInSpeed = _config.fadeInSpeed;
        var _fadeOutSpeed = _config.fadeOutSpeed;
        var _animationSpeedMin = _config.animationSpeedMin;
        var _animationSpeedMax = _config.animationSpeedMax;
        var _minScale = _config.minScale;
        var _maxScale = _config.maxScale;
        var _minAngle = _config.minAngle;
        var _maxAngle = _config.maxAngle;
        var _loop = _config.loop;
        var _autoplay = _config.autoplay;
        var _effects = _config.effects || [];
        var _loopEffects = _config._loopEffects;

        var _index = 0;
        var _playing = false;
        var _loaded = false;

        // --- functions

        var _load = function(index, img) {
            _images[index] = {
                img: img,
                w: img.naturalWidth,
                h: img.naturalHeight,
                success: img.naturalWidth > 0 && img.naturalHeight > 0
            };

            var count = 0, len = self.length;

            for (var i = 0; i < len; i++) {
                if (_images[i]) {
                    $(_images[i].img).remove();

                    count++;
                }
            }

            if ((_preload === 0 && count === _images.length) || _preload <= count) {
                if (_config && _config.loaded) {
                    _loaded = true;

                    _config.loaded.call(self, _images);
                }

                if (_autoplay) {
                    _check();
                }
            }
        };

        var _check = function() {
            if (!_images || !_images.length) {
                return;
            }

            // ---

            _playing = true;

            // ---

            var index = _index;

            var image = _images[index];

            var img = $(image.img);

            var imageWidth = image.w;
            var imageHeight = image.h;

            var imageRation = imageWidth / imageHeight;

            var containerWidth = _container.width();
            var containerHeight = _container.height();

            var containerRation = containerWidth / containerHeight;

            // --- Scale, Move, Rotate, ... options

            var size = PROPORTION_POINT([imageWidth, imageHeight], [containerWidth, containerHeight], false);

            //var k = PROPORTION_RATIO([imageWidth, imageHeight], [containerWidth, containerHeight], false);
            //console.log(k, imageWidth / k, imageHeight / k);

            var rect1 = [0, 0, size[0], size[1]];
            var rect2 = SCALE_RECT(rect1, RANDOM_F(_minScale, _maxScale));

            var rotate = RANDOM_F(_minAngle, _maxAngle);

            // --- 

            var lenX, lenY, fromX, fromY, toX, toY, fromWidth, fromHeight, toWidth, toHeight;

            /*
             * effect:
             * 
             * zoomIn,
             * zoomOut,
             * 
             * leftTop,
             * rightTop,
             * leftBottom,
             * rightBottom,
             * 
             * leftRightTop,
             * leftRightCenter, 
             * leftRightBottom,
             * 
             * rightLeftTop,
             * rightLeftCenter,
             * rightLeftBottom,
             * 
             * topBottomLeft,
             * topBottomCenter,
             * topBottomRight,
             * 
             * bottomTopLeft,
             * bottomTopCenter,
             * bottomTopRight,
             * 
             * random
             */

            var effect = (_effects && _effects.length && index < _effects.length) ? _effects[index] : _loopEffects && _effects && _effects.length ? _effects[index % _effects.length] : {effect: 'random'};

            effect = (effect.effect === 'random' || _effectsList.indexOf(effect.effect) === -1) ? _effectsList[RANDOM(0, _effectsList.length)] : effect;

            //var effect = 'topBottomLeft';

            switch (effect.effect) {
                case 'zoomIn':
                case 'zoomInRotate':
                    fromWidth = rect1[2];
                    fromHeight = rect1[3];

                    toWidth = rect2[2];
                    toHeight = rect2[3];

                    fromX = (containerWidth - fromWidth) / 2;
                    fromY = (containerHeight - fromHeight) / 2;

                    toX = 0;
                    toY = 0;
                    break;
                case 'zoomOut':
                case 'zoomOutRotate':
                    fromWidth = rect2[2];
                    fromHeight = rect2[3];

                    toWidth = rect1[2];
                    toHeight = rect1[3];

                    fromX = (containerWidth - fromWidth) / 2;
                    fromY = (containerHeight - fromHeight) / 2;

                    toX = 0;
                    toY = 0;
                    break;

                case 'leftTop':
                    break;
                case 'rightTop':
                    break;
                case 'leftBottom':
                    break;
                case 'rightBottom':
                    break;

                case 'leftRightTop':
                case 'leftRightCenter':
                case 'leftRightBottom':
                    fromWidth = rect1[2];
                    fromHeight = rect1[3];

                    toWidth = fromWidth;
                    toHeight = fromHeight;

                    if (effect === 'leftRightTop') {
                        fromY = 0;
                    } else if (effect === 'leftRightCenter') {
                        fromY = -(toHeight - containerHeight) / 2;
                    } else if (effect === 'leftRightBottom') {
                        fromY = -(toHeight - containerHeight);
                    }

                    fromX = -(toWidth - containerWidth);

                    toX = -fromX;
                    toY = 0;
                    break;

                case 'rightLeftTop':
                case 'rightLeftCenter':
                case 'rightLeftBottom':
                    fromWidth = rect1[2];
                    fromHeight = rect1[3];

                    toWidth = fromWidth;
                    toHeight = fromHeight;

                    if (effect === 'rightLeftTop') {
                        fromY = 0;
                    } else if (effect === 'rightLeftCenter') {
                        fromY = -(toHeight - containerHeight) / 2;
                    } else if (effect === 'rightLeftBottom') {
                        fromY = -(toHeight - containerHeight);
                    }

                    fromX = 0;

                    toX = -(toWidth - containerWidth) / 2;
                    toY = 0;
                    break;

                case 'topBottomLeft':
                case 'topBottomCenter':
                case 'topBottomRight':
                    fromWidth = rect1[2];
                    fromHeight = rect1[3];

                    toWidth = fromWidth;
                    toHeight = fromHeight;

                    if (effect === 'topBottomLeft') {
                        fromX = 0;
                    } else if (effect === 'topBottomCenter') {
                        fromX = -(toWidth - containerWidth) / 2;
                    } else if (effect === 'topBottomRight') {
                        fromX = -(toWidth - containerWidth);
                    }

                    fromY = -(toHeight - containerHeight);

                    toX = 0;
                    toY = -fromY;

                    break;

                case 'bottomTopLeft':
                case 'bottomTopCenter':
                case 'bottomTopRight':
                    fromWidth = rect1[2];
                    fromHeight = rect1[3];

                    toWidth = fromWidth;
                    toHeight = fromHeight;

                    if (effect === 'bottomTopLeft') {
                        fromX = 0;
                    } else if (effect === 'bottomTopCenter') {
                        fromX = -(toWidth - containerWidth) / 2;
                    } else if (effect === 'bottomTopRight') {
                        fromX = -(toWidth - containerWidth);
                    }

                    fromY = 0;

                    toX = 0;
                    toY = -(toHeight - containerHeight);

                    break;
                default:
                    break;
            }

            lenX = fromWidth - containerWidth;
            lenY = fromHeight - containerHeight;

            if (index === 0) {
                if (_config && _config.start) {
                    _config.start.call(self, _images);
                }
            }

            console.log('size', size, 'containerWidth', containerWidth, 'containerHeight', containerHeight, 'fromWidth', fromWidth, 'fromHeight', fromHeight, 'toWidth', toWidth, 'toHeight', toHeight, 'fromX', fromX, 'fromY', fromY, 'toX', toX, 'toY', toY);

            // ---

            var transition = true;

            var fadeInComplete = function() {
                if (_config && _config.afterFadeIn) {
                    _config.afterFadeIn.call(self, _images, image, index);
                }

                img.transition({
                    x: toX + 'px',
                    y: toY + 'px',
                    scale: toWidth / fromWidth,
                    queue: false,
                    duration: RANDOM(Math.max(Math.max(lenX, lenY) * 10, _animationSpeedMin), _animationSpeedMax),
                    easing: 'in-out',
                    complete: function() {
                        if (_loop || index !== _images.length - 1) {
                            if (_config && _config.beforeFadeOut) {
                                _config.beforeFadeOut.call(self, _images, image, index);
                            }
                            img.css({
                                'z-index': '-2'
                            });

                            if (transition) {
                                img.transition({
                                    opacity: 0,
                                    duration: _fadeOutSpeed,
                                    complete: fadeOutComplete
                                });
                            } else {
                                img.fadeOut(_fadeOutSpeed, fadeOutComplete);
                            }

                            if (_playing) {
                                _check();
                            }
                        } else {
                            if (_config && _config.end) {
                                _playing = false;

                                _config.end.call(self, _images);
                            }
                        }
                    }
                });
            };

            var fadeOutComplete = function() {
                if (_config && _config.afterFadeOut) {
                    _config.afterFadeOut.call(self, _images, image, index);
                }

                img.removeAttr('style').remove().hide();
            };

            // ---

            if (_config && _config.beforeFadeIn) {
                _config.beforeFadeIn.call(self, _images, image, index);
            }

            img.css({
                'width': fromWidth + 'px',
                'height': fromHeight + 'px',
                'left': fromX + 'px',
                'top': fromY + 'px',
                'z-index': '-1',
                'position': 'absolute',
                'opacity': '0'
            }).appendTo(_container);

            if (transition) {
                img.show().transition({
                    opacity: 1,
                    duration: _fadeInSpeed,
                    complete: fadeInComplete
                });
            } else {
                img.fadeIn(_fadeInSpeed, fadeInComplete);
            }

            // ---

            _index = (_index + 1) % _images.length;
        };

        // --- methods

        self.start = function() {
            if (_playing) {
                return false;
            }

            _check();

            return true;
        };
        self.stop = function() {
            _playing = false;
        };

        self.isPlaying = function() {
            return _playing;
        };

        self.isLoaded = function() {
            return _loaded;
        };

        // ---

        _container.css({
            'position': 'relative',
            'overflow': 'hidden'
        });

        if (_randomize) {
            self.sort(function() {
                return 0.5 - Math.random();
            });
        }

        self.hide().each(function(index, img) {
            if (img.complete) {
                _load(index, img);
            } else {
                $(img).one('load', function() {
                    _load(index, img);
                });
            }
        });

        // ---

        return self;
    };

}(jQuery));