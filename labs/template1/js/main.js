// --- requestAnimationFrame

(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for (var i = 0; i < vendors.length && !window.requestAnimationFrame; ++i) {
        window.requestAnimationFrame = window[vendors[i] + 'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[i] + 'CancelAnimationFrame'] || window[vendors[i] + 'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() {
                callback(currTime + timeToCall);
            }, timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

// --- FPS

var FPS = function(debug) {
    var self = this;

    var startTime = Date.now();
    var currentTime = startTime;
    var frames = 0;
    var lastFrames = 0;

    self.exec = function() {
        frames++;
        currentTime = Date.now();
        if (currentTime - startTime >= 1000) {
            console.log('fps: ' + frames);

            debug.innerHTML = frames;

            lastFrames = frames;
            frames = 0;
            startTime = currentTime;
        }

        // ---

        return lastFrames;
    };

    return self;
};

// --- loop

(function(window, document, $) {
    var debug = document.getElementById('debug');
    var fps = new FPS(debug);

    (function loop() {
        fps.exec();

        // --- loop

        requestAnimationFrame(loop);
    })();

    $(function() {
        var interval = setInterval(function() {
            if ($.prototype.slidely && $.prototype.textly) {
                clearInterval(interval);

                var player = null, audio = null, slidely = null, textly = null, actions = $('#actions');

                try {
                    audiojs.events.ready(function() {
                        player = audiojs.createAll();

                        if (player) {
                            audio = player[0];
                            audio.load('songs/' + SOUND);
                        }
                    });
                } catch (e) {
                    alert('audiojs' + e.message);
                }

                try {
                    textly = $('#texts > div').textly({
                        container: '#texts',
                        effects: [
                            {
                                position: 'center',
                                effect: '',
                                fadeInSpeed: 1000,
                                fadeOutSpeed: 1000,
                                animationSpeedMin: 2000,
                                animationSpeedMax: 3000,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                position: 'center',
                                effect: '',
                                fadeInSpeed: 1000,
                                fadeOutSpeed: 1000,
                                animationSpeedMin: 2000,
                                animationSpeedMax: 3000,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                position: 'center',
                                effect: '',
                                fadeInSpeed: 1000,
                                fadeOutSpeed: 1000,
                                animationSpeedMin: 2000,
                                animationSpeedMax: 3000,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                position: 'center',
                                effect: '',
                                fadeInSpeed: 1000,
                                fadeOutSpeed: 1000,
                                animationSpeedMin: 2000,
                                animationSpeedMax: 3000,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                position: 'center',
                                effect: '',
                                fadeInSpeed: 1000,
                                fadeOutSpeed: 1000,
                                animationSpeedMin: 2000,
                                animationSpeedMax: 3000,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                position: 'center',
                                effect: '',
                                fadeInSpeed: 1000,
                                fadeOutSpeed: 1000,
                                animationSpeedMin: 2000,
                                animationSpeedMax: 3000,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                position: 'center',
                                effect: '',
                                fadeInSpeed: 1000,
                                fadeOutSpeed: 1000,
                                animationSpeedMin: 2000,
                                animationSpeedMax: 3000,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                position: 'center',
                                effect: '',
                                fadeInSpeed: 1000,
                                fadeOutSpeed: 1000,
                                animationSpeedMin: 2000,
                                animationSpeedMax: 3000,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                position: 'center',
                                effect: '',
                                fadeInSpeed: 1000,
                                fadeOutSpeed: 1000,
                                animationSpeedMin: 2000,
                                animationSpeedMax: 3000,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                position: 'center',
                                effect: '',
                                fadeInSpeed: 1000,
                                fadeOutSpeed: 1000,
                                animationSpeedMin: 2000,
                                animationSpeedMax: 3000,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                position: 'center',
                                effect: '',
                                fadeInSpeed: 1000,
                                fadeOutSpeed: 1000,
                                animationSpeedMin: 2000,
                                animationSpeedMax: 3000,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                position: 'center',
                                effect: '',
                                fadeInSpeed: 1000,
                                fadeOutSpeed: 1000,
                                animationSpeedMin: 2000,
                                animationSpeedMax: 3000,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                position: 'center',
                                effect: '',
                                fadeInSpeed: 1000,
                                fadeOutSpeed: 1000,
                                animationSpeedMin: 2000,
                                animationSpeedMax: 3000,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                position: 'center',
                                effect: '',
                                fadeInSpeed: 1000,
                                fadeOutSpeed: 1000,
                                animationSpeedMin: 2000,
                                animationSpeedMax: 3000,
                                delay: 0,
                                sleep: 0
                            }
                        ],
                        beforeFadeIn: function(texts, text, index) {
                            console.log('textly beforeFadeIn', texts);
                        },
                        afterFadeIn: function(texts, text, index) {
                            console.log('textly afterFadeIn', texts);
                        },
                        beforeFadeOut: function(texts, text, index) {
                            console.log('textly beforeFadeOut', texts);
                        },
                        afterFadeOut: function(texts, text, index) {
                            console.log('textly afterFadeOut', texts);
                        }
                    });
                } catch (e) {
                    alert('textly' + e.message);
                }

                try {
                    slidely = $('#slides > img').slidely({
                        container: '#slides',
                        autoplay: false,
                        loop: false,
                        effects: [
                            {
                                effect: 'zoomOut',
                                fadeInSpeed: 1500,
                                fadeOutSpeed: 1500,
                                animationSpeedMin: 8000,
                                animationSpeedMax: 14000,
                                minScale: 1.2,
                                maxScale: 1.5,
                                minAngle: 0,
                                maxAngle: 10,
                                reverse: false,
                                rotate: false,
                                boost: false,
                                boostPoints: [],
                                scale: false,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                effect: 'zoomIn',
                                fadeInSpeed: 1500,
                                fadeOutSpeed: 1500,
                                animationSpeedMin: 8000,
                                animationSpeedMax: 14000,
                                minScale: 1.2,
                                maxScale: 1.5,
                                minAngle: 0,
                                maxAngle: 10,
                                reverse: false,
                                rotate: false,
                                boost: false,
                                boostPoints: [],
                                scale: false,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                effect: 'zoomIn',
                                fadeInSpeed: 1500,
                                fadeOutSpeed: 1500,
                                animationSpeedMin: 8000,
                                animationSpeedMax: 14000,
                                minScale: 1.2,
                                maxScale: 1.5,
                                minAngle: 0,
                                maxAngle: 10,
                                reverse: false,
                                rotate: false,
                                boost: false,
                                boostPoints: [],
                                scale: false,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                effect: 'zoomIn',
                                fadeInSpeed: 1500,
                                fadeOutSpeed: 1500,
                                animationSpeedMin: 8000,
                                animationSpeedMax: 14000,
                                minScale: 1.2,
                                maxScale: 1.5,
                                minAngle: 0,
                                maxAngle: 10,
                                reverse: false,
                                rotate: false,
                                boost: false,
                                boostPoints: [],
                                scale: false,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                effect: 'bottomTopCenter',
                                fadeInSpeed: 1500,
                                fadeOutSpeed: 1500,
                                animationSpeedMin: 8000,
                                animationSpeedMax: 14000,
                                minScale: 1.2,
                                maxScale: 1.5,
                                minAngle: 0,
                                maxAngle: 10,
                                reverse: false,
                                rotate: false,
                                boost: false,
                                boostPoints: [],
                                scale: false,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                effect: 'bottomTopCenter',
                                fadeInSpeed: 1500,
                                fadeOutSpeed: 1500,
                                animationSpeedMin: 8000,
                                animationSpeedMax: 14000,
                                minScale: 1.2,
                                maxScale: 1.5,
                                minAngle: 0,
                                maxAngle: 10,
                                reverse: false,
                                rotate: false,
                                boost: false,
                                boostPoints: [],
                                scale: false,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                effect: 'zoomIn',
                                fadeInSpeed: 1500,
                                fadeOutSpeed: 1500,
                                animationSpeedMin: 8000,
                                animationSpeedMax: 14000,
                                minScale: 1.2,
                                maxScale: 1.5,
                                minAngle: 0,
                                maxAngle: 10,
                                reverse: false,
                                rotate: false,
                                boost: false,
                                boostPoints: [],
                                scale: false,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                effect: 'topBottomCenter',
                                fadeInSpeed: 1500,
                                fadeOutSpeed: 1500,
                                animationSpeedMin: 8000,
                                animationSpeedMax: 14000,
                                minScale: 1.2,
                                maxScale: 1.5,
                                minAngle: 0,
                                maxAngle: 10,
                                reverse: false,
                                rotate: false,
                                boost: false,
                                boostPoints: [],
                                scale: false,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                effect: 'bottomTopCenter',
                                fadeInSpeed: 1500,
                                fadeOutSpeed: 1500,
                                animationSpeedMin: 8000,
                                animationSpeedMax: 14000,
                                minScale: 1.2,
                                maxScale: 1.5,
                                minAngle: 0,
                                maxAngle: 10,
                                reverse: false,
                                rotate: false,
                                boost: false,
                                boostPoints: [],
                                scale: false,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                effect: 'bottomTopCenter',
                                fadeInSpeed: 1500,
                                fadeOutSpeed: 1500,
                                animationSpeedMin: 8000,
                                animationSpeedMax: 14000,
                                minScale: 1.2,
                                maxScale: 1.5,
                                minAngle: 0,
                                maxAngle: 10,
                                reverse: false,
                                rotate: false,
                                boost: false,
                                boostPoints: [],
                                scale: false,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                effect: 'zoomIn',
                                fadeInSpeed: 1500,
                                fadeOutSpeed: 1500,
                                animationSpeedMin: 8000,
                                animationSpeedMax: 14000,
                                minScale: 1.2,
                                maxScale: 1.5,
                                minAngle: 0,
                                maxAngle: 10,
                                reverse: false,
                                rotate: false,
                                boost: false,
                                boostPoints: [],
                                scale: false,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                effect: 'bottomTopCenter',
                                fadeInSpeed: 1500,
                                fadeOutSpeed: 1500,
                                animationSpeedMin: 8000,
                                animationSpeedMax: 14000,
                                minScale: 1.2,
                                maxScale: 1.5,
                                minAngle: 0,
                                maxAngle: 10,
                                reverse: false,
                                rotate: false,
                                boost: false,
                                boostPoints: [],
                                scale: false,
                                delay: 0,
                                sleep: 0
                            },
                            {
                                effect: 'zoomOut',
                                fadeInSpeed: 1500,
                                fadeOutSpeed: 1500,
                                animationSpeedMin: 8000,
                                animationSpeedMax: 14000,
                                minScale: 1.2,
                                maxScale: 1.5,
                                minAngle: 0,
                                maxAngle: 10,
                                reverse: false,
                                rotate: false,
                                boost: false,
                                boostPoints: [],
                                scale: false,
                                delay: 0,
                                sleep: 0
                            }
                        ],
                        loaded: function(images) {
                            console.log('slidely loaded', images);

                            var self = this;

                            var interval = setInterval(function() { // all loaded
                                if (audio) {
                                    clearInterval(interval);

                                    /*
                                    actions.fadeIn('fast').bind('click', function() {
                                        actions.fadeOut('fast');
                                        
                                        self.start();

                                        audio.play();
                                    });
                                    */

                                    self.start();

                                    audio.play();
                                }
                            }, 100);
                        },
                        beforeFadeIn: function(images, image, index) {
                            console.log('slidely beforeFadeIn', images);
                        },
                        afterFadeIn: function(images, image, index) {
                            console.log('slidely afterFadeIn', images);

                            if (textly) {
                                textly.start(index);
                            }
                        },
                        beforeFadeOut: function(images, image, index) {
                            console.log('slidely beforeFadeOut', images);
                        },
                        afterFadeOut: function(images, image, index) {
                            console.log('slidely afterFadeOut', images);
                        },
                        start: function(images) {
                            console.log('slidely start', images);
                        },
                        end: function(images) {
                            console.log('slidely end', images);

                            if (textly) {
                                textly.start(13);
                            }
                        }
                    });
                } catch (e) {
                    alert('slidely' + e.message);
                }
            }
        }, 100);
    });
})(window, document, jQuery);