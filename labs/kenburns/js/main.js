(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for (var i = 0; i < vendors.length && !window.requestAnimationFrame; ++i) {
        window.requestAnimationFrame = window[vendors[i] + 'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[i] + 'CancelAnimationFrame'] || window[vendors[i] + 'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() {
                callback(currTime + timeToCall);
            }, timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

var FPS = function(debug) {
    var self = this;

    var startTime = Date.now();
    var currentTime = startTime;
    var frames = 0;
    var lastFrames = 0;

    self.exec = function() {
        frames++;
        currentTime = Date.now();
        if (currentTime - startTime >= 1000) {
            console.log('fps: ' + frames);

            debug.innerHTML = frames;

            lastFrames = frames;
            frames = 0;
            startTime = currentTime;
        }

        // ---

        return lastFrames;
    };

    return self;
};

var UPDATER = function(canvas, context) {
    var self = this;

    self.exec = function() {

    };

    return self;
};

var RENDERER = function(canvas, context) {
    var self = this;

    var type = 6;

    self.exec = function() {

        var PI2 = Math.PI * 2;

        for (var i = 0; i < 512; i++) {

            var x = 100;
            var y = 100;

            var x = Math.floor(Math.random() * canvas.width);
            var y = Math.floor(Math.random() * canvas.width);

            var size = 10;

            switch (type) {
                case 0:
                    context.fillStyle = ((1 << 24) * Math.random() | 0).toString(16);
                    context.beginPath();
                    context.arc(x, y, size, 0, PI2, false);
                    context.closePath();
                    context.fill();
                    break;
                case 1:
                    context.strokeStyle = ((1 << 24) * Math.random() | 0).toString(16);
                    context.beginPath();
                    context.arc(x, y, size, 0, PI2, true); // Outer circle
                    context.moveTo(x + 2 * size / 3, y);
                    context.arc(x, y, 2 * size / 3, 0, Math.PI, false);
                    context.moveTo(x - size / 6, y - size / 3);
                    context.arc(x - size / 3, y - size / 3, size / 6, 0, PI2, false);  // Left eye
                    context.moveTo(x + size / 2, y - size / 3);
                    context.arc(x + size / 3, y - size / 3, size / 6, 0, PI2, false);  // Right eye
                    context.closePath();
                    context.stroke();
                    break;
                case 2:
                    context.fillStyle = ((1 << 24) * Math.random() | 0).toString(16);
                    context.beginPath();
                    context.lineTo(x, y);
                    context.bezierCurveTo(x + 2, y - size, x + (size * 2), y, x, y + size);
                    context.lineTo(x, y);
                    context.bezierCurveTo(x + 2, y - size, x - (size * 2), y, x, y + size);
                    context.closePath();
                    context.fill();
                    break;
                case 3:
                    var angleIncrement = Math.PI / 5;
                    var ninety = Math.PI * .5;

                    context.fillStyle = ((1 << 24) * Math.random() | 0).toString(16);
                    context.beginPath();
                    for (var j = 0; j <= 10; j++) {
                        var radius = (j % 2 > 0 ? size : size * .5);
                        var px = Math.cos(ninety + angleIncrement * j) * radius;
                        var py = Math.sin(ninety + angleIncrement * j) * radius;
                        context.lineTo(px + x, py + y);
                    }
                    context.closePath();
                    context.fill();

                    break;
                case 4:
                    var x1 = Math.random() * size;
                    var y1 = Math.random() * size;
                    var x2 = Math.random() * size;
                    var y2 = Math.random() * size;
                    var x3 = Math.random() * size;
                    var y3 = Math.random() * size;

                    context.fillStyle = ((1 << 24) * Math.random() | 0).toString(16);
                    context.beginPath();
                    context.moveTo(x + x1, y + y1);
                    context.lineTo(x + x2, y + y2);
                    context.lineTo(x + x3, y + y3);
                    context.closePath();
                    context.fill();

                    break;
                case 5:
                    var fromAngle = Math.random() * PI2;
                    var toAngle = Math.random() * PI2;

                    context.fillStyle = ((1 << 24) * Math.random() | 0).toString(16);
                    context.beginPath();
                    context.arc(x, y, size, fromAngle, toAngle, false);
                    context.closePath();
                    context.fill();
                    break;
                case 6:
                    context.save();
                    context.scale(0.1, 0.1);
                    context.translate(x, y);
                    context.beginPath();
                    context.moveTo(0, 0);
                    context.lineTo(512, 0);
                    context.lineTo(512, 512);
                    context.lineTo(0, 512);
                    context.closePath();
                    context.clip();
                    context.translate(0, 0);
                    context.translate(0, 0);
                    context.scale(1, 1);
                    context.translate(0, 0);
                    context.fillStyle = ((1 << 24) * Math.random() | 0).toString(16);
                    context.lineCap = 'butt';
                    context.lineJoin = 'miter';
                    context.miterLimit = 4;
                    context.save();
                    context.beginPath();
                    context.moveTo(237, 410.631);
                    context.lineTo(275, 410.631);
                    context.lineTo(275, 462);
                    context.lineTo(237, 462);
                    context.lineTo(237, 410.631);
                    context.closePath();
                    context.moveTo(123.521, 368.049);
                    context.bezierCurveTo(195.089, 402.95099999999996, 316.535, 403.13599999999997, 388.48, 368.049);
                    context.bezierCurveTo(348.959, 353.45599999999996, 316.67400000000004, 313.936, 310.677, 288.50699999999995);
                    context.bezierCurveTo(327.80600000000004, 288.50699999999995, 344.505, 284.01699999999994, 354.803, 276.64099999999996);
                    context.bezierCurveTo(322.24, 259.94199999999995, 297.121, 233.84999999999997, 288.013, 205.25299999999996);
                    context.bezierCurveTo(296.495, 205.25299999999996, 313.299, 201.49499999999995, 320.01399999999995, 195.64999999999995);
                    context.bezierCurveTo(290.79, 178.396, 267.411, 146.388, 256, 118);
                    context.bezierCurveTo(244.589, 146.388, 221.21, 178.39600000000002, 191.987, 195.65);
                    context.bezierCurveTo(198.701, 201.495, 215.505, 205.25300000000001, 223.987, 205.25300000000001);
                    context.bezierCurveTo(214.879, 233.85000000000002, 189.76, 259.94100000000003, 157.19799999999998, 276.641);
                    context.bezierCurveTo(167.49499999999998, 284.017, 184.194, 288.507, 201.32299999999998, 288.507);
                    context.bezierCurveTo(195.327, 313.936, 163.042, 353.456, 123.521, 368.049);
                    context.closePath();
                    context.moveTo(245.579, 71.497);
                    context.lineTo(221.913, 74.766);
                    context.lineTo(239.139, 91.322);
                    context.lineTo(234.934, 114.839);
                    context.lineTo(256, 103.571);
                    context.lineTo(277.066, 114.838);
                    context.lineTo(272.86199999999997, 91.321);
                    context.lineTo(290.087, 74.765);
                    context.lineTo(266.421, 71.496);
                    context.lineTo(256, 50);
                    context.lineTo(245.579, 71.497);
                    context.closePath();
                    context.fill();
                    context.stroke();
                    context.restore();
                    context.restore();
                    break;
                case 7:
                    context.save();
                    context.scale(0.1, 0.1);
                    context.translate(0, 0);
                    context.beginPath();
                    context.moveTo(0, 0);
                    context.lineTo(512, 0);
                    context.lineTo(512, 512);
                    context.lineTo(0, 512);
                    context.closePath();
                    context.clip();
                    context.translate(0, 0);
                    context.translate(0, 0);
                    context.scale(1, 1);
                    context.translate(0, 0);
                    context.fillStyle = ((1 << 24) * Math.random() | 0).toString(16);
                    context.lineCap = 'butt';
                    context.lineJoin = 'miter';
                    context.miterLimit = 4;
                    context.save();
                    context.beginPath();
                    context.moveTo(282.718, 76.719);
                    context.bezierCurveTo(282.718, 61.962, 270.756, 50, 256, 50);
                    context.bezierCurveTo(241.24400000000003, 50, 229.282, 61.962, 229.282, 76.719);
                    context.bezierCurveTo(229.282, 88.758, 223.22500000000002, 93.928, 215.298, 93.928);
                    context.lineTo(215.284, 93.928);
                    context.lineTo(215.284, 116.55799999999999);
                    context.lineTo(296.702, 116.55799999999999);
                    context.lineTo(296.702, 93.928);
                    context.bezierCurveTo(288.708, 93.928, 282.718, 88.724, 282.718, 76.719);
                    context.closePath();
                    context.moveTo(256, 93.264);
                    context.bezierCurveTo(246.877, 93.264, 239.45499999999998, 85.842, 239.45499999999998, 76.719);
                    context.bezierCurveTo(239.45499999999998, 67.595, 246.87699999999998, 60.172999999999995, 256, 60.172999999999995);
                    context.bezierCurveTo(265.12300000000005, 60.172999999999995, 272.545, 67.595, 272.545, 76.719);
                    context.bezierCurveTo(272.545, 85.842, 265.123, 93.264, 256, 93.264);
                    context.closePath();
                    context.moveTo(364.695, 199.608);
                    context.bezierCurveTo(336.275, 171.188, 303.714, 168.037, 296.702, 136.90300000000002);
                    context.lineTo(215.284, 136.91700000000003);
                    context.bezierCurveTo(208.267, 168.06200000000004, 175.697, 171.21500000000003, 147.30399999999997, 199.60800000000003);
                    context.bezierCurveTo(119.49199999999998, 227.42200000000003, 102.28699999999998, 265.84400000000005, 102.28699999999998, 308.288);
                    context.bezierCurveTo(102.287, 393.181, 171.106, 462, 256, 462);
                    context.bezierCurveTo(340.89300000000003, 462, 409.71299999999997, 393.181, 409.71299999999997, 308.288);
                    context.bezierCurveTo(409.713, 265.845, 392.509, 227.423, 364.695, 199.608);
                    context.closePath();
                    context.moveTo(256, 432.001);
                    context.bezierCurveTo(187.676, 432.001, 132.287, 376.61199999999997, 132.287, 308.288);
                    context.bezierCurveTo(132.287, 239.96400000000006, 187.676, 184.575, 256, 184.575);
                    context.bezierCurveTo(324.324, 184.575, 379.71299999999997, 239.964, 379.71299999999997, 308.288);
                    context.bezierCurveTo(379.71299999999997, 376.612, 324.324, 432.001, 256, 432.001);
                    context.closePath();
                    context.moveTo(283.753, 220.032);
                    context.bezierCurveTo(373.557, 249.40200000000002, 366.192, 380.089, 272.782, 398.495);
                    context.bezierCurveTo(325.746, 353.851, 332.41, 272.706, 283.753, 220.032);
                    context.closePath();
                    context.fill();
                    context.stroke();
                    context.restore();
                    context.restore();
                    break;
                case 8:

                    break;
                case 9:

                    break;
                default:
                    break;
            }
        }
    };

    return self;
};

(function(window, document) {

    var debug = document.getElementById('debug');

    var canvas = document.getElementById('graphics');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    window.onresize = function(event) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    };

    var context = canvas.getContext('2d');
    var updater = new UPDATER(canvas, context);
    var rendered = new RENDERER(canvas, context);
    var fps = new FPS(debug);

    (function loop() {
        // --- clear

        context.save();
        context.setTransform(1, 0, 0, 1, 0, 0);
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.restore();

        // ---

        //updater.exec();
        //rendered.exec();
        fps.exec();

        // --- loop

        requestAnimationFrame(loop);
    })();

})(window, document);