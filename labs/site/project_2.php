<?php
require_once 'Mobile_Detect.php';

try {
    if (class_exists('Mobile_Detect')) {
        $detect = new Mobile_Detect();
        $isMobile = $detect->isMobile();
        $isTablet = $detect->isTablet();

        if ($isMobile || $isTablet) {
            header("Status: 301 Moved Permanently");
            header("Location: //" . ($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']) . (strlen($_SERVER['QUERY_STRING']) ? '&' : '?') . 'v=1');
            exit;
        }
    }
} catch (Exception $exc) {
    
}

define('RELEASE', true);

define('SERVER_URL', '//' . $_SERVER['HTTP_HOST'] . '/api.php');
define('PHOTO_PREFIX', '//' . $_SERVER['HTTP_HOST']);
define('SOUND_PREFIX', '//' . $_SERVER['HTTP_HOST'] . '/apidata/site_data2/songs/');
$PREFIX = RELEASE ? '//' . $_SERVER['HTTP_HOST'] . '/apidata/site_data2/' : 'site_data2/';

//define('SERVER_URL', 'http://mimo-us.cloudapp.net/api.php');
//define('PHOTO_PREFIX', 'http://mimo-us.cloudapp.net/');
//define('SOUND_PREFIX', 'http://mimo-us.cloudapp.net/apidata/site_data2/songs/');
//$PREFIX = RELEASE ? "http://mimo-us.cloudapp.net/apidata/site_data2/" : "site_data2/";

$leave_a_comment = "Leave a comment";
$MINUTTA_WITH_LOVE = "MINUTTA WITH LOVE";
$LOGIN = "LOGIN";
$LIKE = "LIKE";
$ANSWER_ME = "ANSWER ME";
$SEND = "SEND";

/*
  $leave_a_comment = "Оставить комментарий";
  $MINUTTA_WITH_LOVE = "МИНУТТА С ЛЮБОВЬЮ";
  $LOGIN = "ВОЙТИ";
  $LIKE = "МНЕ НРАВИТСЯ"
  $ANSWER_ME = "ОТВЕТИТЬ МНЕ";
  $SEND = "ОТПРАВИТЬ";
 */

$TYPE = 0;

$PHOTOS = array();
if (!RELEASE) {
    $PHOTOS[] = '"' . $PREFIX . '/frames/1.jpg"';
    $PHOTOS[] = '"' . $PREFIX . '/frames/2.jpg"';
    $PHOTOS[] = '"' . $PREFIX . '/frames/3.jpg"';
}

$SOUNDS = array();

if (!RELEASE) {
    $SOUNDS[] = '"' . $PREFIX . '/songs/6.mp3"';
}
 
if (RELEASE) {

    $ok = false;

    require_once 'config.php';

    $pid = isset($_REQUEST['pid']) && !empty($_REQUEST['pid']) ? $_REQUEST['pid'] : NULL;

    if (!empty($pid)) {
        $db = MinuttaServer::InitDB();

        if (!empty($db)) {

            $q = "SELECT * FROM projects WHERE id='$pid' LIMIT 1";

            $res = $db->query($q);

            if (!empty($res)) {
                $data = $res->fetch_assoc();

                if (!empty($data)) {
                    $ok = true;

                    $TYPE = intval($data['template_id']);

                    $SOUNDS[] = '"' . SOUND_PREFIX . (intval($data['music_id']) + 1) . '.mp3' . '"';

                    if (isset($data['frames']) && !empty($data['frames'])) {
                        $frames = json_decode($data['frames']);

                        if (!empty($frames) && is_array($frames)) {
                            foreach ($frames as $frame) {
                                $PHOTOS[] = '"' . PHOTO_PREFIX . $frame . '"';
                            }
                        }
                    }
                }
            } else {
                //echo 'DB error 2';
            }
        } else {
            //echo 'DB error 1';
        }
    } else {
        //echo 'Bad params';
    }

    if (!$ok) {
        header('HTTP/1.0 404 Not Found');
        echo "<h1>404 Not Found</h1>";
        echo "The page that you have requested could not be found.";
        exit();
    }
}
?>

<!DOCTYPE html>
<html>
    <head prefix="og: http://ogp.me/ns#">
        <meta charset="utf-8">
        <title><?= $MINUTTA_WITH_LOVE; ?></title>

        <meta name="description" content="" />
        <meta name="keywords" content="" />

        <link rel="apple-touch-icon-precomposed" href="<?= $PREFIX ?>images/apple_touch_icon.png" />

        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">

        <meta name="application-name" content="minutta" />
        <meta name="msapplication-TileColor" content="#ffffff" />
        <meta name="msapplication-TileImage" content="<?= $PREFIX ?>/images/thumbnail.png" />

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale = 1.0, maximum-scale = 1.0">

        <link rel="shortcut icon" href="<?= $PREFIX ?>favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?= $PREFIX ?>favicon.ico" type="image/x-icon">

        <link href='http://fonts.googleapis.com/css?family=Poiret+One&subset=latin,cyrillic,latin-ext' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="<?= $PREFIX ?>css/normalize.css">
        <link rel="stylesheet" href="<?= $PREFIX ?>css/main.css">
        <link rel="stylesheet" href="<?= $PREFIX ?>css/style.css">

        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-46690684-1', location.host);
            ga('send', 'pageview');
        </script>

        <script src="<?= $PREFIX ?>js/vendor/modernizr-2.7.0.min.js"></script>
        <script src="<?= $PREFIX ?>js/vendor/jquery-1.10.2.min.js"></script>

        <!--
        <script src="http://www.protonotes.com/js/protonotes.js" type="text/javascript"></script>
        -->

        <script type="text/javascript">
            //var groupnumber = "zO0oufBexd75";
            //var show_menubar_default = false;
            //var show_notes_default = false;
        </script>
    </head>
    <body class="screen_normal loading first">
        <div id="fb-root"></div>

        <div class="m-bg-blur-container">
            <img src=<?= $PHOTOS[0] ?> class="m-bg-blur" />
        </div>

        <div class="m-bg-mask"></div>

        <div class="minutta" style="display: <?= $version == 2 ? 'none' : 'block'; ?>">
            <div class="graphics-container-fullscreen"></div>

            <canvas id="graphics1" class="graphics" width="0" height="0"></canvas> 

            <div id="m-text-slide1">
                <span><div></div><span id="m-text-slide1-message"></span></span>
            </div>

            <a class="m-screen-button m-button m-button-action">
                <div class="m-button-icon"></div>
                <span><?= $LIKE; ?></span>
            </a>
            
            <div class="m-video">
                <div class="m-bg-normal-container">
                    <img src=<?= $PHOTOS[0] ?> class="m-bg-normal" />
                    <div class="graphics-container-min graphics-container-normal">
                        <canvas id="graphics2" class="graphics" width="0" height="0"></canvas> 
                    </div>
                </div>

                <div id="m-text-slide2">
                    <span><div></div><span id="m-text-slide2-message"></span></span>
                </div>

                <div class="m-video-loading"></div>
                <div class="m-video-play"></div>
                <div class="m-video-stop">
                    <div class="m-video-fullscreen"></div>
                </div>   
            </div>

            <div class="m-header">
                <a class="m-welcome"><?= $MINUTTA_WITH_LOVE; ?></a>

                <a class="button-ios"></a> 

                <a class="m-login m-button"><?= $LOGIN; ?></a>
            </div>
            <div class="m-footer"> 
                <div class="center">     
                    <a class="m-like-button m-button m-button-action">
                        <div class="m-button-icon"></div>
                        <span><?= $LIKE; ?></span>
                    </a>
                    <!--
                    <a class="m-share m-button m-button-action">
                        <div class="m-button-icon"></div>
                        <span>SHARE</span>
                    </a>
                    <a class="m-info m-button m-button-action">     
                        <div class="m-button-icon"></div>
                        <span>INFO</span>
                    </a>
                    <a class="m-download m-button m-button-action">
                        <div class="m-button-icon"></div>
                        <span>DOWNLOAD</span>
                    </a>
                    <a class="m-report m-button m-button-action">
                        <div class="m-button-icon"></div>
                        <span>REPORT</span>
                    </a>
                    -->
                    <a class="m-comment-button m-button m-button-action">
                        <div class="m-button-icon"></div>
                        <span><?= $ANSWER_ME; ?></span>
                    </a>
                </div>

            </div>
            <div class="m-likes m-circle">
                <div class="m-circle-icon m-likes-icon"></div>
                <div id="m-likes-counter" class="m-circle-counter">5</div>
            </div>
            <div class="m-comments m-circle">
                <div class="m-circle-icon m-comments-icon"></div>
                <div id="m-comments-counter" class="m-circle-counter">5</div>
            </div>
            <div class="m-likes-container"></div>
            <div class="m-comments-container">
                <div id="m-comments-header">
                    <div id="m-comments-ear-small" class="ear-small">
                        <div class="ear-icon-small ear-icon-comments-small"></div>
                        <div id="comments-counter-small" class="comments-counter counter-text fnt-reg"><?= $leave_a_comment; ?></div>
                    </div>
                </div>
                <div id="m-add-comment">					
                    <div class="m-comment-user-cover">
                        <img src="<?= $PREFIX ?>images/user_thumb.png">
                    </div>			
                    <div class="m-comment-area">					    						
                        <textarea name="comment" maxlength="255" placeholder="<?= $leave_a_comment; ?>"></textarea>				   						
                    </div>
                    <div id="m-send-comment" class="send-comment">
                        <div class="button button-turquoise txt-shadow fnt-thick">																			
                            <span><?= $SEND; ?></span>					
                        </div>
                    </div>																		
                </div>	
                <div id="m-comments-list">
                    <ul>
                        <?php
                        /*
                          <li class="comment_my">
                          <div class="m-comment-user-cover">
                          <img src="<?= $PREFIX ?>images/user_thumb.png">
                          </div>
                          <div class="m-comment-user-message">
                          <div class="m-comment-user-author ">m-comment-user-author</div>
                          <div class="m-comment-user-text ">m-comment-user-text</div>
                          <div class="m-comment-user-data ">m-comment-user-data</div>
                          </div>
                          </li>
                         */
                        ?>
                    </ul>	
                </div>
            </div>
        </div>

        <script>
            FULLSCREEN = false;
            RELEASE = <?= RELEASE ? 1 : 0; ?>;
            PREFIX = "<?= $PREFIX; ?>";

            GET_GRAPHICS_WIDTH = function() {
                return $(document.body).hasClass('screen_normal') ? 854 : $(document.body).hasClass('screen_min') ? 570 : $(document.body).hasClass('screen_max') ? window.innerWidth : window.innerWidth;
            };

            GET_GRAPHICS_HEIGHT = function() {
                return $(document.body).hasClass('screen_normal') ? 480 : $(document.body).hasClass('screen_min') ? 320 : $(document.body).hasClass('screen_max') ? window.innerHeight : window.innerHeight;
            };

            GLOBAL_PHOTOS = [<?= implode(', ', $PHOTOS); ?>];
            GLOBAL_SOUNDS = [<?= implode(', ', $SOUNDS); ?>];
            GLOBAL_TEMPLATE = <?= $TYPE;  ?>;
            GLOBAL_AUTOPLAY = false;
            GLOBAL_COMMENTS = [];
            GLOBAL_PID = '<?= isset($pid) ? $pid : 0; ?>';
            GLOBAL_SERVER_URL = '<?= SERVER_URL; ?>';
            GLOBAL_MESSAGES = ['What a beautiful day', 'My amazing journey', 'My beautiful life', 'Sunny days', 'What a beautiful weather'];
        </script>

        <script src="<?= $PREFIX ?>js/plugins.js"></script>
        <script src="<?= $PREFIX ?>js/vendor/jquery.mousewheel.js"></script>
        <script src="<?= $PREFIX ?>js/vendor/jquery.json-2.4.min.js"></script>
        <script src="<?= $PREFIX ?>js/vendor/moment-with-langs.min.js"></script>
        <script src="<?= $PREFIX ?>js/vendor/jquery.browser.js"></script>

        <script src="<?= $PREFIX ?>js/dancer/src/dancer.js"></script>
        <script src="<?= $PREFIX ?>js/dancer/src/support.js"></script>
        <script src="<?= $PREFIX ?>js/dancer/src/kick.js"></script>
        <script src="<?= $PREFIX ?>js/dancer/src/adapterWebkit.js"></script>
        <script src="<?= $PREFIX ?>js/dancer/src/adapterMoz.js"></script>
        <script src="<?= $PREFIX ?>js/dancer/src/adapterFlash.js"></script>
        <script src="<?= $PREFIX ?>js/dancer/lib/fft.js"></script>
        <script src="<?= $PREFIX ?>js/dancer/lib/flash_detect.js"></script>
        <script src="<?= $PREFIX ?>js/dancer/plugins/dancer.fft.js"></script>
        <script src="<?= $PREFIX ?>js/dancer/plugins/dancer.waveform.js"></script>
        <script src="<?= $PREFIX ?>js/dancer/plugins/dancer.effects.js"></script>

        <script src="<?= $PREFIX ?>js/lib/utils.js"></script>
        <script src="<?= $PREFIX ?>js/lib/config.js"></script>
        <script src="<?= $PREFIX ?>js/lib/pixels.js"></script>
        <script src="<?= $PREFIX ?>js/lib/points.js"></script>
        <script src="<?= $PREFIX ?>js/lib/transitions.js"></script>
        <script src="<?= $PREFIX ?>js/lib/components.js"></script>
        <script src="<?= $PREFIX ?>js/lib/slideshows.js"></script>
        <script src="<?= $PREFIX ?>js/lib/player.js"></script>
        <script src="<?= $PREFIX ?>js/lib/messages.js"></script>

        <script src="<?= $PREFIX ?>js/components/comments.js"></script>
        <script src="<?= $PREFIX ?>js/components/likes.js"></script>

        <script src="<?= $PREFIX ?>js/facebook.js"></script> 

        <script src="//connect.facebook.net/en_US/all.js"></script>

        <script src="<?= $PREFIX ?>js/main.js"></script>
    </body>
</html>