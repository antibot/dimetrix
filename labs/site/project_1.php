<?php
require_once 'Mobile_Detect.php';

try {
    if (class_exists('Mobile_Detect')) {
        $detect = new Mobile_Detect();
        $isMobile = $detect->isMobile();
        $isTablet = $detect->isTablet();

        if (!$isMobile && !$isTablet) {
            header("Status: 301 Moved Permanently");
            header("Location: //" . ($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']) . (strlen($_SERVER['QUERY_STRING']) ? '&' : '?') . 'v=3');
            exit;
        }
    }
} catch (Exception $exc) {
    
}

define('RELEASE', true);

define('PHOTO_PREFIX', '//' . $_SERVER['HTTP_HOST']);
define('SOUND_PREFIX', '//' . $_SERVER['HTTP_HOST'] . '/apidata/site_data/songs/');
$PREFIX = RELEASE ? '//' . $_SERVER['HTTP_HOST'] . "/apidata/site_data/" : "site_data/";

//define('PHOTO_PREFIX', 'http://mimo-us.cloudapp.net/');
//define('SOUND_PREFIX', 'http://mimo-us.cloudapp.net/apidata/site_data/songs/');
//$PREFIX = RELEASE ? "http://mimo-us.cloudapp.net/apidata/site_data/" : "site_data/";

$leave_a_comment = "Leave a comment";
$MINUTTA_WITH_LOVE = "MINUTTA WITH LOVE";
$LOGIN = "LOGIN";
$LIKE = "LIKE";
$ANSWER_ME = "ANSWER ME";
$SEND = "SEND";
$INSTALL = "INSTALL";

/*
  $leave_a_comment = "Оставить комментарий";
  $MINUTTA_WITH_LOVE = "МИНУТТА С ЛЮБОВЬЮ";
  $LOGIN = "ВОЙТИ";
  $LIKE = "МНЕ НРАВИТСЯ"
  $ANSWER_ME = "ОТВЕТИТЬ МНЕ";
  $SEND = "ОТПРАВИТЬ";
  $INSTALL = "УСТАНОВИТЬ";
 */

$TYPE = 0;

$PHOTOS = array();
if (!RELEASE) {
    $PHOTOS[] = '"' . $PREFIX . '/frames/1.jpg"';
    $PHOTOS[] = '"' . $PREFIX . '/frames/2.jpg"';
    $PHOTOS[] = '"' . $PREFIX . '/frames/3.jpg"';
}

$SOUNDS = array();
if (!RELEASE) {
    $SOUNDS[] = '"' . $PREFIX . '/songs/1.mp3"';
}

if (RELEASE) {

    $ok = false;

    require_once 'config.php';

    $pid = isset($_REQUEST['pid']) && !empty($_REQUEST['pid']) ? $_REQUEST['pid'] : NULL;

    if (!empty($pid)) {
        $db = MinuttaServer::InitDB();

        if (!empty($db)) {

            $q = "SELECT * FROM projects WHERE id='$pid' LIMIT 1";

            $res = $db->query($q);

            if (!empty($res)) {
                $data = $res->fetch_assoc();

                if (!empty($data)) {
                    $ok = true;

                    $TYPE = intval($data['template_id']);

                    $SOUNDS[] = '"' . SOUND_PREFIX . (intval($data['music_id']) + 1) . '.mp3' . '"';

                    if (isset($data['frames']) && !empty($data['frames'])) {
                        $frames = json_decode($data['frames']);

                        if (!empty($frames) && is_array($frames)) {
                            foreach ($frames as $frame) {
                                $PHOTOS[] = '"' . PHOTO_PREFIX . $frame . '"';
                            }
                        }
                    }
                }
            } else {
                //echo 'DB error 2';
            }
        } else {
            //echo 'DB error 1';
        }
    } else {
        //echo 'Bad params';
    }

    if (!$ok) {
        header('HTTP/1.0 404 Not Found');
        echo "<h1>404 Not Found</h1>";
        echo "The page that you have requested could not be found.";
        exit();
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= $MINUTTA_WITH_LOVE; ?></title>

        <meta name="description" content="" />
        <meta name="keywords" content="" />

        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">

        <meta name="application-name" content="minutta" />

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale = 1.0, maximum-scale = 1.0">

        <link rel="shortcut icon" href="<?= $PREFIX ?>favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?= $PREFIX ?>favicon.ico" type="image/x-icon">

        <link href='http://fonts.googleapis.com/css?family=Poiret+One&subset=latin,cyrillic,latin-ext' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="<?= $PREFIX ?>css/normalize.css">
        <link rel="stylesheet" href="<?= $PREFIX ?>css/main.css">
        <link rel="stylesheet" href="<?= $PREFIX ?>css/style.css">

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-46690684-1', location.host);
            ga('send', 'pageview');
        </script>
        
        <script src="<?= $PREFIX ?>js/vendor/modernizr-2.7.0.min.js"></script>
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/jquery.gsap.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/plugins/CSSRulePlugin.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/plugins/RaphaelPlugin.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/plugins/ColorPropsPlugin.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/plugins/EaselPlugin.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/plugins/TextPlugin.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/plugins/ScrollToPlugin.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/plugins/KineticPlugin.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/utils/Draggable.min.js"></script>
    </head>
    <body class="loading">
        <div class="m-base-max-graphics">
            <canvas id="graphics" width="0" height="0"></canvas>
        </div>

        <div class="minutta">
            <img src=<?= $PHOTOS[0] ?> class="m-bg-blur" />
            <div class="m-bg-mask"></div>

            <div class="m-video">
                <img src=<?= $PHOTOS[0] ?> class="m-bg-normal" />
                <div class="m-base-min-graphics"></div>
                <div class="m-video-loading"></div>
                <div class="m-video-play"></div>
                <div class="m-video-stop"></div>
            </div>
            <div class="m-header">
                <a class="m-welcome"><?= $MINUTTA_WITH_LOVE; ?></a>
                <a class="m-login m-button"><?= $LOGIN; ?></a>
            </div>
            <div class="m-footer">
                <div class="center">     
                    <!--
                    <a class="m-like-button m-button m-button-action">
                        <div class="m-button-icon"></div>
                        <span>LIKE</span>
                    </a>
                    <a class="m-share m-button m-button-action">
                        <div class="m-button-icon"></div>
                        <span>SHARE</span>
                    </a>
                    <a class="m-info m-button m-button-action">     
                        <div class="m-button-icon"></div>
                        <span>INFO</span>
                    </a>
                    -->
                    <a class="m-download m-button m-button-action">
                        <div class="m-button-icon"></div>
                        <span><?= $INSTALL; ?></span>
                    </a>
                    <!--
                    <a class="m-report m-button m-button-action">
                        <div class="m-button-icon"></div>
                        <span>REPORT</span>
                    </a>
                    <a class="m-comment-button m-button m-button-action">
                        <div class="m-button-icon"></div>
                        <span>COMMENT</span>
                    </a>
                    -->
                </div>

            </div>
            <div class="m-likes m-circle">
                <div class="m-circle-icon m-likes-icon"></div>
                <div id="m-likes-counter" class="m-circle-counter">5</div>
            </div>
            <div class="m-comments m-circle">
                <div class="m-circle-icon m-comments-icon"></div>
                <div id="m-comments-counter" class="m-circle-counter">5</div>
            </div>
        </div>

        <script>
<?php
echo 'RELEASE = ' . (RELEASE ? 1 : 0) . ';';
echo 'PREFIX = "' . $PREFIX . '";';


echo 'GLOBAL_PHOTOS = [' . implode(', ', $PHOTOS) . '];';
echo 'GLOBAL_SOUNDS = [' . implode(', ', $SOUNDS) . '];';
echo 'GLOBAL_TEMPLATE = ' . $TYPE . ';';
?>

            GET_GRAPHICS_WIDTH = function() {
                return window.innerWidth <= 854 ? window.innerWidth : 854;
            };

            GET_GRAPHICS_HEIGHT = function() {
                return window.innerWidth <= 854 ? window.innerHeight : 490;
            };



            GLOBAL_AUTOPLAY = false;
        </script>

        <script src="<?= $PREFIX ?>js/plugins.js"></script>

        <script src="<?= $PREFIX ?>js/vendor/processing-1.4.1.js"></script>
        <script src="<?= $PREFIX ?>js/vendor/jquery.browser.js"></script>

        <script src="<?= $PREFIX ?>js/lib/config.js"></script>
        <script src="<?= $PREFIX ?>js/lib/utils.js"></script>
        <script src="<?= $PREFIX ?>js/lib/pixels.js"></script>
        <script src="<?= $PREFIX ?>js/lib/points.js"></script>

        <script src="<?= $PREFIX ?>js/lib/transitions.js"></script>

        <script src="<?= $PREFIX ?>js/lib/slideshows.js"></script>

        <script src="<?= $PREFIX ?>js/lib/components.js"></script>

        <script src="<?= $PREFIX ?>js/player.js"></script>
        <script src="<?= $PREFIX ?>js/main.js"></script>
        <script src="<?= $PREFIX ?>js/editor.js"></script>
    </body>
</html>
