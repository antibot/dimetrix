(function($) {
    LIMIT = 854;

    // --- PHOTO

    window.ADDPHOTO = function(data) {
        data = data || '';

        GLOBAL_PHOTOS.push($.trim(data));
    };

    window.ADDPHOTOS = function(data) {
        data = data || '';

        $.each(data.split(','), function(index, url) {
            GLOBAL_PHOTOS.push($.trim(url));
        });
    };

    window.CLEARPHOTOS = function(data) {
        GLOBAL_PHOTOS = [];
    };

// --- MUSIC

    window.ADDMUSIC = function(data) {
        GLOBAL_SOUNDS.push($.trim(data));
    };

    window.ADDMUSICS = function(data) {
        $.each(data.split(','), function(index, url) {
            GLOBAL_SOUNDS.push($.trim(url));
        });
    };

    window.CLEARMUSIC = function(data) {
        GLOBAL_SOUNDS = [];
    };

    // --- ACTIONS

    window.CLEAR = function() {
        window.CLEARPHOTOS();
        window.CLEARMUSIC();
        window.STOP();
    };

    window.BUILD = function() {
        if (audioVisualizator) {
            audioVisualizator.build();
        }
        if (photoVisualizator) {
            photoVisualizator.build();
        }
    };

    window.PLAY = function() {
        if (audioVisualizator) {
            audioVisualizator.play();
        }
        if (photoVisualizator) {
            photoVisualizator.play();
        }
    };

    window.STOP = function() {
        if (audioVisualizator) {
            audioVisualizator.stop();
        }
        if (photoVisualizator) {
            photoVisualizator.stop();
        }
    };

    window.TEMPLATE = function(type) {
        if (photoVisualizator) {
            photoVisualizator.template(type);
        }
    };

    window.EFFECTDURATION = function(duration) {
        if (photoVisualizator) {
            photoVisualizator.effect_duration(duration);
        }
    };

    window.FADEDURATIONIMAGE = function(duration) {
        if (photoVisualizator) {
            photoVisualizator.fade_duration_image(duration);
        }
    };

    window.FADEDURATIONTRANSITION = function(duration) {
        if (photoVisualizator) {
            photoVisualizator.fade_duration_transition(duration);
        }
    };

    // ---

    BUILD();

    // ---

    var graphics = $('#graphics').hide().remove();

    var spans = $('.m-button-action:not(.m-like-button):not(.m-comment-button) span');

    var like_button = $('.m-like-button');
    var comment_button = $('.m-comment-button');

    var likes_button = $('.m-likes');
    var comments_button = $('.m-comments');

    var video = $('.m-video');
    var video_play = $('.m-video-play');
    var video_stop = $('.m-video-stop');

    var base_min_graphics = $('.m-base-min-graphics');
    var base_max_graphics = $('.m-base-max-graphics');

    graphics.click(function() {
        //return false; 
    });

    video_play.click(function(event) {
        if (!$(document.body).hasClass('loading')) {
            $(document.body).addClass('playing');
            $(document.body).addClass(function() {
                return window.innerWidth <= LIMIT ? 'playingMax' : 'playingMin';
            });

            PLAY();
            video_play.hide();
            graphics.show();

            return false;
        }
    });

    video_stop.click(function(event) {
        if ($(document.body).hasClass('playing') && $(document.body).hasClass('playingMin')) {
            $(document.body).removeClass('playing');
            $(document.body).removeClass('playingMin');
            $(document.body).removeClass('playingMax');

            STOP();
            video_play.show();
            //video_stop.hide();
            graphics.hide();

            return false;
        }
    });

    $(document).click(function(event) {
        if (!$(document.body).hasClass('loading') && $(document.body).hasClass('playing') && $(document.body).hasClass('playingMax')) {
            $(document.body).removeClass('playing');
            $(document.body).removeClass('playingMin');
            $(document.body).removeClass('playingMax');

            STOP();
            video_play.show();
            graphics.hide();

            return false;
        }
    });

    $(window).resize(function(event) {
        if (window.innerWidth <= LIMIT) {
            base_max_graphics.html(graphics);

            spans.hide();

            like_button.css('display', 'inline-block');
            comment_button.css('display', 'inline-block');
            likes_button.hide();
            comments_button.hide();
            video.css({
                'width': 570,
                'margin-left': -290,
                'height': 320,
                'margin-top': -165
            });
        } else {
            base_min_graphics.html(graphics);

            spans.show();

            like_button.css('display', 'none');
            comment_button.css('display', 'none');
            likes_button.show();
            comments_button.show();
            video.css({
                'width': 854,
                'margin-left': -432,
                'height': 480,
                'margin-top': -245
            });
        }
    }).resize();

    $(window).focus(function() {
        console.log("focus");
    });

    $(window).blur(function() {
        console.log("blur");
    });

    $(window).on("pageshow", function() {
        console.log("pageshow");
    });

    $(window).on("pagehide", function() {
        console.log("pagehide");
    });

    (function() {
        var hidden = "hidden";

        // Standards:
        if (hidden in document)
            document.addEventListener("visibilitychange", onchange);
        else if ((hidden = "mozHidden") in document)
            document.addEventListener("mozvisibilitychange", onchange);
        else if ((hidden = "webkitHidden") in document)
            document.addEventListener("webkitvisibilitychange", onchange);
        else if ((hidden = "msHidden") in document)
            document.addEventListener("msvisibilitychange", onchange);
        // IE 9 and lower:
        else if ('onfocusin' in document)
            document.onfocusin = document.onfocusout = onchange;
        // All others:
        else
            window.onpageshow = window.onpagehide = window.onfocus = window.onblur = onchange;

        function onchange(event) {
            console.log(event);

            var v = 'visible', h = 'hidden', eventMap = {
            focus: v, focusin: v, pageshow: v, blur: h, focusout: h, pagehide: h
            };

            event = event || window.event;

            var className;

            if (event.type in eventMap) {
                className = eventMap[event.type];
            } else {
                className = this[hidden] ? "hidden" : "visible";
            }

            if (className === "pagehide" || className === "hidden") {
                console.log("className", className);

                if ($(document.body).hasClass('playing')) {
                    $(document.body).removeClass('playing');
                    $(document.body).removeClass('playingMin');
                    $(document.body).removeClass('playingMax');

                    STOP();
                    video_play.show();
                    graphics.hide();
                }
            }

            //alert("document.body.className" + document.body.className);
        }
    })();

})(jQuery);