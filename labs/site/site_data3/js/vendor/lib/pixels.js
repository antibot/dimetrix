function GET_PIXELS(count) {
    count = count || 100;

    var pixels = [];

    for (i = 0; i < count; i++) {
        pixels[i] = {
            x: Math.random() * GET_GRAPHICS_WIDTH(),
            y: GET_GRAPHICS_HEIGHT() / 2,
            toX: 0,
            toY: GET_GRAPHICS_HEIGHT() / 2,
            color: Math.random() * 200 + 55,
            angle: Math.random() * Math.PI * 2,
            angleRotation: 0,
            angleIncrement: (Math.random() - 0.5),
            toAngle: 0,
            size: 0,
            toSize: RANDOM(1, window.POINT_MAX),
            r: 0,
            g: 0,
            b: 0,
            toR: Math.random() * 255,
            toG: Math.random() * 255,
            toB: Math.random() * 255,
            flightMode: 0,
            speedX: 0,
            speedY: 0
        };
    


        pixels[i].angleIncrement = pixels[i].angleIncrement / 10;
        pixels[i].toX = pixels[i].x;
        
        //console.log(pixels[i].angleIncrement);
    }

    return pixels;
}