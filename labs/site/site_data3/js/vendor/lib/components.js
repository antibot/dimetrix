function GET_COMPONENTS(processing, transitions_base, transitions_border, transitions_images, transitions_waveform, pixels) {
    if (!processing || !transitions_base || !transitions_border || !transitions_images || !transitions_waveform || !pixels) {
        return [];
    }

    var Universe = function() {
        return {
            render: false,
            TYPE_NONE: 3,
            TYPE_CIRCLE: 1,
            TYPE_STAR: 0,
            TYPE_HEART: 2,
            type: GLOBAL_TEMPLATE || 0,
            currentType: GLOBAL_TEMPLATE || 0,
            mx: 0,
            my: 0,
            impulsX: 0,
            impulsY: 0,
            impulsToX: 0,
            impulsToY: 0,
            startEffectsTime: new Date(),
            startEffectsWaveformTime: new Date(),
            indexesForBaseEffects: [0, 1, 3, 4, 6, 7],
            indexesForImagesEffects: [2, 5],
            indexesForWaveformEffects: FILL_ARRAY(8, 200),
            effectsIndex: 0,
            changeIndex: 0,
            template: function(type) {
                this.type = type;
                this.currentType = type;
            },
            effect_duration: function(speed) {

            },
            fade_duration_image: function(speed) {

            },
            fade_duration_transition: function(speed) {

            },
            hasWaveform: function() {
                var audioVisualizator = window.audioVisualizator || {};
                var waveform = "getWaveform" in audioVisualizator ? (audioVisualizator.getWaveform() || []) : [];
                return !IS_NULL_ARRAY(waveform);
            },
            start: function(objects, from, to) {
                console.log('start slideshow');

                var object = objects[from];

                console.log('object.index', object.index);

                if (!window.RANDOMIZE_COLOR) {
                    $.each(pixels, function(index, pixel) {
                        var rgb = hexToRGB(object.colors[index % object.colors.length]);

                        pixel.toR = rgb.r;
                        pixel.toG = rgb.g;
                        pixel.toB = rgb.b;
                    });
                }

                if (transitions_images.length && transitions_images[1]()) {
                    this.impulsX = 0;
                    this.impulsY = 0;
                }
            },
            beforeChange: function(objects, from, to) {
                console.log('beforeChange slideshow');

                var object = objects[to];

                console.log('object.index', object.index);

                if (!window.RANDOMIZE_COLOR) {
                    $.each(pixels, function(index, pixel) {
                        var rgb = hexToRGB(object.colors[index % object.colors.length]);

                        pixel.toR = rgb.r;
                        pixel.toG = rgb.g;
                        pixel.toB = rgb.b;
                    });
                }

                if (object.fade_time === window.FADE_DURATION_TRANSITION) {
                    if (transitions_base.length && transitions_base[Math.floor(Math.random() * transitions_base.length)]()) {
                        this.impulsX = 0;
                        this.impulsY = 0;
                    }
                } else if (object.fade_time === window.FADE_DURATION_IMAGE) {
                    if (transitions_images.length && transitions_images[Math.floor(Math.random() * transitions_images.length)]()) {
                        this.impulsX = 0;
                        this.impulsY = 0;
                    }
                } else {

                }
            },
            change: function(objects, from, to) {
                console.log('change slideshow');

                if (transitions_border.length && transitions_border[Math.floor(Math.random() * transitions_border.length)]()) {
                    this.impulsX = 0;
                    this.impulsY = 0;
                }

                this.changeIndex++;
            },
            afterChange: function(objects, from, to) {
                console.log('afterChange slideshow');

                if (this.changeIndex) {

                } else {
                    if (transitions_border.length && transitions_border[Math.floor(Math.random() * transitions_border.length)]()) {
                        this.impulsX = 0;
                        this.impulsY = 0;
                    }
                }
            },
            stop: function(objects, from, to) {
                console.log('stop slideshow');
            },
            pressed: function(mouseX, mouseY) {

            },
            released: function(mouseX, mouseY) {

            },
            update: function() {
                if (!this.render || this.currentType === this.TYPE_NONE) {
                    return;
                }

                var count = pixels.length, i;

                this.impulsX = this.impulsX + (this.impulsToX - this.impulsX) / 30;
                this.impulsY = this.impulsY + (this.impulsToY - this.impulsY) / 30;

                // move to tox, toy
                for (i = 0; i < count; i++) {
                    var pixel = pixels[i];

                    // change position 
                    pixel.x = pixel.x + (pixel.toX - pixel.x) / 10;
                    pixel.y = pixel.y + (pixel.toY - pixel.y) / 10;

                    // change size 
                    pixel.size = pixel.size + (pixel.toSize - pixel.size) / 10;

                    // change color 
                    pixel.r = pixel.r + (pixel.toR - pixel.r) / 10;
                    pixel.g = pixel.g + (pixel.toG - pixel.g) / 10;
                    pixel.b = pixel.b + (pixel.toB - pixel.b) / 10;

                    // random movement
                    if (pixel.flightMode === 0) {
                        // change position
                        pixel.toX += pixel.speedX;
                        pixel.toY += pixel.speedY;

                        // check for bounds
                        if (pixel.x < 0) {
                            pixel.x = GET_GRAPHICS_WIDTH();
                            pixel.toX = GET_GRAPHICS_WIDTH();
                        }
                        if (pixel.x > GET_GRAPHICS_WIDTH()) {
                            pixel.x = 0;
                            pixel.toX = 0;
                        }

                        if (pixel.y < 0) {
                            pixel.y = GET_GRAPHICS_HEIGHT();
                            pixel.toY = GET_GRAPHICS_HEIGHT();
                        }
                        if (pixel.y > GET_GRAPHICS_HEIGHT()) {
                            pixel.y = 0;
                            pixel.toY = 0;
                        }
                    }

                    if (pixel.flightMode !== 2) {
                        // add impuls
                        pixel.toX += Math.floor(this.impulsX * (pixel.size / window.FLICKER_MAX));
                        pixel.toY += Math.floor(this.impulsY * (pixel.size / window.FLICKER_MAX));
                    }

                    pixel.angleRotation += pixel.angleIncrement;
                }

                // set flicker
                var r1 = Math.floor(Math.random() * pixels.length);
                var r2 = Math.floor(Math.random() * pixels.length);

                if (pixels[r1].flightMode !== 2) {
                    pixels[r1].size = RANDOM(window.FLICKER_MIN, window.FLICKER_MAX);

                }
                if (pixels[r2].flightMode !== 2) {
                    pixels[r2].size = RANDOM(window.FLICKER_MIN, window.FLICKER_MAX);
                }


                /*
                 if (this.hasWaveform() && this.indexesForWaveformEffects.indexOf(this.effectsIndex) !== -1) {
                 this.currentType = this.TYPE_CIRCLE;
                 
                 if (Date.now() - this.startEffectsWaveformTime.getTime() > (this.effectsIndex === this.indexesForWaveformEffects[0] ? window.WAVEFORM_TIMEOUT_MAX : window.WAVEFORM_TIMEOUT_MIN)) {
                 this.startEffectsWaveformTime = new Date();
                 
                 if (transitions_waveform[Math.floor(Math.random() * transitions_waveform.length)]()) {
                 this.impulsX = 0;
                 this.impulsY = 0;
                 }
                 
                 this.startEffectsTime = new Date();
                 
                 this.effectsIndex++;
                 }
                 } else {
                 this.currentType = this.type;
                 
                 if (Date.now() - this.startEffectsTime.getTime() > 2000) {
                 this.effectsIndex = (this.effectsIndex > (this.indexesForBaseEffects.length + this.indexesForImagesEffects.length + this.indexesForWaveformEffects.length) ? 0 : this.effectsIndex);
                 
                 this.startEffectsTime = new Date();
                 
                 this.effectsIndex = (this.hasWaveform() ? this.effectsIndex : ((this.effectsIndex > this.indexesForBaseEffects.length && this.effectsIndex > this.indexesForImagesEffects.length) ? 0 : this.effectsIndex));
                 
                 this.impulsX = Math.random() * 800 - 400;
                 this.impulsY = -Math.random() * 400;
                 
                 if (this.indexesForBaseEffects.indexOf(this.effectsIndex) !== -1) {
                 if (transitions_base[Math.floor(Math.random() * transitions_base.length)]()) {
                 this.impulsX = 0;
                 this.impulsY = 0;
                 }
                 } else if (this.indexesForImagesEffects.indexOf(this.effectsIndex) !== -1) {
                 if (transitions_images[Math.floor(Math.random() * transitions_images.length)]()) {
                 this.impulsX = 0;
                 this.impulsY = 0;
                 }
                 }
                 
                 this.startEffectsWaveformTime = new Date();
                 
                 this.effectsIndex++;
                 }
                 }
                 }
                 
                 this.effectsIndex = (this.effectsIndex > (this.indexesForBaseEffects.length + this.indexesForImagesEffects.length + this.indexesForWaveformEffects.length) ? 0 : this.effectsIndex);
                 */
            },
            draw: function() {
                if (!this.render || this.currentType === this.TYPE_NONE) {
                    return;
                }

                var count = pixels.length, i;
                for (i = 0; i < count; i++) {
                    var pixel = pixels[i];

                    processing.fill(pixel.r, pixel.g, pixel.b);

                    // ---

                    switch (this.currentType) {
                        case this.TYPE_NONE:
                            break;
                        case this.TYPE_CIRCLE:
                            processing.ellipse(pixel.x, pixel.y, pixel.size, pixel.size);
                            break;
                        case this.TYPE_STAR:
                            processing.beginShape();

                            var j;
                            var angleIncrement = Math.PI / 5;
                            var ninety = Math.PI * .5;

                            for (j = 0; j <= 10; j++) {
                                var radius = (j % 2 > 0 ? pixel.size : pixel.size * .5);
                                var px = Math.cos(pixel.angleRotation + ninety + angleIncrement * j) * radius - pixel.size / 2;
                                var py = Math.sin(pixel.angleRotation + ninety + angleIncrement * j) * radius - pixel.size / 2;
                                processing.vertex(px + pixel.x, py + pixel.y);
                            }

                            processing.endShape(processing.CLOSE);
                            break;
                        case this.TYPE_HEART:
                            var x = pixel.x;
                            var y = pixel.y;

                            var size = pixel.size;

                            processing.beginShape();
                            processing.vertex(x, y);
                            processing.bezierVertex(x + 2, y - size, x + (size * 2), y, x, y + size);
                            processing.vertex(x, y);
                            processing.bezierVertex(x + 2, y - size, x - (size * 2), y, x, y + size);
                            processing.endShape(processing.CLOSE);
                            break;
                        default:

                            break;
                    }
                }
            }
        };
    };

    return [new Universe()];
}