/** Композиция слова и окончани[е, я, ий]
 * 
 * @param value - число
 * @param result - слово без окончания
 * @param arg1
 * @param arg2
 * @param arg3
 * @return 
 * 
 */
function composition(value, result, arg1, arg2, arg3) {
    result = result === undefined ? "" : result;
    arg1 = arg1 === undefined ? "" : arg1;
    arg2 = arg2 === undefined ? "" : arg2;
    arg3 = arg3 === undefined ? "" : arg3;

    var count = value;

    var last_digit = count % 10;
    var last_two_digits = count % 100;

    if (last_digit === 1 && last_two_digits !== 11) {
        result += arg1;
    } else if ((last_digit === 2 && last_two_digits !== 12) || (last_digit === 3 && last_two_digits !== 13) || (last_digit === 4 && last_two_digits !== 14)) {
        result += arg2;
    } else {
        result += arg3;
    }

    return result;
}

$.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));

function dec2hex(dec) {
    dec = Math.floor(dec);

    return dec > 15 ? dec.toString(16) : ("0" + dec.toString(16));
}
function decs2hex(r, g, b) {
    return '#' + dec2hex(r) + dec2hex(g) + dec2hex(b);
}

var FPS = function(debug) {
    var self = this;

    var startTime = Date.now();
    var currentTime = startTime;
    var frames = 0;
    var lastFrames = 0;

    self.exec = function() {
        frames++;
        currentTime = Date.now();
        if (currentTime - startTime >= 1000) {
            console.log('fps: ' + frames);

            if (debug) {
                debug.innerHTML = frames;
            }

            lastFrames = frames;
            frames = 0;
            startTime = currentTime;
        }

        // ---

        return lastFrames;
    };

    return self;
};

(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for (var i = 0; i < vendors.length && !window.requestAnimationFrame; ++i) {
        window.requestAnimationFrame = window[vendors[i] + 'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[i] + 'CancelAnimationFrame'] || window[vendors[i] + 'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame) {
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() {
                callback(currTime + timeToCall);
            }, timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
    }

    if (!window.cancelAnimationFrame) {
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
    }
}());

function cutHex(hex) {
    hex = hex || 'ffffff';

    return (hex.charAt(0) === "#") ? hex.substring(1, 7) : hex;
}
function hexToR(hex) {
    return parseInt((cutHex(hex)).substring(0, 2), 16);
}
function hexToG(hex) {
    return parseInt((cutHex(hex)).substring(2, 4), 16);
}
function hexToB(hex) {
    return parseInt((cutHex(hex)).substring(4, 6), 16);
}

function hexToRGB(hex) {
    return {
        r: hexToR(hex),
        g: hexToG(hex),
        b: hexToB(hex)
    };
}


function RANDOM(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function PROPORTION_RATIO(from, to, fitInside) {
    from = from || [];
    to = to || [];
    fitInside = fitInside === undefined ? true : fitInside;

    return fitInside ? Math.max(from[0] / to[0], from[1] / to[1]) : Math.min(from[0] / to[0], from[1] / to[1]);
}

function PROPORTION_POINTS(from, to, fitInside) {
    from = from || [];
    to = to || [];
    fitInside = fitInside === undefined ? true : fitInside;

    var ratio = PROPORTION_RATIO(from, to, fitInside);

    return [from[0] / ratio, from[1] / ratio];
}

function INTERPOLATE_POINT(x1, y1, x2, y2, i) {
    return  [x1 + (x2 - x1) * i, y1 + (y2 - y1) * i];
}

function INTERPOLATE_RECT(r1, r2, i) {
    var p1 = INTERPOLATE_POINT(r1[0], r1[1], r2[0], r2[1], i);
    var p2 = INTERPOLATE_POINT(r1[2], r1[3], r2[2], r2[3], i);

    return [p1[0], p1[1], p2[0], p2[1]];
}

function SCALE_RECT(r, scale) {
    // Scale a rect around its center
    var w = r[2] - r[0];
    var h = r[3] - r[1];
    var cx = (r[2] + r[0]) / 2;
    var cy = (r[3] + r[1]) / 2;
    var scalew = w * scale;
    var scaleh = h * scale;

    return [cx - scalew / 2, cy - scaleh / 2, cx + scalew / 2, cy + scaleh / 2];
}

function PRELOADER(urls) {
    $(urls).each(function() {
        $('<img/>').css({
            'display': 'none',
            'src': this
        });
    });
}

function IS_NULL_ARRAY(arr) {
    arr = arr || [];

    if (!arr || !arr.length) {
        return true;
    }

    var i = 0, count = arr.length;

    for (; i < count; i++) {
        if (arr[i]) {
            return false;
        }
    }

    return true;
}

function NORMOLIZE_ARRAY(arr) {
    var result = new Float32Array();

    for (var i = 0; i < arr.lenght; i++) {
        if (Math.abs(arr[i]) > 0.01) {
            result.push(arr[i]);
        }
    }

    return arr;
}

function FILL_ARRAY(from, to) {
    var result = [];

    for (var i = from; i < to; i++) {
        result.push(i);
    }

    return result;
}

function GET_POINTS_SIZE(points) {
    points = points || [];

    if (!points.length) {
        return [0, 0];
    }

    var maxX = 0, maxY = 0, minX = Number.MAX_VALUE, minY = Number.MAX_VALUE;

    for (var i = 0; i < points.length; i++) {
        var point = points[i];

        minX = Math.min(minX, point[0]);
        minY = Math.min(minY, point[1]);

        maxX = Math.max(maxX, point[0]);
        maxY = Math.max(maxY, point[1]);
    }

    return [maxX - minX, maxY - minY];
}

function GET_POINTS_WIDTH(points) {
    return GET_POINTS_SIZE(points)[0];
}

function GET_POINTS_HEIGHT(points) {
    return GET_POINTS_SIZE(points)[1];
}
