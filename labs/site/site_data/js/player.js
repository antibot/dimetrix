(function($) {

    'use strict';

    var AudioVisualizator = function() {
        // --- Constants

        var self = this;

        self.STATUS_NONE = 0;
        self.STATUS_PROGRESS = 1;
        self.STATUS_SUCCESS = 2;
        self.STATUS_ERROR = 3;

        self.MODE_NONE = 0;
        self.MODE_BUFFER = 1;
        self.MODE_MEDIA_ELEMENT = 2;
        self.MODE_FLASH = 3;

        // --- Options

        if (typeof AudioContext !== 'undefined' || typeof webkitAudioContext !== 'undefined') {
            self.mode = ['safari'].indexOf($.browser.name) === -1 ? self.MODE_MEDIA_ELEMENT : self.MODE_BUFFER;
        } else {
            if (swfobject.hasFlashPlayerVersion('10.0.0')) {
                self.mode = self.MODE_FLASH;
            } else {
                self.mode = self.MODE_NONE;
            }
        }
    
        self.mode = self.MODE_BUFFER;

        self.asynchrone = false;

        // --- Get/Set

        self.isPlaying = false;

        // --- Callback

        self.loaded = function(success, current) {
        };
        self.complete = function() {
        };
    
        // --- getters
        
        self.sounds = function() {
            return GLOBAL_SOUNDS;
        };

        // --- Variables

        self.data = {};
        self.current = null;

        self.audioContext = null;
        self.audioAnalyser = null;
        self.audioSource = null;
        self.audioNodes = null;
        self.audioElement = null;
        self.flashElement = null;

        // --- Functions

        self.getCurrentSound = function() {
            return self.current && self.current.hasOwnProperty('url') ? self.current.url : null;
        };
        self.hasCurrentSound = function() {
            return self.getCurrentSound() !== null;
        };
        self.getDuration = function() {
            var duration = 0;

            switch (self.mode) {
                case self.MODE_NONE: // MODE_NONE

                    break;
                case self.MODE_BUFFER: // MODE_BUFFER

                    break;
                case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT

                    break;
                case self.MODE_FLASH: // MODE_FLASH

                    break;
                default:
                    break;
            }

            return duration;
        };
        self.getPosition = function() {
            var position = 0;

            switch (self.mode) {
                case self.MODE_NONE: // MODE_NONE

                    break;
                case self.MODE_BUFFER: // MODE_BUFFER

                    break;
                case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT

                    break;
                case self.MODE_FLASH: // MODE_FLASH

                    break;
                default:
                    break;
            }

            return position;
        };
        self.setPosition = function(position) {
            switch (self.mode) {
                case self.MODE_NONE: // MODE_NONE

                    break;
                case self.MODE_BUFFER: // MODE_BUFFER

                    break;
                case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT

                    break;
                case self.MODE_FLASH: // MODE_FLASH

                    break;
                default:
                    break;
            }
        };
        self.getVolume = function() {
            var volume = 0;

            switch (self.mode) {
                case self.MODE_NONE: // MODE_NONE

                    break;
                case self.MODE_BUFFER: // MODE_BUFFER

                    break;
                case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT

                    break;
                case self.MODE_FLASH: // MODE_FLASH

                    break;
                default:
                    break;
            }

            return volume;
        };
        self.setVolume = function(volume) {

            switch (self.mode) {
                case self.MODE_NONE: // MODE_NONE

                    break;
                case self.MODE_BUFFER: // MODE_BUFFER

                    break;
                case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT

                    break;
                case self.MODE_FLASH: // MODE_FLASH

                    break;
                default:
                    break;
            }
        };
        self.instance = function() {
            if (!self.audioContext) {
                switch (self.mode) {
                    case self.MODE_NONE: // MODE_NONE

                        break;
                    case self.MODE_BUFFER: // MODE_BUFFER
                    case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT
                        if (typeof AudioContext !== 'undefined') {
                            self.audioContext = new AudioContext();
                        } else if (typeof webkitAudioContext !== 'undefined') {
                            self.audioContext = new webkitAudioContext();
                        }

                        self.audioAnalyser = self.audioContext.createAnalyser();
                        self.audioAnalyser.smoothingTimeConstant = 0.85;
                        self.audioAnalyser.connect(self.audioContext.destination);
                        self.audioAnalyser.oncomplete = function() {
                            if (self.complete) {
                                self.complete.call(self);
                            }
                        };
                        break;
                    case self.MODE_FLASH: // MODE_FLASH
                        if (document.getElementById('audioVisualizatorContainer') === null) {
                            if (document.body !== null) {
                                var div = document.createElement('div');
                                div.id = 'audioVisualizatorContainer';
                                document.body.appendChild(div);
                            }
                        }

                        var flashvars = {
                        };
                        var params = {
                        };
                        var attributes = {
                            id: 'audioVisualizatorObject',
                            name: 'audioVisualizatorObject'
                        };

                        swfobject.embedSWF('swf/audioVisualizator.swf', 'audioVisualizatorContainer', '1', '1', '10.0.0', 'swf/expressInstall.swf', flashvars, params, attributes);

                        self.audioContext = {};
                        break;

                    default:
                        break;
                }
            }

            return self.audioContext;
        };
        self.build = function(index) {
            self.stop();

            var sounds = self.sounds() || [];

            if (sounds && sounds.length) {
                index = Math.max(0, Math.min(index === undefined ? Math.floor(Math.random() * sounds.length) : index, sounds.length - 1));

                var url = sounds[index];

                console.log('url = ' + url, 'index = ' + index);

                var load = true;

                if (self.data.hasOwnProperty(url)) {
                    switch (self.data[url].status) {
                        case 1:
                            load = false;
                            break;
                        case 2:
                            load = false;
                            break;
                        case 3:
                            self.data[url] = {url: url, status: self.STATUS_PROGRESS, buffer: []};
                            break;
                        default:
                            self.data[url] = {url: url, status: self.STATUS_PROGRESS, buffer: []};
                            break;
                    }
                } else {
                    self.data[url] = {url: url, status: self.STATUS_PROGRESS, buffer: []};
                }

                self.current = self.data[url];

                switch (self.mode) {
                    case self.MODE_NONE: // MODE_NONE

                        break;
                    case self.MODE_BUFFER: // MODE_BUFFER
                        if (load) {
                            var request = new XMLHttpRequest();
                            request.open('GET', url, true);
                            request.responseType = 'arraybuffer';
                            request.addEventListener('load', function(event) {
                                if (self.asynchrone) {
                                    self.audioContext.decodeAudioData(request.response, function(buffer) {
                                        self.data[url].buffer = buffer;
                                        self.data[url].status = self.STATUS_SUCCESS;

                                        if (self.loaded) {
                                            self.loaded.call(self, true, self.current);
                                        }
                                    });
                                } else {
                                    self.data[url].buffer = self.audioContext.createBuffer(request.response, false);
                                    self.data[url].status = self.STATUS_SUCCESS;

                                    if (self.loaded) {
                                        self.loaded.call(self, true, self.current);
                                    }
                                }

                            }, false);
                            request.addEventListener('error', function(event) {
                                self.data[url].buffer = [];
                                self.data[url].status = self.STATUS_ERROR;

                                if (self.loaded) {
                                    self.loaded.call(self, false, self.current);
                                }
                            }, false);
                            request.addEventListener('abort', function(event) {
                                self.data[url].buffer = [];
                                self.data[url].status = self.STATUS_ERROR;

                                if (self.loaded) {
                                    self.loaded.call(self, false, self.current);
                                }
                            }, false);
                            request.addEventListener('progress', function(event) {

                            }, false);

                            request.send();
                        } else {
                            if (self.loaded) {
                                self.loaded.call(self, true, self.current);
                            }
                        }
                        break;
                    case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT
                        if (load) {
                            self.audioElement = new Audio();
                            self.audioElement.loop = true;
                            self.audioElement.src = url;
                            self.audioElement.addEventListener('canplay', function() {

                            });
                            self.audioElement.addEventListener('canplaythrough', function() {
                                self.data[url].buffer = [];
                                self.data[url].status = self.STATUS_SUCCESS;

                                if (self.loaded) {
                                    self.loaded.call(self, true, self.current);
                                }
                            });
                            self.audioElement.addEventListener('progress', function() {

                            });
                            self.audioElement.addEventListener('abort', function() {
                                self.data[url].buffer = [];
                                self.data[url].status = self.STATUS_ERROR;

                                if (self.loaded) {
                                    self.loaded.call(self, false, self.current);
                                }
                            });

                            self.audioElement.load();
                        } else {
                            if (self.loaded) {
                                self.loaded.call(self, true, self.current);
                            }
                        }
                        break;
                    case self.MODE_FLASH:
                        /*
                         self.flashElement = document.getElementById('audioVisualizatorObject');
                         if(self.flashElement === null) {
                         var interval = setInterval(function() {
                         self.flashElement = document.getElementById('audioVisualizatorObject');
                         
                         if(self.flashElement) {
                         clearInterval(interval);
                         }
                         }, 1000);
                         } else {
                         
                         }
                         */

                        break;
                    default:
                        break;
                }
            }
        };
        self.play = function() {
            if (self.current && self.current.hasOwnProperty('url') && self.current.url && self.current.hasOwnProperty('status') && self.current.status === self.STATUS_SUCCESS) {
                self.stop();

                switch (self.mode) {
                    case self.MODE_NONE: // MODE_NONE

                        break;
                    case self.MODE_BUFFER: // MODE_BUFFER
                        if (self.current.hasOwnProperty('buffer') && self.current.buffer && self.current.buffer.length) {
                            self.isPlaying = true;

                            self.audioSource = self.audioContext.createBufferSource();
                            self.audioSource.buffer = self.current.buffer;
                            self.audioSource.loop = true;
                        }

                        break;
                    case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT
                        if (self.audioElement) {
                            self.isPlaying = true;

                            self.audioElement = new Audio();
                            self.audioElement.loop = true;
                            self.audioElement.src = self.current.url;

                            self.audioSource = self.audioContext.createMediaElementSource(self.audioElement);
                            self.audioSource.loop = true;
                        }
                        break;
                    case self.MODE_FLASH: // MODE_FLASH
                        // TODO
                        break;
                    default:
                        break;
                }

                if (self.isPlaying) {
                    switch (self.mode) {
                        case self.MODE_NONE: // MODE_NONE

                            break;
                        case self.MODE_BUFFER: // MODE_BUFFER
                        case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT
                            self.audioNodes = {};

                            self.audioNodes.filter = self.audioContext.createBiquadFilter();
                            self.audioNodes.panner = self.audioContext.createPanner();
                            self.audioNodes.volume = self.audioContext.createGainNode();
                            // var compressor = audioContext.createDynamicsCompressor();

                            self.audioNodes.filter.type = 1; // highpass
                            self.audioNodes.filter.frequency.value = 512;
                            self.audioNodes.panner.setPosition(0, 0, 0);
                            self.audioNodes.volume.gain.value = 1;

                            self.audioSource.connect(self.audioNodes.filter);

                            self.audioNodes.filter.connect(self.audioNodes.panner);
                            self.audioNodes.panner.connect(self.audioNodes.volume);
                            self.audioNodes.volume.connect(self.audioAnalyser);
                            break;
                        case self.MODE_FLASH: // MODE_FLASH
                            break;
                        default:
                            break;
                    }

                    switch (self.mode) {
                        case self.MODE_NONE: // MODE_NONE

                            break;
                        case self.MODE_BUFFER: // MODE_BUFFER
                            // audioContext.currentTime
                            self.audioSource.noteOn(0);
                            break;
                        case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT
                            if (self.audioElement) {
                                self.audioElement.play();
                            }
                            break;
                        case self.MODE_FLASH: // MODE_FLASH
                            // TODO
                            break;
                        default:
                            break;
                    }
                }
            }
        };
        self.stop = function() {
            self.isPlaying = false;

            if (self.audioSource) {
                switch (self.mode) {
                    case self.MODE_NONE: // MODE_NONE

                        break;
                    case self.MODE_BUFFER: // MODE_BUFFER
                    case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT
                        self.audioSource.disconnect(self.audioNodes.filter);
                        break;
                    case self.MODE_FLASH: // MODE_FLASH
                        // TODO
                        break;
                    default:
                        break;
                }

                switch (self.mode) {
                    case self.MODE_NONE: // MODE_NONE

                        break;
                    case self.MODE_BUFFER: // MODE_BUFFER
                        self.audioSource.noteOff(0);
                        break;
                    case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT
                        if (self.audioElement) {
                            self.audioElement.pause();
                            //self.audioElement.currentTime = 0;
                            self.audioElement.src = '';
                            self.audioElement.load();
                        }
                        break;
                    case self.MODE_FLASH: // MODE_FLASH
                        // TODO
                        break;
                    default:
                        break;
                }
            }
        };
        self.getWaveform = function() { // [-2..2]
            var result = [];

            switch (self.mode) {
                case self.MODE_NONE: // MODE_NONE

                    break;
                case self.MODE_BUFFER: // MODE_BUFFER
                case self.MODE_MEDIA_ELEMENT: // MODE_MEDIA_ELEMENT
                    if (self.audioAnalyser && self.audioAnalyser.frequencyBinCount) {
                        var bytesArray = new Uint8Array(self.audioAnalyser.frequencyBinCount);
                        self.audioAnalyser.getByteTimeDomainData(bytesArray); // bytesArray - [0..255]

                        for (var i = 0, count = bytesArray.length; i < count; i++) {
                            result[i] = ((bytesArray[i] - 128) / 64); // [-2..2]
                        }
                    }
                    break;
                case self.MODE_FLASH: // MODE_FLASH
                    // TODO
                    break;
                default:
                    break;
            }

            return result;
        };
    };

    // -- usage

    var audioVisualizator = new AudioVisualizator();
    if (audioVisualizator.instance()) {
        audioVisualizator.loaded = function(success, current) {
            console.log("loaded audioVisualizator");
            
            var self = this;

            if (GLOBAL_AUTOPLAY) {
                PLAY();
            }

            
        };
        audioVisualizator.complete = function() {
            console.log("complete audioVisualizator");
            
            var self = this;

            audioVisualizator.build();
        };

        // ---

        window.audioVisualizator = audioVisualizator;
    }
})(jQuery);