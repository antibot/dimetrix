var FB_CONNECTION = function(callback) {
    var self = this;

    // ---

    var initialized = false;

    // ---

    window.fbAsyncInit = function() {
        FB.init({
            appId: '469325003178652',
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true  // parse XFBML
        });

        initialized = true;

        if (callback) {
            callback.call(self);
        }
    };

    self.isInitialized = function() {
        return initialized;
    };

    self.login = function(callback) {
        console.log('login');

        FB.Event.subscribe('auth.authResponseChange', function(response) {
            console.log('authResponseChange', response);

            if (response.status === 'connected') {
                if (callback) {
                    callback.call(self, true);
                }
            } else if (response.status === 'not_authorized') {
                if (callback) {
                    callback.call(self, false);
                }
            } else {
                if (callback) {
                    callback.call(self, false);
                }
            }
        });

        FB.login();
    };

    self.logout = function(callback) {
        console.log('logout');

        FB.logout(function(response) {
            console.log('logout', response);

            if (callback) {
                callback.call(self);
            }
        });
    };

    self.getStatus = function(callback) {
        console.log('getStatus');

        FB.getLoginStatus(function(response) {
            console.log('getStatus', response);

            if (response.status === 'connected') {
                var uid = response.authResponse.userID;
                var accessToken = response.authResponse.accessToken;

                if (callback) {
                    callback.call(self, true);
                }
            } else if (response.status === 'not_authorized') {
                callback.call(self, false);
            } else {
                callback.call(self, false);
            }
        });
    };

    self.getMyInfo = function(callback) {
        console.log('getMyInfo');

        FB.api('/me', function(response) {
            console.log('getMyInfo', response);

            if (callback) {
                callback.call(self, response);
            }
        });
    };

    self.getFriendsInfo = function(callback) {
        console.log('getyFriendsInfo');
    };
};

window.fb_MyInfo = null;
window.fb_FriendsInfo = null;

window.fb_connection = new FB_CONNECTION(function() {
    var self = this;
});