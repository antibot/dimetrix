jQuery(function($) {
    if ($.browser.device) {

    }

    // ---

    var body = $(document.body);

    // ---

    var pixels = GET_PIXELS($.browser.device ? window.PIXELS_COUNT_MOBILE : window.PIXELS_COUNT_DESKTOP);

    var render = false;

    // ---

    var text_slide1 = document.getElementById('m-text-slide1');
    var text_slide1_message = document.getElementById('m-text-slide1-message');

    var canvas1 = document.getElementById('graphics1');
    canvas1.width = window.innerWidth;
    canvas1.height = window.innerHeight;
    $(window).bind('resize', function(event) {
        canvas1.width = window.innerWidth;
        canvas1.height = window.innerHeight;
    });

    var context1 = canvas1.getContext('2d');

    // ---

    var text_slide2 = document.getElementById('m-text-slide2');
    var text_slide2_message = document.getElementById('m-text-slide2-message');

    var canvas2 = document.getElementById('graphics2');
    canvas2.width = GET_GRAPHICS_WIDTH();
    canvas2.height = GET_GRAPHICS_HEIGHT();
    $(window).bind('resize', function(event) {
        canvas2.width = GET_GRAPHICS_WIDTH();
        canvas2.height = GET_GRAPHICS_HEIGHT();
    });

    var context2 = canvas2.getContext('2d');

    // ---

    var fps = new FPS();

    // ---

    var transitions_base = GET_TRANSITIONS_BASE(pixels);
    var transitions_border = GET_TRANSITIONS_BORDER(pixels);
    var transitions_images = GET_TRANSITIONS_IMAGES(pixels);
    var transitions_waveform = GET_TRANSITIONS_WAVEFORM(pixels);
    var components = GET_COMPONENTS(canvas1, context1, transitions_base, transitions_border, transitions_images, transitions_waveform, pixels);
    var slideshows = GET_SLIDESHOWS(canvas2, context2, transitions_base, transitions_border, transitions_images, transitions_waveform, pixels);

    // ---

    console.log("pixels", pixels.length);

    // ---

    canvas1.onmousemove = function(event) {
        var mouseX = event.pageX - canvas1.offsetLeft;
        var mouseY = event.pageY - canvas1.offsetTop;

        for (var i = 0; i < components.length; i++) {
            components[i].mx = mouseX;
            components[i].my = mouseY;
        }
    };

    canvas1.onmousedown = function() {

    };

    // ---

    var frame = 0;

    var messageFrame = 0;

    var ended = false;
    var endedFrame = 0;


    for (var i = 0; i < slideshows.length; i++) {
        slideshows[i].start = function(objects, from, to) {
            if (ended) {
                endedFrame++;
                if (endedFrame > 3) {
                    $('.m-screen-button').css('padding', '0px 15px').find('span').hide();
                }
            }

            if (frame === 0) {
                setTimeout(function() {
                    if ($(document.body).hasClass('screen_min') || $(document.body).hasClass('screen_normal')) {
                        $(text_slide2).fadeOut(1500);
                    } else if ($(document.body).hasClass('screen_max')) {
                        $(text_slide1).fadeOut(1500);
                    }
                }, 1000);
            } else {
                if (frame % 3 === 0) {
                    TEXT_SLIDE(GLOBAL_MESSAGES[frame++ % GLOBAL_MESSAGES.length]);

                    setTimeout(function() {
                        if ($(document.body).hasClass('screen_min') || $(document.body).hasClass('screen_normal')) {
                            $(text_slide2).fadeOut(1000);
                        } else if ($(document.body).hasClass('screen_max')) {
                            $(text_slide1).fadeOut(1000);
                        }
                    }, 2000);

                    if ($(document.body).hasClass('screen_min') || $(document.body).hasClass('screen_normal')) {
                        $(text_slide2).fadeIn(1000);
                    } else if ($(document.body).hasClass('screen_max')) {
                        $(text_slide1).fadeIn(1000);
                    }
                }
            }

            frame++;

            frame %= 10000;

            for (var j = 0; j < components.length; j++) {
                components[j].start(objects, from, to);
            }
        };
        slideshows[i].beforeChange = function(objects, from, to) {
            for (var j = 0; j < components.length; j++) {
                components[j].beforeChange(objects, from, to);
            }
        };
        slideshows[i].change = function(objects, from, to) {
            for (var j = 0; j < components.length; j++) {
                components[j].change(objects, from, to);
            }
        };
        slideshows[i].afterChange = function(objects, from, to) {
            for (var j = 0; j < components.length; j++) {
                components[j].afterChange(objects, from, to);
            }
        };
        slideshows[i].end = function(objects, from, to) {
            ended = true;

            $('.m-screen-button').css('padding', '30px 50px 30px 50px').show();

            for (var j = 0; j < components.length; j++) {
                components[j].end(objects, from, to);
            }
        };
        slideshows[i].loaded = function() {
            console.log("loaded slideshow");
        };
    }

    // ---

    (function loop() {
        var componentsCount = components.length, slideshowsCount = slideshows.length, i;

        if (render) {
            for (i = 0; i < componentsCount; i++) {
                components[i].render = true;
                components[i].update();
            }

            for (i = 0; i < slideshowsCount; i++) {
                slideshows[i].render = true;
                slideshows[i].update();
            }

            // --- clear

            context1.clearRect(0, 0, canvas1.width, canvas1.height);
            context2.clearRect(0, 0, canvas2.width, canvas2.height);

            // ---

            for (i = 0; i < componentsCount; i++) {
                components[i].draw();
            }

            for (i = 0; i < slideshowsCount; i++) {
                slideshows[i].draw();
            }

            // ---

            fps.exec();

        } else {
            for (i = 0; i < componentsCount; i++) {
                components[i].render = false;
            }

            for (i = 0; i < slideshowsCount; i++) {
                slideshows[i].render = false;
            }
        }

        // --- loop

        requestAnimationFrame(loop);
    })();

    // --- 

    var graphics1 = $(canvas1);
    var graphics2 = $(canvas2);

    var spans = $('.m-button-action:not(.m-like-button):not(.m-comment-button) span');

    var header = $('.m-header');
    var footer = $('.m-footer');

    var login_button = $('.m-login');

    var like_button = $('.m-like-button');
    var comment_button = $('.m-comment-button');

    var ios_button = $('.button-ios');

    var likes_button = $('.m-likes');
    var comments_button = $('.m-comments');

    var likes_container = $('.m-likes-container');
    var comments_container = $('.m-comments-container');

    var base_min_graphics = $('.m-base-min-graphics');
    var base_max_graphics = $('.m-base-max-graphics');

    var video = $('.m-video');
    var video_play = $('.m-video-play');
    var video_stop = $('.m-video-stop');
    var video_fullscreen = $('.m-video-fullscreen');

    var bg_normal_container = $('.m-bg-normal-container');
    var bg_blur_container = $('.m-bg-blur-container');

    var graphics_container_min_normal = $('.graphics-container-min.graphics-container-normal');
    var graphics_container_fullscreen = $('.graphics-container-fullscreen');

    // ---

    login_button.click(function(event) {
        console.log('login_button');

        ga('send', 'click', 'button', 'login');

        var action = function() {
            console.log('action');
        };


        window.fb_connection.getStatus(function(ok) {
            console.log('status', ok);

            var friendsInfo = function(response) {
                console.log('friendsInfo', response);

                window.fb_FriendsInfo = response;
            };

            var myInfo = function(response) {
                console.log('myInfo', response);

                window.fb_MyInfo = response;

                window.fb_connection.getFriendsInfo(friendsInfo);

                action();
            };

            if (ok) {
                window.fb_connection.getMyInfo(myInfo);
            } else {
                window.fb_connection.login(function(ok) {
                    console.log('login', ok);

                    if (ok) {
                        window.fb_connection.getMyInfo(myInfo);
                    } else {

                    }
                });
            }
        });
    });

    like_button.click(function(event) {
        console.log('like_button');

        ga('send', 'click', 'button', 'likes');
    });

    comment_button.click(function(event) {
        console.log('comment_button');

        ga('send', 'click', 'button', 'comments');
    });

    ios_button.click(function(event) {
        console.log('ios_button');

        ga('send', 'click', 'button', 'ios');
    });

    // ---

    window.PLAY = function() {
        render = true;

        $(document.body).addClass('render');

        if (dancer) {
            dancer.loaded = function() {
                this.play();
            };

            if (dancer.isLoaded()) {
                dancer.loaded();
            }
        }

        video_play.hide();
        video_stop.show();
    };

    window.STOP = function() {
        render = false;

        $(document.body).removeClass('render');
        if (dancer) {
            dancer.pause();
        }

        video_play.show();
        video_stop.hide();
    };

    window.CHECK_SIZE = function() {
        for (var i = 0, count = slideshows.length; i < count; i++) {
            slideshows[i].restart();
        }
    };

    window.CHECH_FULLSCREAN = function() {
        $(document.body).removeClass('screen_min screen_normal screen_max');

        if (FULLSCREEN) {
            graphics2.remove();
            graphics_container_min_normal.empty();
            graphics_container_fullscreen.html(graphics2);

            bg_normal_container.hide();

            $(document.body).addClass('screen_max');

            //likes_button.hide();
            //comments_button.hide();
            like_button.hide();
            comment_button.hide();
            header.hide();
            footer.hide();
        } else {
            graphics2.remove();
            graphics_container_min_normal.html(graphics2);
            graphics_container_fullscreen.empty();

            bg_normal_container.show();

            $(document.body).addClass('screen_normal');

            //likes_button.show();
            //comments_button.show();
            like_button.show();
            comment_button.show();

            header.show();
            footer.show();

            CHECH_RESIZE();
        }

        CHECK_SIZE();
    };

    window.CHECH_RESIZE = function() {
        if (FULLSCREEN) {
            return;
        }

        var hasMinClass = $(document.body).hasClass('screen_min');
        var hasNormalClass = $(document.body).hasClass('screen_normal');
        var hasMaxClass = $(document.body).hasClass('screen_max');
        var first = $(document.body).hasClass('first');

        var resize = window.innerWidth <= 854;
        if (resize && (!hasMinClass || first)) {
            $(document.body).removeClass('first');

            $(document.body).removeClass('screen_min screen_normal screen_max');

            $(document.body).addClass('screen_min');

            CHECK_SIZE();

            spans.hide();

            //like_button.css('display', 'inline-block');
            //comment_button.css('display', 'inline-block');
            //likes_button.hide();
            //comments_button.hide();
        }
        if (!resize && (!hasNormalClass || first)) {
            $(document.body).removeClass('first');

            $(document.body).removeClass('screen_min screen_normal screen_max');

            $(document.body).addClass('screen_normal');

            CHECK_SIZE();

            spans.show();

            //like_button.css('display', 'none');
            //comment_button.css('display', 'none');
            //likes_button.show();
            //comments_button.show();
        }
    };

    window.TEXT_SLIDE = function(text) {
        text_slide1_message.innerHTML = text;
        text_slide2_message.innerHTML = text;
    };

    // ---

    video_play.click(function(event) {
        $(document.body).addClass('render');

        PLAY();

        return false;
    }).mouseover(function(event) {

    }).mouseout(function(event) {

    });

    video_stop.click(function(event) {
        if ($(document.body).hasClass('render') && ($(document.body).hasClass('screen_min') || $(document.body).hasClass('screen_normal'))) {

            STOP();

            return false;
        }
    }).mouseover(function(event) {

    }).mouseout(function(event) {

    });

    video_fullscreen.click(function(event) {
        FULLSCREEN = !FULLSCREEN;

        CHECH_FULLSCREAN();

        $(window).resize();

        return false;
    });

    $(document).click(function(event) {
        if ($(document.body).hasClass('render') && $(document.body).hasClass('screen_max')) {

            STOP();

            return false;
        }
    });

    $(document).bind('keyup', function(e) {
        console.log(e);

        if (e.keyCode === 13) { // esc

        }
        if (e.keyCode === 27) { // esc
            if (FULLSCREEN) {
                FULLSCREEN = !FULLSCREEN;

                CHECH_FULLSCREAN();

                $(window).resize();
            }
        }
    });

    $(document).mousemove(function(event) {
        if (FULLSCREEN && render) {

        }
    }).mouseover(function(event) {
        if (FULLSCREEN && render) {
            video_stop.show();
        }
    }).mouseout(function(event) {
        if (FULLSCREEN && render) {
            video_stop.hide();
        }
    });

    video.mousemove(function(event) {
        if (!FULLSCREEN && render) {

        }
    }).mouseover(function(event) {
        if (!FULLSCREEN && render) {
            video_stop.show();
        }
    }).mouseout(function(event) {
        if (!FULLSCREEN && render) {
            video_stop.hide();
        }
    });

    // ---

    $(window).bind('resize', function(event) {
        CHECH_RESIZE();

        CHECH_FULLSCREAN();
    });

    FULLSCREEN = GLOBAL_TEMPLATE === -1 || GLOBAL_TEMPLATE === 3;

    if (FULLSCREEN) {
        setTimeout(function() {
            $(window).trigger('resize');

            canvas2.width = GET_GRAPHICS_WIDTH();
            canvas2.height = GET_GRAPHICS_HEIGHT();
        }, 1000);
    }

    // ---

    TEXT_SLIDE('MINUTTA WITH LOVE');
});