(function($) {
    var ModelComment = function() {
        var self = this;

        return self;
    };

    var Comments = function(options) {
        options = options || {};

        var self = this;

        // --- private

        var _lastComment = null;

        // --- constants

        var CONST_PREFIX = '{PREFIX}';
        var CONST_AUTHOR = '{AUTHOR}';
        var CONST_TEXT = '{TEXT}';
        var CONST_DATE = '{DATE}';

        // --- variables

        var showButton = options.showButton || $('.comments_show_button');
        var hideButton = options.hideButton || $('.comments_hide_button');
        var container = options.container || $('.comments_container');
        var counter = options.counter || $('.comments_counter');
        var counter2 = options.counter2 || $('.comments_counter2');
        var list = options.list || $('.comments_list');
        var area = options.area || $('.comments_area');
        var send = options.send || $('.comments_send');

        // --- functions

        self.added = function() {

        };

        // --- actions

        if (showButton && showButton.length) {
            showButton.click(function() {
                ga('send', 'click', 'button', 'comments_show');

                self.panelShow();
                return false;
            });
        }

        if (hideButton && hideButton.length) {
            hideButton.click(function() {
                ga('send', 'click', 'button', 'comments_hide');

                self.panelHide();
                return false;
            });
        }

        if (container && container.length) {
            container.click(function() {
                return false;
            });
        }

        if (counter && counter.length) {
            counter.click(function() {
                return false;
            });
        }

        if (counter2 && counter2.length) {
            counter2.click(function() {
                return false;
            });
        }

        if (list && list.length) {
            list.click(function() {
                return false;
            });
        }

        if (area && area.length) {
            area.click(function() {
                return false;
            });
        }

        if (send && send.length) {
            send.click(function() {
                ga('send', 'click', 'button', 'send');

                console.log('send', window.fb_MyInfo, window.fb_FriendsInfo);

                var action = function() {
                    console.log('action');

                    self.addComment({
                        author: 'Me',
                        my: 1,
                        text: area.val()
                    });

                    area.val('');

                    if (self.added) {
                        self.added.call(self, self.lastComment());
                    }
                };

                if (window.fb_FriendsInfo) {

                } else {

                }

                if (window.fb_MyInfo) {
                    action();
                } else {
                    console.log('window.fb_connection', window.fb_connection);

                    window.fb_connection.getStatus(function(ok) {
                        console.log('status', ok);

                        var friendsInfo = function(response) {
                            console.log('friendsInfo', response);

                            window.fb_FriendsInfo = response;
                        };

                        var myInfo = function(response) {
                            console.log('myInfo', response);

                            window.fb_MyInfo = response;

                            window.fb_connection.getFriendsInfo(friendsInfo);

                            action();
                        };

                        if (ok) {
                            window.fb_connection.getMyInfo(myInfo);
                        } else {
                            window.fb_connection.login(function(ok) {
                                console.log('login', ok);

                                if (ok) {
                                    window.fb_connection.getMyInfo(myInfo);
                                } else {

                                }
                            });
                        }
                    });
                }

                return false;
            });
        }

        // --- methods

        self.template = function() {
            return ['<li>',
                '<div class="m-comment-user-cover">',
                '<img src="', CONST_PREFIX, 'images/user_thumb.png">',
                '</div>',
                '<div class="m-comment-user-message">',
                '<div class="m-comment-user-author">', CONST_AUTHOR, '</div>',
                '<div class="m-comment-user-text">', CONST_TEXT, '</div>',
                '<div class="m-comment-user-data">', CONST_DATE, '</div>',
                '</div>',
                '</li>'].join('');
        };

        self.addComment = function(comment) {
            comment = comment || {};

            var result = '';

            comment.id = comment.id || ('comment_new_' + self.size());
            comment.prefix = comment.prefix || PREFIX;
            comment.cover = comment.cover || '';
            comment.author = comment.author || 'Unknown';
            comment.text = comment.text || area.val();
            comment.date = comment.date || self.date(new Date().getTime());
            comment.my = comment.my || 0;

            var _id = comment.id;
            var _prefix = comment.prefix;
            var _cover = comment.cover;
            var _author = comment.author;
            var _text = comment.text;
            var _date = comment.date;
            var _my = comment.my;

            // ---

            _lastComment = comment;

            // ---

            var _template = $(self.template()
                    .replace(CONST_PREFIX, _prefix)
                    .replace(CONST_AUTHOR, _author)
                    .replace(CONST_TEXT, _text)
                    .replace(CONST_DATE, _date));

            _template.attr('id', _id);
            _template.addClass(function() {
                return _my ? 'comment_my' : '';
            });

            list.append(_template);

            self.check();

            return result;
        };

        self.removeComment = function(comment) {
            comment = comment || {};
        };
        self.removeAll = function() {
            list.empty();
        };

        self.lastComment = function() {
            return _lastComment;
        };

        self.panelShow = function() {
            container.animate({right: 0}, 500);
        };
        self.panelHide = function() {
            container.animate({right: -container.width()}, 500);
        };
        self.size = function() {
            return list.find('li').size();
        };
        self.check = function() {
            var _count = self.size();

            counter.html(_count <= 0 ? 'Оставить комментарий' : _count + composition(_count, ' комментар', 'ий', 'ия', 'иев'));
            counter2.html(_count);
        };
        self.date = function(time) {
            moment.lang('ru');
            return moment(new Date(time)).format('LLLL');
        };
        self.fromJSON = function(json) {
            json = json || {};
            if (json && json.hasOwnProperty('comments') && json.comments.length) {
                self.removeAll();

                $.each(json.comments, function(index, comment) {
                    var user = comment.user || {};

                    self.addComment({
                        text: comment.comment,
                        date: self.date(comment.created * 1000),
                        id: 'comment_' + comment.id,
                        author: $.trim((user.first_name || '') + ' ' + (user.last_name || ''))
                    });
                });
            }
        };
        self.toJSON = function() {
            var result = '';

            return result;
        };

        return self;
    };

    // ---

    var server_url = GLOBAL_SERVER_URL;

    var comments = new Comments({
        showButton: $('.m-comments, .m-comment-button'),
        hideButton: null,
        container: $('.m-comments-container'),
        counter: $('.m-comments-container .comments-counter'),
        counter2: $('#m-comments-counter'),
        list: $('#m-comments-list ul'),
        area: $('.m-comment-area > textarea'),
        send: $('#m-send-comment > div')
    });

    comments.added = function(comment) {
        console.log('comment', comment);
        if (comment) {
            var request = {
                action: 'proto.addComment',
                pid: GLOBAL_PID || 0,
                comment: comment.text,
                test: 1
            };

            $.post(server_url, request, function(data) {
                console.log('data', data);

                comments.fromJSON(data);
            });
        }
    };

    if (GLOBAL_COMMENTS && GLOBAL_COMMENTS.length) {
        $.each(GLOBAL_COMMENTS, function(index, comment) {
            if (comment) {
                comments.addComment(comment);
            }
        });

        comments.check();
    }

    $(document).click(function(event) {
        comments.panelHide();
    });

    $.post(server_url, {action: 'proto.getComments', pid: GLOBAL_PID || 0, test: 1}, function(data) {
        console.log('data', data);

        comments.fromJSON(data);
    });

})(jQuery);