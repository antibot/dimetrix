function GET_POINTS_TRANSITIONS(pixels, points_struct) {
    return function() {
        var points = points_struct.points();

        var size = points_struct.size();

        var k = points_struct.k();

        var factor = Math.max(1, Math.random() + 0.5);

        var offsetX = points_struct.offsetX();
        var offsetY = points_struct.offsetY();

        offsetX = 2 * offsetX - offsetX * factor;
        offsetY = 2 * offsetY - offsetY * factor;

        for (var i = 0; i < pixels.length; i++) {
            var p = pixels[i];
            if (p.flightMode !== 2) {
                p.toX = points[Math.floor(i * k) % points.length][0] * factor + offsetX;
                p.toY = points[Math.floor(i * k) % points.length][1] * factor + offsetY;
                p.speedX = (Math.random() - 0.5) / 2;
                p.speedY = (Math.random() - 0.5) / 2;
            }
        }

        return true;
    };
}

function GET_TRANSITIONS_BASE(pixels) {
    if (!pixels) {
        return [];
    }

    var transitions = [];

    transitions.push(function() {
        for (var i = 0; i < pixels.length; i++) {
            var p = pixels[i];
            if (p.flightMode !== 2) {
                p.toX = Math.random() * window.innerWidth;
                p.toY = Math.random() * window.innerHeight;
                p.speedX = Math.cos(p.angle) * Math.random() * 3;
                p.speedY = Math.sin(p.angle) * Math.random() * 3;
            }
        }

        return false;
    });

    /*
    transitions.push(function() {
        for (var i = 0; i < pixels.length; i++) {
            var p = pixels[i];
            if (p.flightMode !== 2) {
                p.r = 255;
                p.g = 255;
                p.b = 255;
                p.size = RANDOM(window.SCALE_MIN, window.SCALE_MAX);
            }
        }

        return false;
    });
    */
    transitions.push(function() {
        for (var i = 0; i < pixels.length; i++) {
            var p = pixels[i];
            if (p.flightMode !== 2) {
                p.toSize = RANDOM(window.POINT_MIN, window.POINT_MAX);
            }
        }

        return false;
    });

    return transitions;
}

function GET_TRANSITIONS_BORDER(pixels) {
    if (!pixels) {
        return [];
    }

    var transitions = [];

    transitions.push(function() {
        for (var i = 0; i < pixels.length; i++) {
            var pixel = pixels[i];
            if (pixel.flightMode !== 2) {
                switch (i % 4) {
                    case 0:
                        pixel.toX = RANDOM(0, 10);
                        pixel.toY = Math.random() * GET_GRAPHICS_HEIGHT();
                        break;
                    case 1:
                        pixel.toX = RANDOM(GET_GRAPHICS_WIDTH() - 10, GET_GRAPHICS_WIDTH() - 5);
                        pixel.toY = Math.random() * GET_GRAPHICS_HEIGHT() - 5;
                        break;
                    case 2:
                        pixel.toX = Math.random() * GET_GRAPHICS_WIDTH() - 5;
                        pixel.toY = RANDOM(5, 10);
                        break;
                    case 3:
                        pixel.toX = Math.random() * GET_GRAPHICS_WIDTH() - 5;
                        pixel.toY = RANDOM(GET_GRAPHICS_HEIGHT() - 10, GET_GRAPHICS_HEIGHT() - 5);
                        break;
                    default:
                        break;
                }

                pixel.speedX = Math.cos(pixel.angle) * (Math.random() - 0.5) / 2;
                pixel.speedY = Math.sin(pixel.angle) * (Math.random() - 0.5) / 2;
                pixel.toSize = RANDOM(window.POINT_MIN, window.POINT_MAX);
            }
        }

        return true;
    });

    return transitions;
}

function GET_TRANSITIONS_IMAGES(pixels) {
    if (!pixels) {
        return [];
    }

    var transitions = [];

    transitions.push(function() {
        var part = Math.min(window.innerWidth, window.innerHeight) / 4;
        var r = Math.floor(Math.random() * part + part);
        for (var i = 0; i < pixels.length; i++) {
            var p = pixels[i];
            if (p.flightMode !== 2) {
                p.toSize = RANDOM(window.POINT_MIN, window.POINT_MAX);
                p.toX = window.innerWidth / 2 + Math.cos(i * 3.6 * Math.PI / 180) * r;
                p.toY = window.innerHeight / 2 + Math.sin(i * 3.6 * Math.PI / 180) * r;
                p.speedX = (Math.random() - 0.5) / 2;
                p.speedY = (Math.random() - 0.5) / 2;
                p.toR = Math.random() * 255;
                p.toG = Math.random() * 255;
                p.toB = Math.random() * 255;
            }
        }

        return true;
    });

    var POINTS_STRUCT = GET_POINTS();

    for (var i = 0; i < POINTS_STRUCT.length; i++) {
        transitions.push(GET_POINTS_TRANSITIONS(pixels, POINTS_STRUCT[i]));
    }

    return transitions;
}

function GET_TRANSITIONS_WAVEFORM(pixels) {
    if (!pixels) {
        return [];
    }

    var transitions = [];

    transitions.push(function() {
        var dancer = window.dancer || {};
        var waveform = "getWaveform" in dancer ? (dancer.getWaveform() || []) : [];

        var w = window.innerWidth;
        var h = window.innerHeight;

        var offsetX = 50;
        var offsetY = 0;

        var k = (window.innerWidth - offsetX * 2) / pixels.length;

        for (var i = 0; i < pixels.length; i++) {
            var p = pixels[i];
            if (p.flightMode !== 2) {
                p.toX = i * k + offsetX;
                p.toY = (h / 2) + waveform[(i % waveform.length)] * (h / 2);
                p.speedX = (Math.random() - 0.5) / 2;
                p.speedY = (Math.random() - 0.5) / 2;
            }
        }

        return true;
    });

    return transitions;
}


