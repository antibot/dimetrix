function GET_MESSAGES() {
    var result = [];

    result.push("Hi, my name is <span class='partition'>Dimetrix</span><br /> I'm <span class='partition'>developer</span>");
    result.push("Mobile, Desktop, WEB, Console, TV, Radio <span class='partition'>applications</span><br /> Client side, server side");
    result.push("Contact with me<br>Email: <span class='partition contacts'>hi@dimetrix.ru</span><br />Facebook: <a href='https://www.facebook.com/dimetrix.ru' target='_blank' class='partition contacts'>facebook.com</a>");

    return result;
}