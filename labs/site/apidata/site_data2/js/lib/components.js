function GET_COMPONENTS(canvas, context, transitions_base, transitions_border, transitions_images, transitions_waveform, pixels) {
    if (!canvas || !context || !transitions_base || !transitions_border || !transitions_images || !transitions_waveform || !pixels) {
        return [];
    }

    var PI2 = Math.PI * 2;

    var Universe = function Pixels() {
        return {
            render: true,
            TYPE_NONE: 3,
            TYPE_CIRCLE: 1,
            TYPE_STAR: 0,
            TYPE_HEART: 2,
            type: GLOBAL_TEMPLATE || 0,
            currentType: GLOBAL_TEMPLATE || 0,
            mx: 0,
            my: 0,
            impulsX: 0,
            impulsY: 0,
            impulsToX: 0,
            impulsToY: 0,
            startEffectsTime: new Date(),
            startEffectsWaveformTime: new Date(),
            indexesForBaseEffects: [0, 1, 2, 3, 5, 6, 7, 8, 10, 11],
            indexesForImagesEffects: [4, 9],
            indexesForWaveformEffects: FILL_ARRAY(12, 300),
            effectsIndex: 0,
            focusedParticleIndex: null,
            template: function(type) {
                this.type = type;
                this.currentType = type;
            },
            start: function(objects, from, to) {
                if (!$(document.body).hasClass('screen_max')) {
                    return;
                }

                console.log('start slideshow');

                var object = objects[from];

                console.log('object.index', object.index);

                if (!window.RANDOMIZE_COLOR) {
                    $.each(pixels, function(index, pixel) {
                        var rgb = hexToRGB(object.colors[index % object.colors.length]);

                        pixel.toR = rgb.r;
                        pixel.toG = rgb.g;
                        pixel.toB = rgb.b;
                    });
                }

                if (transitions_images.length && transitions_images[1]()) {
                    this.impulsX = 0;
                    this.impulsY = 0;
                }
            },
            beforeChange: function(objects, from, to) {
                console.log('beforeChange slideshow');

                if (!$(document.body).hasClass('screen_max')) {
                    return;
                }

                var object = objects[to];

                console.log('object.index', object.index);

                if (!window.RANDOMIZE_COLOR) {
                    $.each(pixels, function(index, pixel) {
                        var rgb = hexToRGB(object.colors[index % object.colors.length]);

                        pixel.toR = rgb.r;
                        pixel.toG = rgb.g;
                        pixel.toB = rgb.b;
                    });
                }

                if (object.fade_time === window.FADE_DURATION_TRANSITION) {
                    //if (transitions_base.length && transitions_base[Math.floor(Math.random() * transitions_base.length)]()) {
                    //this.impulsX = 0;
                    //this.impulsY = 0;
                    //}

                    for (var i = 0; i < pixels.length; i++) {
                        var pixel = pixels[i];
                        if (pixel.flightMode !== 2) {
                            pixel.toX = Math.random() * GET_GRAPHICS_WIDTH();
                            pixel.toY = Math.random() * GET_GRAPHICS_HEIGHT();
                            pixel.speedX = Math.cos(pixel.angle) * Math.random() * 3;
                            pixel.speedY = Math.sin(pixel.angle) * Math.random() * 3;
                            pixel.toSize = RANDOM(window.SCALE_MIN, window.SCALE_MAX);
                        }
                    }

                } else if (object.fade_time === window.FADE_DURATION_IMAGE) {
                    if (transitions_images.length && transitions_images[Math.floor(Math.random() * transitions_images.length)]()) {
                        this.impulsX = 0;
                        this.impulsY = 0;
                    }
                } else {

                }
            },
            change: function(objects, from, to) {
                console.log('change slideshow');

                if (!$(document.body).hasClass('screen_max')) {
                    return;
                }

                if (transitions_border.length && transitions_border[Math.floor(Math.random() * transitions_border.length)]()) {
                    this.impulsX = 0;
                    this.impulsY = 0;
                }

                this.changeIndex++;
            },
            afterChange: function(objects, from, to) {
                console.log('afterChange slideshow');

                if (!$(document.body).hasClass('screen_max')) {
                    return;
                }

                if (this.changeIndex) {

                } else {
                    if (transitions_border.length && transitions_border[Math.floor(Math.random() * transitions_border.length)]()) {
                        this.impulsX = 0;
                        this.impulsY = 0;
                    }
                }
            },
            end: function(objects, from, to) {
                console.log('end slideshow');

                if (!$(document.body).hasClass('screen_max')) {
                    return;
                }
            },
            update: function() {
                if (!this.render || this.currentType === this.TYPE_NONE) {
                    return;
                }

                this.impulsX = this.impulsX + (this.impulsToX - this.impulsX) / 30;
                this.impulsY = this.impulsY + (this.impulsToY - this.impulsY) / 30;

                // move to tox
                for (var i = 0; i < pixels.length; i++) {
                    var p = pixels[i];
                    p.x = p.x + (p.toX - p.x) / 10;
                    p.y = p.y + (p.toY - p.y) / 10;
                    p.size = p.size + (p.toSize - p.size) / 10;

                    p.r = p.r + (p.toR - p.r) / 10;
                    p.g = p.g + (p.toG - p.g) / 10;
                    p.b = p.b + (p.toB - p.b) / 10;
                }

                // update speed
                for (var i = 0; i < pixels.length; i++) {
                    var p = pixels[i];
                    // check for flightmode
                    var a = Math.abs(p.toX - this.mx) * Math.abs(p.toX - this.mx);
                    var b = Math.abs(p.toY - this.my) * Math.abs(p.toY - this.my);
                    var c = Math.sqrt(a + b);

                    if (p.flightMode !== 2) {
                        if (c < 120) {
                            if (p.flightMode === 0) {
                                var alpha = Math.atan2(p.y - this.my, p.x - this.mx) * 180 / Math.PI + Math.random() * 180 - 90;
                                p.degree = alpha;
                                p.degreeSpeed = Math.random() * 1 + 0.5;
                                p.frame = 0;
                            }
                            p.flightMode = 1;
                        } else {
                            p.flightMode = 0;
                        }
                    }

                    // random movement
                    if (p.flightMode === 0) {
                        // change position
                        p.toX += p.speedX;
                        p.toY += p.speedY;

                        // check for bounds
                        if (p.x < 0) {
                            p.x = window.innerWidth;
                            p.toX = window.innerWidth;
                        }
                        if (p.x > window.innerWidth) {
                            p.x = 0;
                            p.toX = 0;
                        }

                        if (p.y < 0) {
                            p.y = window.innerHeight;
                            p.toY = window.innerHeight;
                        }
                        if (p.y > window.innerHeight) {
                            p.y = 0;
                            p.toY = 0;
                        }
                    }

                    // seek mouse
                    /*
                     if (p.flightMode === 1) {
                     p.toX = this.mx + Math.cos((p.degree + p.frame) % 360 * Math.PI / 180) * c;
                     p.toY = this.my + Math.sin((p.degree + p.frame) % 360 * Math.PI / 180) * c;
                     p.frame += p.degreeSpeed;
                     p.degreeSpeed += 0.01;
                     }*/

                    if (p.flightMode !== 2) {
                        // add impuls
                        p.toX += Math.floor(this.impulsX * p.size / 30);
                        p.toY += Math.floor(this.impulsY * p.size / 30);
                    }
                }

                // set an choord
                var r1 = Math.floor(Math.random() * pixels.length);
                var r2 = Math.floor(Math.random() * pixels.length);

                if (pixels[r1].flightMode !== 2) {
                    pixels[r1].size = RANDOM(window.FLICKER_MIN, window.FLICKER_MAX);
                }
                if (pixels[r2].flightMode !== 2) {
                    pixels[r2].size = RANDOM(window.FLICKER_MIN, window.FLICKER_MAX);
                }

                var dancer = window.dancer || {};
                var waveform = "getWaveform" in dancer ? (dancer.getWaveform() || []) : [];
                var hasWaveform = !IS_NULL_ARRAY(waveform);

                if (!$(document.body).hasClass('screen_max')) {
                    if (this.indexesForWaveformEffects.indexOf(this.effectsIndex) !== -1 && hasWaveform) {
                        if (Date.now() - this.startEffectsWaveformTime.getTime() > (this.effectsIndex === this.indexesForWaveformEffects[0] ? 2500 : 50)) {
                            this.startEffectsWaveformTime = new Date();

                            if (transitions_waveform[Math.floor(Math.random() * transitions_waveform.length)]()) {
                                this.impulsX = 0;
                                this.impulsY = 0;
                            }

                            this.startEffectsTime = new Date();

                            this.effectsIndex++;
                        }
                    } else {

                        if (Date.now() - this.startEffectsTime.getTime() > 2000) {
                            this.startEffectsTime = new Date();

                            this.effectsIndex = (hasWaveform ? this.effectsIndex : ((this.effectsIndex > this.indexesForBaseEffects[this.indexesForBaseEffects.length - 1] && this.effectsIndex > this.indexesForImagesEffects[this.indexesForBaseEffects.length - 1]) ? 0 : this.effectsIndex));

                            this.impulsX = Math.random() * 800 - 400;
                            this.impulsY = -Math.random() * 400;

                            if (this.indexesForBaseEffects.indexOf(this.effectsIndex) !== -1) {
                                if (transitions_base[Math.floor(Math.random() * transitions_base.length)]()) {
                                    this.impulsX = 0;
                                    this.impulsY = 0;
                                }
                            } else if (this.indexesForImagesEffects.indexOf(this.effectsIndex) !== -1) {
                                if (transitions_images[Math.floor(Math.random() * transitions_images.length)]()) {
                                    this.impulsX = 0;
                                    this.impulsY = 0;
                                }
                            }

                            this.startEffectsWaveformTime = new Date();

                            this.effectsIndex++;
                        }
                    }

                }

                this.effectsIndex = (this.effectsIndex > (this.indexesForBaseEffects.length + this.indexesForImagesEffects.length + this.indexesForWaveformEffects.length) ? 0 : this.effectsIndex);
            },
            draw: function() {
                if (!this.render || this.currentType === this.TYPE_NONE) {
                    return;
                }

                var count = pixels.length, i;
                for (i = 0; i < count; i++) {
                    var pixel = pixels[i];

                    var pixel = pixels[i];

                    var x = pixel.x;
                    var y = pixel.y;

                    var size = pixel.size;
                    var color = decs2hex(pixel.r, pixel.g, pixel.b);

                    context.fillStyle = color;

                    // ---

                    switch (this.currentType) {
                        case -1:
                        case this.TYPE_NONE:
                            break;
                        case this.TYPE_CIRCLE:
                            context.beginPath();
                            context.arc(x, y, size, 0, PI2, false);
                            context.closePath();
                            context.fill();
                            break;
                        case this.TYPE_STAR:
                            context.beginPath();
                            for (var j = 0; j <= 10; j++) {
                                var radius = (j % 2 > 0 ? size : size * .5);
                                var px = Math.cos(Math.PI * .5 + Math.PI / 5 * j) * radius;
                                var py = Math.sin(Math.PI * .5 + Math.PI / 5 * j) * radius;
                                context.lineTo(px + x, py + y);
                            }
                            context.closePath();
                            context.fill();
                            break;
                        case this.TYPE_HEART:
                            context.beginPath();
                            context.lineTo(x, y);
                            context.bezierCurveTo(x + 2, y - size, x + (size * 2), y, x, y + size);
                            context.lineTo(x, y);
                            context.bezierCurveTo(x + 2, y - size, x - (size * 2), y, x, y + size);
                            context.closePath();
                            context.fill();
                            break;
                        default:

                            break;
                    }
                }
            }
        };
    };

    return [new Universe()];
}