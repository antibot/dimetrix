jQuery(function() {

    console.log("Dancer.isSupported()", Dancer.isSupported());

    // ---

    var body = $(document.body);

    // ---

    if (!Dancer.isSupported()) {
        return;
    }

    var index = 0;
    var interval = 0;
    var attempts = 0;

    var AUDIO_FILES = [];

    $.each(GLOBAL_SOUNDS, function(index, sound) {
        AUDIO_FILES.push(sound.replace('.mp3', ''));
    });

    // ---

    Dancer.setOptions({
        flashSWF: PREFIX + '/js/dancer/lib/soundmanager2.swf',
        flashJS: PREFIX + '/js/dancer/lib/soundmanager2.js'
    });

    // ---

    function build() {
        body.addClass('loading');

        attempts = 0;

        clearInterval(interval);

        dancer = new Dancer();
        dancer.loaded = function() {
            console.log("loaded");

            body.removeClass('loading');

            //this.play();
        };
        dancer.load({src: AUDIO_FILES[index++ % AUDIO_FILES.length], codecs: ['mp3']});

        Dancer.isSupported() || dancer.loaded();

        console.log("Dancer.isSupported()", Dancer.isSupported(), "dancer.isLoaded()", dancer.isLoaded());

        !dancer.isLoaded() ? dancer.bind('loaded', dancer.loaded) : dancer.loaded();

        window.dancer = dancer;

        interval = setInterval(function() {
            if (dancer.isLoaded() && IS_NULL_ARRAY(dancer.getWaveform())) {
                if (attempts > 10) {
                    console.log("rebuild");

                    clearInterval(interval);

                    build();
                }

                attempts++;
            }
        }, 1000);
    }

    // ---

    build();

    // ---

    $(document).mousewheel(function(event) {
        if (dancer) {
            dancer.setVolume(event.deltaY > 0 ? Math.min(1, dancer.getVolume() + 0.1) : Math.max(0, dancer.getVolume() - 0.1));
        }
    });
});