function GET_POINTS_TRANSITIONS(pixels, points_struct) {
    return function() {
        var points = points_struct.points();

        var size = points_struct.size();

        var k = points_struct.k();

        var factor = RANDOM(1, 1.6);

        var offsetX = points_struct.offsetX();
        var offsetY = points_struct.offsetY();

        offsetX += size[0] * (1 - factor) / 2;
        offsetY += size[1] * (1 - factor) / 2;

        for (var i = 0; i < pixels.length; i++) {
            var pixel = pixels[i];
            if (pixel.flightMode !== 2) {
                pixel.toX = points[Math.floor(i * k) % points.length][0] * factor + offsetX;
                pixel.toY = points[Math.floor(i * k) % points.length][1] * factor + offsetY;
                pixel.speedX = (Math.random() - 0.5) / 2;
                pixel.speedY = (Math.random() - 0.5) / 2;
                pixel.toSize = RANDOM(window.POINT_MIN, window.POINT_MAX);
            }
        }

        return true;
    };
}

function GET_TRANSITIONS_BASE(pixels) {
    if (!pixels) {
        return [];
    }

    var transitions = [];

    transitions.push(function() {
        for (var i = 0; i < pixels.length; i++) {
            var pixel = pixels[i];
            if (pixel.flightMode !== 2) {
                pixel.toX = Math.random() * window.innerWidth;
                pixel.toY = Math.random() * window.innerHeight;
                pixel.speedX = Math.cos(pixel.angle) * Math.random() * 3;
                pixel.speedY = Math.sin(pixel.angle) * Math.random() * 3;
                pixel.toSize = RANDOM(window.SCALE_MIN, window.SCALE_MAX);
            }
        }

        return false;
    });

    /*
     transitions.push(function() {
     for (var i = 0; i < pixels.length; i++) {
     var pixel = pixels[i];
     if (pixel.flightMode !== 2) {
     pixel.r = 255;
     pixel.g = 255;
     pixel.b = 255;
     pixel.size = Math.random() * 50 + 50;
     }
     }
     
     return false;
     });
     transitions.push(function() {
     for (var i = 0; i < pixels.length; i++) {
     var pixel = pixels[i];
     if (pixel.flightMode !== 2) {
     pixel.toSize = Math.random() * 10 + 1;
     }
     }
     
     return false;
     });
     */

    return transitions;
}

function GET_TRANSITIONS_BORDER(pixels) {
    if (!pixels) {
        return [];
    }

    var transitions = [];

    transitions.push(function() {
        for (var i = 0; i < pixels.length; i++) {
            var pixel = pixels[i];
            if (pixel.flightMode !== 2) {
                switch (i % 4) {
                    case 0:
                        pixel.toX = RANDOM(0, 10);
                        pixel.toY = Math.random() * window.innerHeight;
                        break;
                    case 1:
                        pixel.toX = RANDOM(window.innerWidth - 10, window.innerWidth - 5);
                        pixel.toY = Math.random() * window.innerHeight - 5;
                        break;
                    case 2:
                        pixel.toX = Math.random() * window.innerWidth - 5;
                        pixel.toY = RANDOM(5, 10);
                        break;
                    case 3:
                        pixel.toX = Math.random() * window.innerWidth - 5;
                        pixel.toY = RANDOM(window.innerHeight - 10, window.innerHeight - 5);
                        break;
                    default:
                        break;
                }

                pixel.speedX = Math.cos(pixel.angle) * (Math.random() - 0.5) / 2;
                pixel.speedY = Math.sin(pixel.angle) * (Math.random() - 0.5) / 2;
                pixel.toSize = RANDOM(window.POINT_MIN, window.POINT_MAX);
            }
        }

        return true;
    });

    return transitions;
}

function GET_TRANSITIONS_IMAGES(pixels) {
    if (!pixels) {
        return [];
    }

    var transitions = [];

    transitions.push(function() {
        var part = Math.min(window.innerWidth, window.innerHeight) / 4;
        var r = Math.floor(Math.random() * part + part);
        for (var i = 0; i < pixels.length; i++) {
            var pixel = pixels[i];
            if (pixel.flightMode !== 2) {
                pixel.toSize = RANDOM(window.POINT_MIN, window.POINT_MAX);
                pixel.toX = window.innerWidth / 2 + Math.cos(i * 3.6 * Math.PI / 180) * r;
                pixel.toY = window.innerHeight / 2 + Math.sin(i * 3.6 * Math.PI / 180) * r;
                pixel.speedX = (Math.random() - 0.5) / 2;
                pixel.speedY = (Math.random() - 0.5) / 2;
                if (window.RANDOMIZE_COLOR) {
                    pixel.toR = Math.random() * 255;
                    pixel.toG = Math.random() * 255;
                    pixel.toB = Math.random() * 255;
                }
            }
        }

        return true;
    });

    var POINTS_STRUCT = GET_POINTS();

    for (var i = 0; i < POINTS_STRUCT.length; i++) {
        transitions.push(GET_POINTS_TRANSITIONS(pixels, POINTS_STRUCT[i]));
    }

    return transitions;
}

function GET_TRANSITIONS_WAVEFORM(pixels) {
    if (!pixels) {
        return [];
    }

    var transitions = [];

    transitions.push(function() {
        var audioVisualizator = window.audioVisualizator || {};
        var waveform = "getWaveform" in audioVisualizator ? (audioVisualizator.getWaveform() || []) : [];

        var w = window.innerWidth;
        var h = window.innerHeight;

        var offsetX = 50;
        var offsetY = 0;

        var k = (window.innerWidth - offsetX * 2) / pixels.length;

        var step = Math.floor(waveform.length / pixels.length);

        for (var i = 0; i < pixels.length; i++) {
            var pixel = pixels[i];
            if (pixel.flightMode !== 2) {
                pixel.toX = i * k + offsetX;
                pixel.toY = (h / 2) + waveform[((i * step) % waveform.length)] * (h / 2) + (h / 4);
                pixel.speedX = (Math.random() - 0.5) / 2;
                pixel.speedY = (Math.random() - 0.5) / 2;
                pixel.toSize = RANDOM(window.POINT_MIN, window.POINT_MAX);
            }
        }

        return true;
    });

    return transitions;
}


