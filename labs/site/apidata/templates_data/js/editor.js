(function($) {

    //window.loading_button = $('#loading_button').hide();
    //window.play_button = $('#play_button').click(function(event) {
        //window.PLAY();
    //}).hide();

    $(document).click(function(event) {
        //if (window.play_button.is(':visible')) {
            //window.PLAY();
            //alert('play');
        //}
    });

    // --- PHOTO

    window.ADDPHOTO = function(data) {
        data = data || '';

        GLOBAL_PHOTOS.push($.trim(data));
    };

    window.ADDPHOTOS = function(data) {
        data = data || '';

        $.each(data.split(','), function(index, url) {
            GLOBAL_PHOTOS.push($.trim(url));
        });
    };

    window.CLEARPHOTOS = function(data) {
        GLOBAL_PHOTOS = [];
    };

// --- MUSIC

    window.ADDMUSIC = function(data) {
        GLOBAL_SOUNDS.push($.trim(data));
    };

    window.ADDMUSICS = function(data) {
        $.each(data.split(','), function(index, url) {
            GLOBAL_SOUNDS.push($.trim(url));
        });
    };

    window.CLEARMUSIC = function(data) {
        GLOBAL_SOUNDS = [];
    };

    // --- ACTIONS

    window.CLEAR = function() {
        window.CLEARPHOTOS();
        window.CLEARMUSIC();
        window.STOP();
    };

    window.BUILD = function() {
        if (audioVisualizator) {
            //audioVisualizator.build();
        }
        if (photoVisualizator) {
            photoVisualizator.build();
        }

        //window.loading_button.show();
    };

    window.PLAY = function() {
        if (audioVisualizator) {
            //audioVisualizator.play();
        }
        if (photoVisualizator) {
            photoVisualizator.play();
        }

        //window.loading_button.hide();
        //window.play_button.hide();
    };

    window.STOP = function() {
        if (audioVisualizator) {
            //audioVisualizator.stop();
        }
        if (photoVisualizator) {
            photoVisualizator.stop();
        } 
    };

    window.TEMPLATE = function(type) {
        if (photoVisualizator) {
            photoVisualizator.template(type);
        }
    };

    window.EFFECTDURATION = function(duration) {
        if (photoVisualizator) {
            photoVisualizator.effect_duration(duration);
        }
    };

    window.FADEDURATIONIMAGE = function(duration) {
        if (photoVisualizator) {
            photoVisualizator.fade_duration_image(duration);
        }
    };

    window.FADEDURATIONTRANSITION = function(duration) {
        if (photoVisualizator) {
            photoVisualizator.fade_duration_transition(duration);
        }
    };
})(jQuery);