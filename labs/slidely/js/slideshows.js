(function() {
    'use strict';

    window.Slideshow_KenBurns = function(photos, options, canvas, context) {
        // --- Options

        photos = photos || [];

        options = options || {};
        options.scale = options.scale || 1.5;
        options.effect_time = options.effect_time || 8000;
        options.fade_time = options.fade_time || 2000;

        // ---

        var self = this;

        // --- Initialize photos

        if (!photos || !photos.length) {
            return null;
        } else if (photos.length === 1) {
            photos.push(photos[0]);
        } else {

        }

        for (var i in photos) {
            var image = new Image();
            image.src = photos[i];

            photos[i] = {
                index: index,
                frame: 0,
                image: image,
                action: false,
                r1: [],
                r2: [],
                scale: options.scale,
                effect_time: options.effect_time,
                fade_time: options.fade_time,
                start_time: 0,
                update_time: 0
            };
        }

        // --- Variables

        var index = 0;
        var beforeChangeDispatched = false;
        var afterChangeDispatched = false;
        var startDispatched = false;
        var stopDispatched = false;

        // --- Base methods & callbacks

        var methods = {
            restart: function() {
                for (var i in photos) {
                    photos[i].action = false;
                }
            },
            effect_time: function(milliseconds) {
                methods.restart();

                for (var i in photos) {
                    photos[i].effect_time = milliseconds;
                }
            },
            fade_time: function(milliseconds) {
                methods.restart();

                for (var i in photos) {
                    photos[i].fade_time = milliseconds;
                }
            },
            start: function(objects, from, to) {

            },
            beforeChange: function(objects, from, to) {

            },
            change: function(objects, from, to) {

            },
            afterChange: function(objects, from, to) {

            },
            stop: function(objects, from, to) {

            },
            loaded: function() {

            },
            exec: function() {
                frame(index);
            }
        };

        // ---

        /** Отрисовка кадра - основная функция слайдщоу
         * 
         * @param {type} position
         * @returns {unresolved}
         */
        var frame = function(position) {
            var photo = photos[position];
            var image = photo.image;

            var from = position;
            var to = (position + 1) % photos.length;

            if (photo.action) {
                var update_time = Date.now() - photo.start_time;

                var alpha = 1;

                var rect = INTERPOLATE_RECT(photo.r1, photo.r2, update_time / photo.effect_time);

                if (update_time > (photo.effect_time - photo.fade_time)) {
                    frame((index + 1) % photos.length);
                    if (update_time > photo.effect_time) {
                        photo.action = false;

                        photo.frame = 0;

                        alpha = 0;

                        index = (index + 1) % photos.length;

                        if (methods.change) {
                            console.log('change ' + (Date.now() - photo.start_time));
                            methods.change.call(methods, photos, from, to);
                        }
                    } else {
                        if (methods.beforeChange && !beforeChangeDispatched) {
                            console.log('beforeChange ' + (Date.now() - photo.start_time));
                            beforeChangeDispatched = true;
                            methods.beforeChange.call(methods, photos, from, to);
                        }

                        alpha = Math.max(0, Math.min(1, 1 - (photo.fade_time - (photo.effect_time - update_time)) / photo.fade_time));
                    }
                } else {
                    if (update_time > photo.fade_time) {
                        if (methods.afterChange && !afterChangeDispatched) {
                            console.log('afterChange ' + (Date.now() - photo.start_time));
                            afterChangeDispatched = true;
                            methods.afterChange.call(methods, photos, from, to);
                        }
                    }
                }

                if (alpha >= 0) {
                    if (alpha < 1) {
                        context.save();

                        context.globalAlpha = alpha;
                    }

                    context.drawImage(image, rect[0], rect[1], rect[2] - rect[0], rect[3] - rect[1]);

                    if (alpha < 1) {
                        context.restore();
                    }
                }

                photo.frame++;

                if (methods.start && !startDispatched) {
                    startDispatched = true;

                    methods.start.call(methods, photos, from, to);
                }
            } else {
                if (image.width > 0 && image.height > 0) {
                    var size = PROPORTION_POINTS([image.width, image.height], [800, 600], false);
                    var rect1 = [0, 0, size[0], size[1]];
                    var rect2 = SCALE_RECT(rect1, photo.scale);

                    console.log(size);
                    console.log(rect1);
                    console.log(rect2);

                    // --- offset

                    var align_x = (Math.floor(Math.random() * 3) - 1) / 2;
                    var align_y = (Math.floor(Math.random() * 3) - 1) / 2;

                    var stepX = (rect2[2] - 800) / 2;
                    var x = -stepX + (RANDOM(-stepX / 2, stepX / 2));
                    rect2[0] += x;
                    rect2[2] += x;

                    var stepY = (rect2[3] - 600) / 2;
                    var y = -stepY + (RANDOM(-stepY / 2, stepY / 2));
                    rect2[1] += y;
                    rect2[3] += y;

                    // --- scale in/out

                    if (photo.index % 2) {
                        photo.r1 = rect1;
                        photo.r2 = rect2;
                    }
                    else {
                        photo.r1 = rect2;
                        photo.r2 = rect1;
                    }

                    // ---

                    photo.action = true;
                    photo.start_time = Date.now();

                    beforeChangeDispatched = false;
                    afterChangeDispatched = false;
                }
            }
        };

        // --- Interval for check when images are loaded

        var interval = setInterval(function() {
            var loaded = 0;

            for (var photo in photos) {
                //if (photo.image.loaded || photo.image.error) {
                //loaded++;
                //}
            }

            if (loaded >= photos.length) {
                clearInterval(interval);

                methods.loaded.call(methods);
            }
        }, 100);

        // --- 

        return methods;
    };
})();