(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];

    for (var i = 0; i < vendors.length && !window.requestAnimationFrame; ++i) {
        window.requestAnimationFrame = window[vendors[i] + 'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[i] + 'CancelAnimationFrame'] || window[vendors[i] + 'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame) {
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() {
                callback(currTime + timeToCall);
            }, timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
    }

    if (!window.cancelAnimationFrame) {
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
    }
}());

// ---

var FPS = function(debug) {
    var self = this;

    var startTime = Date.now();
    var currentTime = startTime;
    var frames = 0;
    var lastFrames = 0;

    self.exec = function() {
        frames++;
        currentTime = Date.now();
        if (currentTime - startTime >= 1000) {
            console.log('fps: ' + frames);

            debug.innerHTML = frames;

            lastFrames = frames;
            frames = 0;
            startTime = currentTime;
        }

        // ---

        return lastFrames;
    };

    return self;
};

// ---

var UPDATER = function(canvas, context) {
    var self = this;

    self.exec = function() {

    };

    return self;
};

// ---

var RENDERER = function(canvas, context) {
    var self = this;

    var compositeTypes = ['source-over', 'source-in', 'source-out', 'source-atop', 'destination-over', 'destination-in', 'destination-out', 'destination-atop', 'lighter', 'darker', 'copy', 'xor'];

    var index = 0;

    self.exec = function() {
        /*
        var centerX = canvas.width / 2;
        var centerY = canvas.height / 2;
        var radius = 70;

        if (SLIDESHOW && SLIDESHOW.hasOwnProperty('exec')) {
            context.globalCompositeOperation = compositeTypes[0];
            context.globalAlpha = 1;

            SLIDESHOW.exec();
        }

        context.globalCompositeOperation = compositeTypes[Math.floor(index / 1000) % compositeTypes.length];
        context.globalAlpha = 0.5;

        context.beginPath();
        context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
        context.fillStyle = '#00ff00';
        context.closePath();
        context.fill();

        index++;
        */
    };

    return self;
};

// ---

(function(window, document) {

    var debug = document.getElementById('debug');

    var canvas = document.getElementById('graphics');
    canvas.width = 800;
    canvas.height = 600;

    window.onresize = function(event) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    };

    var context = canvas.getContext('2d');
    var updater = new UPDATER(canvas, context);
    var rendered = new RENDERER(canvas, context);
    var fps = new FPS(debug);

    // ---

    /**
     * @type Slideshow_KenBurns
     * @type Slideshow_Slide
     */

    var SLIDESHOW = new Slideshow_KenBurns(['frames/1.JPG', 'frames/2.JPG', 'frames/3.JPG'], {}, canvas, context);
    console.log('SLIDESHOW', SLIDESHOW);

    // ---

    (function loop() {
        // --- clear

        context.save();
        context.setTransform(1, 0, 0, 1, 0, 0);
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.restore();

        // ---

        updater.exec();
        rendered.exec();
        
        SLIDESHOW.exec();
        
        fps.exec();

        // --- loop

        requestAnimationFrame(loop);
    })();

})(window, document);