(function($) {

    var BASE_URL = 'http://dimetrix.ru/labs/lab1/';
    //var BASE_URL = 'http://localhost/dimetrix/labs/lab1/';

    // ---

    var panel = $('#panel');
    var panel_toggle = $('#panel_toggle');

    var music_url = $('.music_url').val((audioVisualizator && audioVisualizator.getCurrentSound()) ? audioVisualizator.getCurrentSound() : '');
    var music_file = $('.music_file');
    var music_info = $('.music_info');
    var music_file_loading = $('.music_file_loading');
    var music_file_progress = $('.music_file_progress');

    var photo_url = $('.photo_url');
    var photo_file = $('.photo_file');
    var photo_info = $('.photo_info');
    var photo_file_loading = $('.photo_file_loading');
    var photo_file_progress = $('.photo_file_progress');

    var photo_template = '<li><img src="{src}" url="{url}"/><div class="remove"></div></li>';
    var photo_add = $('.photo_add');
    var photos_list = $('.photos_list').append(function() {
        var result = '';
        $.each(GLOBAL_PHOTOS, function(index, photo) {
            result += photo_template.replace('{src}', photo);
        });
        return result;
    });

    var preview = $('.preview');
    var preview_progress = $('.preview_progress');

    var save = $('.save');
    var save_progress = $('.save_progress');

    var link = $('.link').hide();

    var stop = $('.stop');

// --- functions

    function BUILD_PROJECT() {
        var photos_sources = [];
        var sounds_sources = [];

        photos_list.find('li').each(function(index, element) {
            var photo_source = $.trim($(this).find('img').attr('src'));

            if (photo_source && photo_source.length) {
                photos_sources.push(photo_source);
            }
        });

        var sound_source = $.trim(music_url.val());
        if (sound_source && sound_source.length) {
            sounds_sources.push(sound_source);
        }

        return [photos_sources, sounds_sources];
    }

// --- Panel

    panel.click(function(event) {

    });

    panel_toggle.click(function(event) {
        panel.toggle();
    });

// --- Music

    music_file.fileupload({
        url: 'upload.php',
        dataType: 'json',
        done: function(e, data) {
            console.log('done', e, data);



            var result = data.result;

            if (result && result.hasOwnProperty('error') && result.error === 0) {
                music_file_loading.html('SUCCESS');

                music_url.val(BASE_URL + result.file);
            } else {
                music_file_loading.html('ERROR');

                music_url.val('ERROR');
            }
        },
        fail: function(e, data) {
            console.log('fail', e, data);

            music_file_loading.html('ERROR');

            music_url.val('ERROR');
        },
        send: function(e, data) {
            console.log('send', e, data);
        },
        progressall: function(e, data) {
            console.log('progressall', e, data);

            music_file_loading.html('(' + parseInt(data.loaded / data.total * 100, 10) + '%)');
        },
        change: function(e, data) {
            music_file_loading.html('LOADING...');

            console.log('change', e, data);

            var label = 'file from computer';

            var files = data.files || [];

            if (files && files.length) {
                var file = files[0] || {};

                if (file.hasOwnProperty('name')) {
                    label = file.name;
                }
            }

            music_info.val(label.replace(/\\/g, '/').replace(/.*\//, ''));
        }
    });

// --- Photos


    photo_file.fileupload({
        url: 'upload.php',
        dataType: 'json',
        done: function(e, data) {
            console.log('done', e, data);

            var result = data.result;

            if (result && result.hasOwnProperty('error') && result.error === 0) {
                if (result.hasOwnProperty('file') && result.file) {
                    photo_file_loading.html('SUCCESS');

                    photos_list.append(photo_template.replace('{src}', BASE_URL + result.file));
                } else if (result.hasOwnProperty('files') && result.files && result.files.length) {
                    photo_file_loading.html('SUCCESS');

                    $.each(result.files, function(index, file) {
                        photos_list.append(photo_template.replace('{src}', BASE_URL + file));
                    });
                } else {
                    photo_file_loading.html('ERROR');

                    photo_url.val('ERROR');
                }
            } else {
                photo_file_loading.html('ERROR');

                photo_url.val('ERROR');
            }
        },
        fail: function(e, data) {
            console.log('fail', e, data);

            photo_file_loading.html('ERROR');

            photo_url.val('ERROR');
        },
        send: function(e, data) {
            console.log('send', e, data);
        },
        progressall: function(e, data) {
            console.log('progressall', e, data);

            photo_file_loading.html('(' + parseInt(data.loaded / data.total * 100, 10) + '%)');
        },
        change: function(e, data) {
            photo_file_loading.html('LOADING...');

            console.log('change', e, data);

            var label = 'file from computer';

            var files = data.files || [];

            if (files && files.length) {
                var file = files[0] || {};

                if (file.hasOwnProperty('name')) {
                    label = file.name;
                }
            }

            photo_info.val(label.replace(/\\/g, '/').replace(/.*\//, ''));
        }
    });

    photo_add.click(function(event) {
        var url = $.trim(photo_url.val());

        if (url && url.length) {
            photos_list.append(photo_template.replace('{src}', url));
        }

        console.log('url', url);
    });

    photos_list.sortable();
    photos_list.delegate('li', 'dblclick', function() {
        //$(this).remove();
    });
    photos_list.delegate('.remove', 'click', function() {
        $(this).parents('li').remove();
    });

// --- Preview

    var previewClick = false;

    preview.click(function(event) {
        if (previewClick) {
            return true;
        }

        previewClick = true;

        console.log('previewClick');

        var project = BUILD_PROJECT();
        var photos_sources = project[0];
        var sounds_sources = project[1];

        if (photos_sources && photos_sources.length && sounds_sources && sounds_sources.length) {
            GLOBAL_PHOTOS = photos_sources;
            GLOBAL_SOUNDS = sounds_sources;

            if (audioVisualizator) {
                audioVisualizator.build();
            }
            if (photoVisualizator) {
                photoVisualizator.build();
            }
        }

        console.log(project);

        setTimeout(function() {
            previewClick = false;
        }, 3000);
    });

// --- Save

    var saveClick = false;

    save.click(function(event) {
        if (saveClick) {
            return true;
        }

        saveClick = true;

        console.log('saveClick');

        var project = BUILD_PROJECT();
        var photos_sources = project[0];
        var sounds_sources = project[1];

        if (photos_sources && photos_sources.length && sounds_sources && sounds_sources.length) {
            $.get(BASE_URL + 'save.php', {photos: photos_sources, sounds: sounds_sources}, function(result) {
                result = result || {};

                try {
                    result = $.parseJSON(result);
                } catch (exception) {

                }

                if (result && result.hasOwnProperty('error') && result.error === 0 && result.hasOwnProperty('link') && result.link) {
                    link.attr('href', BASE_URL + result.link).html(BASE_URL + result.link).show();
                } else {
                    alert('SAVE ERROR!');
                }
            });
        }

        console.log(project);

        setTimeout(function() {
            saveClick = false;
        }, 3000);
    });

    // --- Stop

    stop.click(function(event) {
        if (audioVisualizator) {
            audioVisualizator.stop();
        }
    });

    window.ADD = function(data) {
        GLOBAL_PHOTOS.push(data);
    };

    window.CLEAR = function() {
        GLOBAL_PHOTOS = [];
    };

    window.BUILD = function() {
        if (audioVisualizator) {
            audioVisualizator.build();
        }
        if (photoVisualizator) {
            photoVisualizator.build();
        }
    };

    window.PLAY = function() {
        if (audioVisualizator) {
            audioVisualizator.play();
        }
    };

    window.STOP = function() {
        if (audioVisualizator) {
            audioVisualizator.stop();
        }
    };

})(jQuery);