(function($) {

    window.scrollTo(0, 1);

    // ---

    //alert("Dancer.isSupported = " + Dancer.isSupported() + "!");
    //alert("Float32Array = " + (window.Float32Array ? 1 : 0) + " Uint32Array = " + (window.Uint32Array ? 1 : 0) + "!");
    //alert("AudioContext = " + (window.AudioContext ? 1 : 0) + " webkitAudioContext = " + (window.webkitAudioContext ? 1 : 0) + "!");

    // ---

    var photoVisualizator = new Processing("graphics");

    // ---

    var pixels = [];

    var transitions_base = [];
    var transitions_images = [];
    var transitions_waveform = [];

    var components = [];
    var slideshows = [];

    // ---

    $(window).resize(function() {
        photoVisualizator.size(window.innerWidth, window.innerHeight);
    });

    photoVisualizator.size(window.innerWidth, window.innerHeight);
    photoVisualizator.noStroke();
    photoVisualizator.frameRate(60);
    photoVisualizator.fill(0, 0, 0);
    photoVisualizator.background(0);
    photoVisualizator.build = function() {
        pixels = GET_PIXELS(180);

        transitions_base = GET_TRANSITIONS_BASE(pixels);
        transitions_images = GET_TRANSITIONS_IMAGES(pixels);
        transitions_waveform = GET_TRANSITIONS_WAVEFORM(pixels);

        components = GET_COMPONENTS(photoVisualizator, transitions_base, transitions_images, transitions_waveform, pixels);
        slideshows = GET_SLIDESHOWS(photoVisualizator, transitions_base, transitions_images, transitions_waveform, pixels);

        console.log("pixels", pixels.length);
    };

    photoVisualizator.mouseMoved = function() {
        var countComponents = components.length, countSlideshows = slideshows.length, i;

        for (i = 0; i < countComponents; i++) {
            components[i].mx = photoVisualizator.mouseX;
            components[i].my = photoVisualizator.mouseY;
        }

        for (i = 0; i < countSlideshows; i++) {
            slideshows[i].mx = photoVisualizator.mouseX;
            slideshows[i].my = photoVisualizator.mouseY;
        }
    };

    photoVisualizator.mousePressed = function() {
        var countComponents = components.length, countSlideshows = slideshows.length, i;

        for (i = 0; i < countComponents; i++) {
            components[i].pressed(photoVisualizator.mouseX, photoVisualizator.mouseY);
        }

        for (i = 0; i < countSlideshows; i++) {
            slideshows[i].pressed(photoVisualizator.mouseX, photoVisualizator.mouseY);
        }
    };

    photoVisualizator.mouseReleased = function() {
        var countComponents = components.length, countSlideshows = slideshows.length, i;

        for (i = 0; i < countComponents; i++) {
            components[i].released(photoVisualizator.mouseX, photoVisualizator.mouseY);
        }

        for (i = 0; i < countSlideshows; i++) {
            slideshows[i].released(photoVisualizator.mouseX, photoVisualizator.mouseY);
        }
    };

    photoVisualizator.draw = function() {
        var countComponents = components.length, countSlideshows = slideshows.length, i;

        for (i = 0; i < countSlideshows; i++) {
            slideshows[i].update();
        }

        for (i = 0; i < countComponents; i++) {
            components[i].update();
        }

        photoVisualizator.background(0);

        for (i = 0; i < countSlideshows; i++) {
            slideshows[i].draw();
        }

        for (i = 0; i < countComponents; i++) {
            components[i].draw();
        }
    };

    for (var i = 0; i < slideshows.length; i++) {
        slideshows[i].change = function(from, to) {
            for (var j = 0; j < slideshows.length; j++) {
                components[j].change(from, to);
            }
        };
        slideshows[i].start = function() {
            for (var j = 0; j < slideshows.length; j++) {
                components[j].start();
            }
        };
    }

    photoVisualizator.build();
    photoVisualizator.loop();

    // ---

    window.photoVisualizator = photoVisualizator;

})(jQuery);