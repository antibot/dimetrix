var POINTS_MINUTTA = [[0, 0], [0, 4], [0, 8], [0, 12], [0, 16], [0, 20], [0, 24], [4, 0], [4, 4], [4, 8], [4, 12], [4, 16], [4, 20], [4, 24], [8, 4], [8, 8], [8, 12], [12, 8], [12, 12], [12, 16], [12, 20], [16, 16], [16, 20], [20, 8], [20, 12], [20, 16], [20, 20], [24, 0], [24, 4], [24, 8], [24, 12], [28, 0], [28, 4], [28, 8], [28, 12], [28, 16], [28, 20], [28, 24], [32, 0], [32, 4], [32, 8], [32, 12], [32, 16], [32, 20], [32, 24], [48, 0], [48, 4], [48, 8], [48, 12], [48, 16], [48, 20], [48, 24], [52, 0], [52, 4], [52, 8], [52, 12], [52, 16], [52, 20], [52, 24], [68, 0], [68, 4], [68, 8], [68, 12], [68, 16], [68, 20], [68, 24], [72, 0], [72, 4], [72, 8], [72, 12], [72, 16], [72, 20], [72, 24], [76, 0], [76, 4], [76, 8], [80, 8], [80, 12], [84, 12], [84, 16], [84, 20], [88, 16], [88, 20], [88, 24], [92, 0], [92, 4], [92, 8], [92, 12], [92, 16], [92, 20], [92, 24], [96, 0], [96, 4], [96, 8], [96, 12], [96, 16], [96, 20], [96, 24], [112, 0], [112, 4], [112, 8], [112, 12], [112, 16], [112, 20], [116, 0], [116, 4], [116, 8], [116, 12], [116, 16], [116, 20], [116, 24], [120, 24], [124, 24], [128, 24], [132, 20], [132, 24], [136, 0], [136, 4], [136, 8], [136, 12], [136, 16], [136, 20], [136, 24], [152, 0], [156, 0], [160, 0], [164, 0], [164, 4], [164, 8], [164, 12], [164, 16], [164, 20], [164, 24], [168, 0], [168, 4], [168, 8], [168, 12], [168, 16], [168, 20], [168, 24], [172, 0], [176, 0], [180, 0], [188, 0], [192, 0], [196, 0], [200, 0], [200, 4], [200, 8], [200, 12], [200, 16], [200, 20], [200, 24], [204, 0], [204, 4], [204, 8], [204, 12], [204, 16], [204, 20], [204, 24], [208, 0], [212, 0], [216, 0], [224, 24], [228, 16], [228, 20], [228, 24], [232, 8], [232, 12], [232, 16], [232, 20], [232, 24], [236, 0], [236, 4], [236, 8], [236, 12], [236, 16], [240, 0], [240, 4], [240, 16], [244, 0], [244, 4], [244, 8], [244, 12], [244, 16], [248, 12], [248, 16], [248, 20], [248, 24], [252, 20], [252, 24]];

var POINTS_DIMETRIX = [[0, 0], [0, 4], [0, 8], [0, 12], [0, 16], [0, 20], [0, 24], [4, 0], [4, 4], [4, 8], [4, 12], [4, 16], [4, 20], [4, 24], [8, 0], [8, 24], [12, 0], [12, 24], [16, 0], [16, 24], [20, 0], [20, 4], [20, 8], [20, 16], [20, 20], [20, 24], [24, 4], [24, 8], [24, 12], [24, 16], [24, 20], [40, 0], [40, 4], [40, 8], [40, 12], [40, 16], [40, 20], [40, 24], [44, 0], [44, 4], [44, 8], [44, 12], [44, 16], [44, 20], [44, 24], [60, 0], [60, 4], [60, 8], [60, 12], [60, 16], [60, 20], [60, 24], [64, 0], [64, 4], [64, 8], [64, 12], [64, 16], [64, 20], [64, 24], [68, 4], [68, 8], [68, 12], [72, 8], [72, 12], [72, 16], [72, 20], [76, 16], [76, 20], [80, 8], [80, 12], [80, 16], [80, 20], [84, 0], [84, 4], [84, 8], [84, 12], [88, 0], [88, 4], [88, 8], [88, 12], [88, 16], [88, 20], [88, 24], [92, 0], [92, 4], [92, 8], [92, 12], [92, 16], [92, 20], [92, 24], [108, 0], [108, 4], [108, 8], [108, 12], [108, 16], [108, 20], [108, 24], [112, 0], [112, 4], [112, 8], [112, 12], [112, 16], [112, 20], [112, 24], [116, 0], [116, 12], [116, 24], [120, 0], [120, 12], [120, 24], [124, 0], [124, 12], [124, 24], [128, 0], [128, 12], [128, 24], [140, 0], [144, 0], [148, 0], [152, 0], [152, 4], [152, 8], [152, 12], [152, 16], [152, 20], [152, 24], [156, 0], [156, 4], [156, 8], [156, 12], [156, 16], [156, 20], [156, 24], [160, 0], [164, 0], [168, 0], [180, 0], [180, 4], [180, 8], [180, 12], [180, 16], [180, 20], [180, 24], [184, 0], [184, 4], [184, 8], [184, 12], [184, 16], [184, 20], [184, 24], [188, 0], [188, 16], [192, 0], [192, 16], [196, 0], [196, 12], [196, 16], [196, 20], [196, 24], [200, 0], [200, 4], [200, 8], [200, 12], [200, 16], [200, 20], [200, 24], [204, 4], [204, 8], [204, 24], [216, 0], [216, 4], [216, 8], [216, 12], [216, 16], [216, 20], [216, 24], [220, 0], [220, 4], [220, 8], [220, 12], [220, 16], [220, 20], [220, 24], [232, 0], [232, 24], [236, 0], [236, 4], [236, 20], [236, 24], [240, 0], [240, 4], [240, 8], [240, 16], [240, 20], [240, 24], [244, 4], [244, 8], [244, 12], [244, 16], [244, 20], [248, 8], [248, 12], [248, 16], [252, 0], [252, 4], [252, 8], [252, 16], [252, 20], [252, 24], [256, 0], [256, 4], [256, 20], [256, 24]];

var POINTS_HEART = [[0, 36], [0, 48], [0, 60], [0, 72], [0, 84], [0, 96], [0, 108], [0, 120], [12, 24], [12, 36], [12, 48], [12, 60], [12, 72], [12, 84], [12, 96], [12, 108], [12, 120], [12, 132], [12, 144], [24, 12], [24, 24], [24, 36], [24, 48], [24, 60], [24, 72], [24, 84], [24, 96], [24, 108], [24, 120], [24, 132], [24, 144], [24, 156], [36, 0], [36, 12], [36, 24], [36, 36], [36, 48], [36, 132], [36, 144], [36, 156], [36, 168], [48, 0], [48, 12], [48, 24], [48, 36], [48, 156], [48, 168], [48, 180], [60, 0], [60, 12], [60, 24], [60, 168], [60, 180], [60, 192], [72, 0], [72, 12], [72, 24], [72, 180], [72, 192], [84, 0], [84, 12], [84, 24], [84, 180], [84, 192], [84, 204], [96, 0], [96, 12], [96, 24], [96, 192], [96, 204], [96, 216], [108, 12], [108, 24], [108, 204], [108, 216], [108, 228], [120, 24], [120, 36], [120, 216], [120, 228], [120, 240], [132, 36], [132, 48], [132, 240], [132, 252], [132, 264], [144, 24], [144, 36], [144, 228], [144, 240], [144, 252], [156, 12], [156, 24], [156, 36], [156, 216], [156, 228], [156, 240], [168, 12], [168, 24], [168, 204], [168, 216], [168, 228], [180, 0], [180, 12], [180, 24], [180, 192], [180, 204], [180, 216], [192, 0], [192, 12], [192, 24], [192, 180], [192, 192], [192, 204], [204, 0], [204, 12], [204, 24], [204, 168], [204, 180], [204, 192], [216, 0], [216, 12], [216, 24], [216, 36], [216, 156], [216, 168], [216, 180], [228, 0], [228, 12], [228, 24], [228, 36], [228, 48], [228, 144], [228, 156], [228, 168], [240, 12], [240, 24], [240, 36], [240, 48], [240, 60], [240, 72], [240, 108], [240, 120], [240, 132], [240, 144], [240, 156], [252, 12], [252, 24], [252, 36], [252, 48], [252, 60], [252, 72], [252, 84], [252, 96], [252, 108], [252, 120], [252, 132], [252, 144], [264, 36], [264, 48], [264, 60], [264, 72], [264, 84], [264, 96], [264, 108], [264, 120], [264, 132], [276, 60], [276, 72], [276, 84], [276, 96]]; 

var POINTS_LIKE = [[0, 90], [0, 100], [0, 110], [0, 120], [0, 130], [0, 140], [0, 150], [0, 160], [0, 170], [0, 180], [0, 190], [0, 200], [10, 90], [10, 100], [10, 110], [10, 120], [10, 130], [10, 140], [10, 150], [10, 160], [10, 170], [10, 180], [10, 190], [10, 200], [20, 90], [20, 100], [20, 110], [20, 120], [20, 130], [20, 140], [20, 150], [20, 160], [20, 170], [20, 180], [20, 190], [20, 200], [30, 90], [30, 100], [30, 110], [30, 120], [30, 130], [30, 140], [30, 150], [30, 160], [30, 170], [30, 180], [30, 190], [30, 200], [40, 90], [40, 100], [40, 110], [40, 120], [40, 130], [40, 140], [40, 150], [40, 160], [40, 170], [40, 180], [40, 190], [40, 200], [50, 90], [50, 100], [50, 110], [50, 120], [50, 130], [50, 140], [50, 150], [50, 160], [50, 170], [50, 180], [50, 190], [50, 200], [60, 90], [60, 100], [60, 110], [60, 120], [60, 130], [60, 140], [60, 150], [60, 160], [60, 170], [60, 180], [60, 190], [60, 200], [70, 100], [70, 110], [70, 180], [70, 190], [80, 90], [80, 100], [80, 180], [80, 190], [90, 80], [90, 90], [90, 100], [90, 180], [90, 190], [100, 70], [100, 80], [100, 90], [100, 180], [100, 190], [100, 200], [110, 50], [110, 60], [110, 70], [110, 80], [110, 190], [110, 200], [120, 20], [120, 30], [120, 40], [120, 50], [120, 60], [120, 200], [120, 210], [130, 0], [130, 10], [130, 20], [130, 30], [130, 40], [130, 200], [130, 210], [140, 0], [140, 10], [140, 200], [140, 210], [150, 0], [150, 10], [150, 40], [150, 50], [150, 60], [150, 70], [150, 80], [150, 200], [150, 210], [160, 0], [160, 10], [160, 20], [160, 30], [160, 40], [160, 50], [160, 60], [160, 70], [160, 80], [160, 200], [160, 210], [170, 0], [170, 10], [170, 20], [170, 80], [170, 200], [170, 210], [180, 80], [180, 200], [180, 210], [190, 80], [190, 90], [190, 190], [190, 200], [200, 80], [200, 90], [200, 180], [200, 190], [200, 200], [210, 90], [210, 100], [210, 110], [210, 120], [210, 130], [210, 140], [210, 150], [210, 160], [210, 170], [210, 180], [210, 190]]; 

var POINTS_GOOGLE_PLUS = [[0, 160], [0, 170], [0, 180], [0, 190], [0, 200], [10, 40], [10, 50], [10, 60], [10, 150], [10, 160], [10, 170], [10, 180], [10, 190], [10, 200], [10, 210], [20, 20], [20, 30], [20, 40], [20, 50], [20, 60], [20, 70], [20, 80], [20, 140], [20, 150], [20, 160], [20, 170], [20, 180], [20, 190], [20, 200], [20, 210], [30, 10], [30, 20], [30, 30], [30, 40], [30, 50], [30, 60], [30, 70], [30, 80], [30, 90], [30, 140], [30, 150], [30, 200], [30, 210], [30, 220], [40, 10], [40, 20], [40, 70], [40, 80], [40, 90], [40, 140], [40, 210], [40, 220], [50, 0], [50, 10], [50, 80], [50, 90], [50, 130], [50, 140], [50, 210], [50, 220], [60, 0], [60, 90], [60, 100], [60, 130], [60, 220], [70, 0], [70, 90], [70, 100], [70, 110], [70, 120], [70, 130], [70, 220], [80, 0], [80, 10], [80, 90], [80, 100], [80, 110], [80, 120], [80, 130], [80, 140], [80, 220], [90, 0], [90, 10], [90, 20], [90, 90], [90, 100], [90, 110], [90, 120], [90, 130], [90, 140], [90, 210], [90, 220], [100, 0], [100, 10], [100, 20], [100, 30], [100, 40], [100, 50], [100, 60], [100, 70], [100, 80], [100, 90], [100, 120], [100, 130], [100, 140], [100, 210], [100, 220], [110, 0], [110, 10], [110, 20], [110, 30], [110, 40], [110, 50], [110, 60], [110, 70], [110, 80], [110, 130], [110, 140], [110, 150], [110, 200], [110, 210], [120, 0], [120, 10], [120, 30], [120, 40], [120, 50], [120, 60], [120, 70], [120, 140], [120, 150], [120, 160], [120, 170], [120, 180], [120, 190], [120, 200], [120, 210], [130, 0], [130, 150], [130, 160], [130, 170], [130, 180], [130, 190], [130, 200], [140, 170], [140, 180], [150, 30], [150, 40], [160, 30], [160, 40], [170, 30], [170, 40], [180, 0], [180, 10], [180, 20], [180, 30], [180, 40], [180, 50], [180, 60], [180, 70], [190, 0], [190, 10], [190, 20], [190, 30], [190, 40], [190, 50], [190, 60], [190, 70], [200, 30], [200, 40], [210, 30], [210, 40], [220, 30], [220, 40]]; 

var POINTS_SMILE = [[0, 96], [0, 108], [0, 120], [0, 132], [0, 144], [0, 156], [0, 168], [0, 180], [0, 192], [12, 72], [12, 84], [12, 96], [12, 108], [12, 180], [12, 192], [12, 204], [12, 216], [24, 60], [24, 72], [24, 84], [24, 204], [24, 216], [24, 228], [36, 48], [36, 60], [36, 228], [36, 240], [48, 36], [48, 48], [48, 108], [48, 120], [48, 144], [48, 156], [48, 168], [48, 240], [48, 252], [60, 24], [60, 36], [60, 84], [60, 96], [60, 108], [60, 120], [60, 144], [60, 156], [60, 168], [60, 180], [60, 192], [60, 252], [60, 264], [72, 12], [72, 24], [72, 84], [72, 180], [72, 192], [72, 204], [72, 264], [72, 276], [84, 12], [84, 24], [84, 72], [84, 84], [84, 204], [84, 216], [84, 264], [84, 276], [96, 12], [96, 24], [96, 84], [96, 96], [96, 108], [96, 120], [96, 216], [96, 264], [96, 276], [108, 12], [108, 96], [108, 108], [108, 120], [108, 216], [108, 228], [108, 276], [120, 0], [120, 12], [120, 216], [120, 228], [120, 276], [120, 288], [132, 0], [132, 12], [132, 216], [132, 228], [132, 276], [132, 288], [144, 0], [144, 12], [144, 216], [144, 228], [144, 276], [144, 288], [156, 12], [156, 96], [156, 108], [156, 120], [156, 216], [156, 228], [156, 276], [168, 12], [168, 24], [168, 84], [168, 96], [168, 108], [168, 120], [168, 216], [168, 276], [180, 12], [180, 24], [180, 72], [180, 84], [180, 204], [180, 216], [180, 264], [180, 276], [192, 12], [192, 24], [192, 84], [192, 180], [192, 192], [192, 204], [192, 264], [192, 276], [204, 24], [204, 36], [204, 84], [204, 96], [204, 108], [204, 120], [204, 144], [204, 156], [204, 168], [204, 180], [204, 192], [204, 252], [204, 264], [216, 36], [216, 48], [216, 108], [216, 120], [216, 144], [216, 156], [216, 168], [216, 240], [216, 252], [228, 36], [228, 48], [228, 60], [228, 228], [228, 240], [228, 252], [240, 60], [240, 72], [240, 84], [240, 204], [240, 216], [240, 228], [252, 72], [252, 84], [252, 96], [252, 108], [252, 180], [252, 192], [252, 204], [252, 216], [264, 96], [264, 108], [264, 120], [264, 132], [264, 144], [264, 156], [264, 168], [264, 180], [264, 192]]; 

var POINTS_FACEBOOK = [[0, 84], [0, 96], [0, 108], [0, 120], [12, 84], [12, 96], [12, 108], [12, 120], [24, 24], [24, 36], [24, 48], [24, 60], [24, 72], [24, 84], [24, 96], [24, 108], [24, 120], [24, 132], [24, 144], [24, 156], [24, 168], [24, 180], [24, 192], [24, 204], [24, 216], [24, 228], [24, 240], [36, 12], [36, 24], [36, 36], [36, 48], [36, 60], [36, 72], [36, 84], [36, 96], [36, 108], [36, 120], [36, 132], [36, 144], [36, 156], [36, 168], [36, 180], [36, 192], [36, 204], [36, 216], [36, 228], [36, 240], [48, 12], [48, 24], [48, 36], [48, 48], [48, 60], [48, 72], [48, 84], [48, 96], [48, 108], [48, 120], [48, 132], [48, 144], [48, 156], [48, 168], [48, 180], [48, 192], [48, 204], [48, 216], [48, 228], [48, 240], [60, 0], [60, 12], [60, 24], [60, 36], [60, 48], [60, 60], [60, 72], [60, 84], [60, 96], [60, 108], [60, 120], [60, 132], [60, 144], [60, 156], [60, 168], [60, 180], [60, 192], [60, 204], [60, 216], [60, 228], [60, 240], [72, 0], [72, 12], [72, 24], [72, 36], [72, 84], [72, 96], [72, 108], [72, 120], [84, 0], [84, 12], [84, 24], [84, 36], [84, 84], [84, 96], [84, 108], [84, 120], [96, 0], [96, 12], [96, 24], [96, 36], [96, 84], [96, 96], [96, 108], [96, 120]]; 

var POINTS_STAR = [[0, 84], [12, 84], [24, 84], [24, 96], [36, 84], [36, 96], [36, 108], [48, 84], [48, 96], [48, 108], [48, 192], [48, 204], [60, 84], [60, 96], [60, 108], [60, 120], [60, 156], [60, 168], [60, 180], [60, 192], [60, 204], [72, 84], [72, 96], [72, 108], [72, 120], [72, 132], [72, 144], [72, 156], [72, 168], [72, 180], [72, 192], [84, 84], [84, 96], [84, 108], [84, 120], [84, 132], [84, 144], [84, 156], [84, 168], [84, 180], [96, 36], [96, 48], [96, 60], [96, 72], [96, 84], [96, 96], [96, 108], [96, 120], [96, 132], [96, 144], [96, 156], [96, 168], [96, 180], [108, 0], [108, 12], [108, 24], [108, 36], [108, 48], [108, 60], [108, 72], [108, 84], [108, 96], [108, 108], [108, 120], [108, 132], [108, 144], [108, 156], [108, 168], [120, 0], [120, 12], [120, 24], [120, 36], [120, 48], [120, 60], [120, 72], [120, 84], [120, 96], [120, 108], [120, 120], [120, 132], [120, 144], [120, 156], [120, 168], [132, 36], [132, 48], [132, 60], [132, 72], [132, 84], [132, 96], [132, 108], [132, 120], [132, 132], [132, 144], [132, 156], [132, 168], [132, 180], [144, 72], [144, 84], [144, 96], [144, 108], [144, 120], [144, 132], [144, 144], [144, 156], [144, 168], [144, 180], [156, 84], [156, 96], [156, 108], [156, 120], [156, 132], [156, 144], [156, 156], [156, 168], [156, 180], [156, 192], [168, 84], [168, 96], [168, 108], [168, 120], [168, 144], [168, 156], [168, 168], [168, 180], [168, 192], [168, 204], [180, 84], [180, 96], [180, 108], [180, 180], [180, 192], [180, 204], [192, 84], [192, 96], [192, 108], [204, 84], [204, 96], [216, 84], [228, 84]]; 

// ---

var POINTS_OFFSET = 20;

// ---

// 220 x 380

function GET_POINTS() {
    var result = [];

    /*
     offsetX: function() {
     return Math.min(window.innerWidth - POINTS_OFFSET, Math.max(POINTS_OFFSET, (window.innerWidth - this.size()[0]) * Math.random()));
     },
     offsetY: function() {
     return Math.min(window.innerHeight - POINTS_OFFSET, Math.max(POINTS_OFFSET, (window.innerHeight - this.size()[1]) * Math.random()));
     },*/

/*
    result.push({
        points: function() {
            return POINTS_MINUTTA;
        },
        size: function() {
            return GET_POINTS_SIZE(this.points());
        },
        offsetX: function() {
            return (window.innerWidth - this.size()[0]) / 2;
        },
        offsetY: function() {
            return (window.innerHeight - this.size()[1]) / 2;
        },
        k: function() {
            return 3.6;
        }
    });

    result.push({
        points: function() {
            return POINTS_DIMETRIX;
        },
        size: function() {
            return GET_POINTS_SIZE(this.points());
        },
        offsetX: function() {
            return (window.innerWidth - this.size()[0]) / 2;
        },
        offsetY: function() {
            return (window.innerHeight - this.size()[1]) / 2;
        },
        k: function() {
            return 3.6;
        }
    });
    */
    
    result.push({
        points: function() {
            return POINTS_HEART;
        },
        size: function() {
            return GET_POINTS_SIZE(this.points());
        },
        offsetX: function() {
            return (window.innerWidth - this.size()[0]) / 2;
        },
        offsetY: function() {
            return (window.innerHeight - this.size()[1]) / 2;
        },
        k: function() {
            return 3.6;
        }
    });
    
     result.push({
        points: function() {
            return POINTS_LIKE;
        },
        size: function() {
            return GET_POINTS_SIZE(this.points());
        },
        offsetX: function() {
            return (window.innerWidth - this.size()[0]) / 2;
        },
        offsetY: function() {
            return (window.innerHeight - this.size()[1]) / 2;
        },
        k: function() {
            return 3.6;
        }
    });
    
    result.push({
        points: function() {
            return POINTS_GOOGLE_PLUS;
        },
        size: function() {
            return GET_POINTS_SIZE(this.points());
        },
        offsetX: function() {
            return (window.innerWidth - this.size()[0]) / 2;
        },
        offsetY: function() {
            return (window.innerHeight - this.size()[1]) / 2;
        },
        k: function() {
            return 3.6;
        }
    });
    
    result.push({
        points: function() {
            return POINTS_SMILE;
        },
        size: function() {
            return GET_POINTS_SIZE(this.points());
        },
        offsetX: function() {
            return (window.innerWidth - this.size()[0]) / 2;
        },
        offsetY: function() {
            return (window.innerHeight - this.size()[1]) / 2;
        },
        k: function() {
            return 3.6;
        }
    });
    
       result.push({
        points: function() {
            return POINTS_FACEBOOK;
        },
        size: function() {
            return GET_POINTS_SIZE(this.points());
        },
        offsetX: function() {
            return (window.innerWidth - this.size()[0]) / 2;
        },
        offsetY: function() {
            return (window.innerHeight - this.size()[1]) / 2;
        },
        k: function() {
            return 3.6;
        }
    });
    
    
    

    return result;
}