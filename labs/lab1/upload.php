<?php

include_once 'lib/WideImage.php';

function startsWith($haystack, $needle) {
    return !strncmp($haystack, $needle, strlen($needle));
}

function endsWith($haystack, $needle) {
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}

function multiple(array $_files, $top = true) {
    $files = array();
    foreach ($_files as $name => $file) {
        if ($top) {
            $sub_name = $file['name'];
        } else {
            $sub_name = $name;
        }

        if (is_array($sub_name)) {
            foreach (array_keys($sub_name) as $key) {
                $files[$name][$key] = array(
                    'name' => $file['name'][$key],
                    'type' => $file['type'][$key],
                    'tmp_name' => $file['tmp_name'][$key],
                    'error' => $file['error'][$key],
                    'size' => $file['size'][$key],
                );
                $files[$name] = multiple($files[$name], false);
            }
        } else {
            $files[$name] = $file;
        }
    }
    return $files;
}

if (isset($_FILES) && is_array($_FILES) && isset($_FILES["music_file"]) && is_array($_FILES["music_file"])) {
    $files = multiple($_FILES);
    $file = $files["music_file"]; 

    if ($file["error"] == UPLOAD_ERR_OK) {
        $name = strtolower($file["name"]);
        $tmp_name = $file["tmp_name"];

        if (endsWith($name, ".mp3")) {
            $vector = md5(uniqid() . time());
            
            if (move_uploaded_file($tmp_name, "songs/$vector$name")) {
                echo json_encode(array("error" => 0, "file" => "songs/$vector$name"));

                return;
            } 
        }
    }
}

if (isset($_FILES) && is_array($_FILES) && isset($_FILES["photo_file"]) && is_array($_FILES["photo_file"])) {
    $file = $_FILES["photo_file"];

    if ($file["error"] == UPLOAD_ERR_OK) {
        $files = multiple($_FILES);

        $paths = array();

        foreach ($files as $key => $file) {
            $name = strtolower($file["name"]);
            $tmp_name = $file["tmp_name"];

            $rotation = 0;

            $exif = exif_read_data($tmp_name);
            if (!empty($exif['Orientation'])) {
                switch ($exif['Orientation']) {
                    case 8:
                        $rotation = 90;
                        break;
                    case 3:
                        $rotation = 180;
                        break;
                    case 6:
                        $rotation = 270;
                        break;
                    default:
                        break;
                }
            }

            if (endsWith($name, ".jpg") || endsWith($name, ".jpeg") || endsWith($name, ".png") || endsWith($name, ".gif") || endsWith($name, ".bmp")) {
                try {
                    $vector = md5(uniqid() . time());
                    
                    $image = WideImage::load($file["tmp_name"])->resize(1368, 1368, 'inside', 'down')->rotate($rotation);
                    
                    $image->saveToFile("frames/$vector$name", 90);
                    $image->resize(200, 200, 'inside', 'down')->saveToFile("frames/thumb_$vector$name", 90);
                    
                    $paths[] = "frames/$vector$name";
                } catch (Exception $exc) {
                    
                }
            }
        }

        if (count($paths) > 0) {
            echo json_encode(array("error" => 0, "files" => $paths));

            return;
        }
    }
}

echo json_encode(array("error" => 1));
?>