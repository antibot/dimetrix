<!DOCTYPE html>
<html>
    <head prefix="og: http://ogp.me/ns#">
        <meta charset="utf-8">
        <title>Freedom of speech, freedom of thought</title>

        <meta name="description" content="Freedom of speech, freedom of thought" />
        <meta name="keywords" content="html5, css3, canvas, svg, webgl, audio, video, partition, waveform, api, universe, magic, 3d, threejs, animation" />

        <meta property="og:title" content="Freedom of speech, freedom of thought" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="http://dimetrix.ru/" />
        <meta property="og:image" content="http://dimetrix.ru/images/logo.png" />
        <meta property="og:description" content="Freedom of speech, freedom of thought" />
        <meta property="og:site_name" content="dimetrix.ru" />
        <meta prefix="fb: http://ogp.me/ns/fb#" property="fb:app_id" content="144073869062952">

        <link rel="apple-touch-icon-precomposed" href="//dimetrix.ru/images/apple_touch_icon.png" />

        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">

        <meta name="application-name" content="Dimetrix" />
        <meta name="msapplication-TileColor" content="#ffffff" />
        <meta name="msapplication-TileImage" content="//dimetrix.ru/images/thumbnail.png" />

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale = 1.0, maximum-scale = 1.0">

        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.7.0.min.js"></script>
    </head>
    <body>
        <p id="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>

        <div id="panel">
            <table border="0" celpadding="0" cellspacing="0" width="980px"> 
                <tr>
                    <td width="380px">
                        <div class="col-md-12">
                            Музыка по ссулке:
                        </div>
                    </td>  
                    <td width="600px">
                        <div class="col-md-12">
                            Фото по ссулке:
                        </div>
                    </td>  
                </tr>
                <tr>
                    <td width="380px">
                        <div class="col-md-8">
                            <input type="text" class="form-control music_url" placeholder="Sound URL" required autofocus/>
                        </div>  
                    </td>  
                    <td width="600px">
                        <div class="col-md-8">
                            <input type="text" class="form-control photo_url" placeholder="Photo URL" required autofocus />
                        </div>  
                        <div class="col-md-4">
                            <button class="btn btn-primary btn-block photo_add">Добавить</button>
                        </div>
                    </td>  
                </tr>
                <tr>
                    <td width="380px">
                        <div class="col-md-12">
                            Музыка из файла:<span class="music_file_loading"></span>
                        </div>
                    </td>  
                    <td width="600px">
                        <div class="col-md-12">
                            Фото из файла:<span class="photo_file_loading"></span>
                        </div>
                    </td>  
                </tr>

                <tr>
                    <td width="380px">
                        <div class="col-md-10">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-primary btn-file">
                                        Выбрать…<span class="music_file_progress"></span> <input type="file" name="music_file" id="fileupload" class="music_file" />
                                    </span>
                                </span>
                                <input type="text" class="form-control music_info" readonly="">
                            </div>    
                        </div>
                    </td>  
                    <td width="600px">
                        <div class="col-md-10">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-primary btn-file">
                                        Выбрать…<span class="photo_file_progress"></span> <input type="file" name="photo_file" id="fileupload" class="photo_file" multiple="multiple" />
                                    </span>
                                </span>
                                <input type="text" class="form-control photo_info" readonly="">
                            </div>    
                        </div>
                    </td>  
                </tr>

                <tr>
                    <td width="380px">
                    </td> 
                    <td width="600px">
                        <div class="col-md-12">
                            <div>
                                <ul class="photos_list btn-block">

                                </ul>
                            </div>

                        </div>
                    </td>  
                </tr>
                <tr>
                    <td width="380px">
                        <div class="col-md-12">
                            <button class="btn btn-lg btn-primary btn-block preview">Просмотреть <span class="preview_progress"></span></button>
                        </div>

                    </td>  
                    <td width="600px">
                        <div class="col-md-12">
                            <button class="btn btn-lg btn-primary btn-block save">Сохранить <span class="save_progress"></span></button>
                        </div>
                    </td>  
                </tr>
                <tr>
                    <td width="980px" colspan="2">
                        <br />
                        <div class="col-md-12">
                            <button class="stop btn btn-danger btn-block" >Приостановить</button>
                        </div>
                    </td>  

                </tr>
                <tr>
                    <td width="980px" colspan="2">
                        <br />
                        <div class="col-md-12">
                            <a href="" title="Ссылка на видео" target="_blank" class="link btn btn-success btn-block" ></a>
                        </div>
                    </td>  

                </tr>
            </table>
        </div>

        <div id="panel_toggle"></div>

        <div id="WORK_AREA">
            <canvas id="graphics" width="0" height="0"></canvas>
        </div>

        <script>
<?php
require_once './medoo.min.php';

$photos = array();
$sounds = array();
if (isset($_REQUEST) && !empty($_REQUEST) && isset($_REQUEST['id']) && !empty($_REQUEST['id'])) {
    $id = $_REQUEST['id'];

    /*
      $database = new medoo(array(
      'database_type' => 'mysql',
      'database_name' => 'slideshow',
      'server' => 'localhost',
      'username' => 'root',
      'password' => '',
      ));
     */

    $database = new medoo(array(
        'database_type' => 'mysql',
        'database_name' => 'selikhovdmitrey',
        'server' => 'localhost',
        'username' => 'selikhovdmitrey',
        'password' => 'ghbdtnlbvjrc238',
            ));

    $datas = $database->select('slideshow', '*', array(
        'link' => $id,
            ));

    foreach ($datas as $data) {
        $photos_list = json_decode($data['photos']);
        $sounds_list = json_decode($data['sounds']);

        $photos = array_merge($photos, $photos_list);
        $sounds = array_merge($sounds, $sounds_list);
    }

    foreach ($photos as $key => &$value) {
        $value = '"' . $value . '"';
    }
    foreach ($sounds as $key => &$value) {
        $value = '"' . $value . '"';
    }
}

echo 'var GLOBAL_PHOTOS = [' . (implode(', ', $photos)) . '];';
echo 'var GLOBAL_SOUNDS = [' . (implode(', ', $sounds)) . '];';
?>

            GLOBAL_PHOTOS = GLOBAL_PHOTOS && GLOBAL_PHOTOS.length ? GLOBAL_PHOTOS : ["http://dimetrix.ru/labs/lab1/frames/87d72af7f5f56b3719b22b6bdb83d6f9rewalls.com_72468.jpg", "http://dimetrix.ru/labs/lab1/frames/57dd9278c2bbcfa5ac2bc23de261991enovyi_god_i_rozhdestvo-1920x1200 (1).jpg", "http://dimetrix.ru/labs/lab1/frames/3a148fd44115b40df8450baf529a0134nw.jpg", "http://dimetrix.ru/labs/lab1/frames/28e76e77588dfb3fd370e5b552f5f7d3109350_original.jpg", "http://dimetrix.ru/labs/lab1/frames/4a381df63d9d22a1308005aa487a5ce4b0b0ba8f3921750ee79429767900a8a9.jpg", "http://dimetrix.ru/labs/lab1/frames/c05f6a6f887ef2b2b3e3549516a526941319899143_216939-2566x1711.jpg", "http://dimetrix.ru/labs/lab1/frames/4ea7bd90a7bd0f00acdac1ec56b121cfnewyear-moscow.jpg", "http://dimetrix.ru/labs/lab1/frames/0b9aea2ed8367ff7daf35dab1680c419novyi_god_i_rozhdestvo-1920x1200.jpg"];
            // ['songs/8.mp3', 'songs/1.mp3', 'songs/2.mp3', 'songs/3.mp3', 'songs/4.mp3', 'songs/5.mp3', 'songs/6.mp3', 'songs/7.mp3'];

            GLOBAL_SOUNDS = GLOBAL_SOUNDS && GLOBAL_SOUNDS.length ? GLOBAL_SOUNDS : ['songs/3.mp3', 'songs/be2402c154e03ecd24b54d1b7977b066a4adb65db6c61d.mp3', 'songs/new_year2.mp3', 'songs/new_year3.mp3', 'songs/new_year4.mp3'];

        </script>

        <script src="js/plugins.js"></script>

        <script src="//code.jquery.com/jquery-1.9.1.js"></script>
        <script src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

        <script src="js/vendor/processing-1.4.1.js"></script>
        <script src="js/vendor/jquery.browser.js"></script>
        <script src="js/vendor/swfobject.js"></script>

        <script src="js/vendor/jquery.mousewheel.js"></script>
        <script src="js/vendor/jquery.iframe-transport.js"></script>
        <script src="js/vendor/jquery.ui.widget.js"></script>
        <script src="js/vendor/jquery.fileupload.js"></script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/lib/utils.js"></script>
        <script src="js/lib/pixels.js"></script>
        <script src="js/lib/points.js"></script>

        <script src="js/lib/transitions.js"></script>

        <script src="js/lib/slideshows.js"></script>

        <script src="js/lib/components.js"></script>

        <script src="js/player.js"></script>
        <script src="js/main.js"></script>
        <script src="js/editor.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-45926920-1', 'dimetrix.ru');
            ga('send', 'pageview');

        </script>
    </body>
</html>
