(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for (var i = 0; i < vendors.length && !window.requestAnimationFrame; ++i) {
        window.requestAnimationFrame = window[vendors[i] + 'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[i] + 'CancelAnimationFrame'] || window[vendors[i] + 'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() {
                callback(currTime + timeToCall);
            }, timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

var FPS = function(debug) {
    var self = this;

    var startTime = Date.now();
    var currentTime = startTime;
    var frames = 0;
    var lastFrames = 0;

    self.exec = function() {
        frames++;
        currentTime = Date.now();
        if (currentTime - startTime >= 1000) {
            console.log('fps: ' + frames);

            debug.innerHTML = frames;

            lastFrames = frames;
            frames = 0;
            startTime = currentTime;
        }

        // ---

        return lastFrames;
    };

    return self;
};

var UPDATER = function(canvas, context) {
    var self = this;

    self.exec = function() {

    };

    return self;
};

var RENDERER = function(canvas, context) {
    var self = this;

    var pixels = [], i, j, w = 30, h = 30, count = w * h, index;

    for (i = 0; i < count; i++) {
        //var image = new Image();
        //image.src = drawImage();
        
    var element = document.createElement('span');
    element.innerHTML = i;
    element.style.position = 'absolute';
    document.body.appendChild(element);
        
        pixels.push({
            //image: image,
            element: element,
            x: Math.random() * window.innerWidth,
            y: window.innerHeight / 2,
            toX: Math.random() * window.innerWidth,
            toY: Math.random() * window.innerHeight,
            scale: Math.random(),
            toScale: 1,
            angle: Math.random(),
            toAngle: 1,
            size: 10,
            toSize: 10,
            impulsX: 1,
            impulsY: 1,
            toImpulsX: 1,
            toImpulsY: 1,
            color: "rgb(" + (Math.floor(Math.random() * 255)) + "," + (Math.floor(Math.random() * 255)) + "," + (Math.floor(Math.random() * 255)) + ")"
        });
    } 

    self.exec = function() {
        for (i = 0; i < w; i++) {
            for (j = 0; j < h; j++) {
                var pixel = pixels[i * h + j];
                pixel.x = i * 10 + 50;
                pixel.y = j * 10 + 50;
                
                //context.fillStyle = pixel.color;
                //context.fillRect(pixel.x, pixel.y, 8, 8);
                
                //context.beginPath();
                //context.arc(pixel.x, pixel.y, 4, 0, Math.PI * 2);
                //context.closePath();
                //context.fill();
                
                //context.drawImage(pixel.image, pixel.x, pixel.y, image.width, image.height);
                
                pixel.element.style['backgroundColor'] = pixel.color;
                 pixel.element.style['left'] = pixel.x;
                  pixel.element.style['top'] = pixel.y;
            }
        }
    };

    return self;
};

(function(window, document) {

    var debug = document.getElementById('debug');

    var canvas = document.getElementById('graphics');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    window.onresize = function(event) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    };

    var context = canvas.getContext('2d');
    var updater = new UPDATER(canvas, context);
    var rendered = new RENDERER(canvas, context);
    var fps = new FPS(debug);

    (function loop() {
        // --- clear

        context.save();
        context.setTransform(1, 0, 0, 1, 0, 0);
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.restore();

        // ---

        updater.exec();
        rendered.exec();
        fps.exec();

        // --- loop

        requestAnimationFrame(loop);
    })();

})(window, document);