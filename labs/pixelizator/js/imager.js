// Class containing image data
function ImageObject(width, height) {
    var self = this;

    self.header = '';
    self.data = Array();
    self.width = width;
    self.height = height;

    return self;
}

// Convert a value to a little endian hexadecimal value
function getLittleEndianHex(value) {
    var result = [];

    for (var bytes = 4; bytes > 0; bytes--) {
        result.push(String.fromCharCode(value & 255));
        value >>= 8;
    }

    return result.join('');
}

// Set the required bitmap header
function setImageHeader(imageObject) {
    var numFileBytes = getLittleEndianHex(imageObject.width * imageObject.height);
    var w = getLittleEndianHex(imageObject.width);
    var h = getLittleEndianHex(imageObject.height);

    imageObject.header =
            'BM' + // Signature
            numFileBytes + // size of the file (bytes)*
            '\x00\x00' + // reserved
            '\x00\x00' + // reserved
            '\x36\x00\x00\x00' + // offset of where BMP data lives (54 bytes)
            '\x28\x00\x00\x00' + // number of remaining bytes in header from here (40 bytes)
            w + // the width of the bitmap in pixels*
            h + // the height of the bitmap in pixels*
            '\x01\x00' + // the number of color planes (1)
            '\x20\x00' + // 32 bits / pixel
            '\x00\x00\x00\x00' + // No compression (0)
            '\x00\x00\x00\x00' + // size of the BMP data (bytes)*
            '\x13\x0B\x00\x00' + // 2835 pixels/meter - horizontal resolution
            '\x13\x0B\x00\x00' + // 2835 pixels/meter - the vertical resolution
            '\x00\x00\x00\x00' + // Number of colors in the palette (keep 0 for 32-bit)
            '\x00\x00\x00\x00';  // 0 important colors (means all colors are important)
}

// Fill a rectangle
function fillRectangle(imageObject, x, y, w, h, color) {
    for (var ny = y; ny < y + h; ny++) {
        for (var nx = x; nx < x + w; nx++) {
            imageObject.data[ny * imageObject.width + nx] = color;
        }
    }
}

// Fill a circle
function fillCircle(imageObject, x, y, radius, color) {

}

// Flip image vertically
function flipImage(imageObject) {
    var imageObjectData = [];

    for (var x = 0; x < imageObject.width; x++) {
        for (var y = 0; y < imageObject.height; y++) {
            var ny = imageObject.height - 1 - y;

            imageObjectData[(ny * imageObject.width) + x] = imageObject.data[(y * imageObject.width) + x];
        }
    }

    imageObject.data = imageObjectData;
}

// Main draw function
function drawImage() {
    var imageObject = {
        header: '',
        data: [],
        width: 8,
        height: 8
    };

    setImageHeader(imageObject);

    var r = Math.floor(Math.random() * 200 + 55);
    var g = Math.floor(Math.random() * 200 + 55);
    var b = Math.floor(Math.random() * 200 + 55);

    fillRectangle(imageObject, 0, 0, imageObject.width, imageObject.height, String.fromCharCode(r, g, b, 0));

    //fillRectangle(img, 10, 10, 90, 90, String.fromCharCode(255, 0, 0, 0)); // Blue
    //fillRectangle(img, 110, 10, 90, 90, String.fromCharCode(0, 255, 0, 0)); // Green
    //fillRectangle(img, 10, 110, 90, 90, String.fromCharCode(0, 0, 255, 0)); // Red

    // Flip image vertically	
    flipImage(imageObject);

    //console.log(imageObject.data);

    return 'data:image/bmp;base64,' + (window.btoa ? btoa(imageObject.header + imageObject.data.join('')) : Base64.encode(imageObject.header + imageObject.data.join('')));
}