(function($) {

    window.scrollTo(0, 1);


    // ---

    var pixels = [];

    var transitions_base = [];
    var transitions_border = [];
    var transitions_images = [];
    var transitions_waveform = [];

    var components = [];
    var slideshows = [];

    // ---

    window.photoVisualizator = new Processing("graphics");
    //window.photoVisualizator.externals.context.globalCompositeOperation = 'multiply';
    $(window).resize(function() {
        window.photoVisualizator.size(window.innerWidth, window.innerHeight);
    });
    window.photoVisualizator.render = false;
    window.photoVisualizator.size(window.innerWidth, window.innerHeight);
    window.photoVisualizator.noStroke();
    window.photoVisualizator.frameRate(60);
    window.photoVisualizator.fill(0, 0, 0);
    window.photoVisualizator.background(0);
    window.photoVisualizator.build = function() {
        pixels = GET_PIXELS(window.PIXELS_COUNT);

        transitions_base = GET_TRANSITIONS_BASE(pixels);
        transitions_border = GET_TRANSITIONS_BORDER(pixels);
        transitions_images = GET_TRANSITIONS_IMAGES(pixels);
        transitions_waveform = GET_TRANSITIONS_WAVEFORM(pixels);

        components = GET_COMPONENTS(window.photoVisualizator, transitions_base, transitions_border, transitions_images, transitions_waveform, pixels);
        slideshows = GET_SLIDESHOWS(window.photoVisualizator, transitions_base, transitions_border, transitions_images, transitions_waveform, pixels);

        for (var i = 0; i < slideshows.length; i++) {

            slideshows[i].start = function(objects, from, to) {
                for (var j = 0; j < components.length; j++) {
                    components[j].start(objects, from, to);
                }
            };
            slideshows[i].beforeChange = function(objects, from, to) {
                for (var j = 0; j < components.length; j++) {
                    components[j].beforeChange(objects, from, to);
                }
            };
            slideshows[i].change = function(objects, from, to) {
                for (var j = 0; j < components.length; j++) {
                    components[j].change(objects, from, to);
                }
            };
            slideshows[i].afterChange = function(objects, from, to) {
                for (var j = 0; j < components.length; j++) {
                    components[j].afterChange(objects, from, to);
                }
            };
            slideshows[i].stop = function(objects, from, to) {
                for (var j = 0; j < components.length; j++) {
                    components[j].stop(objects, from, to);
                }
            };
            slideshows[i].loaded = function() {
                window.location = "minutta://mobile/form_Submitted:loaded";

                console.log('loaded');
            };
        }

        console.log("pixels", pixels.length);
    };
    window.photoVisualizator.template = function(type) {
        var countComponents = components.length, countSlideshows = slideshows.length, i;

        for (i = 0; i < countComponents; i++) {
            components[i].template(type);
        }

        for (i = 0; i < countSlideshows; i++) {
            slideshows[i].template(type);
        }
    };
    window.photoVisualizator.effect_duration = function(speed) {
        speed = speed || window.EFFECT_DURATION;

        var countComponents = components.length, countSlideshows = slideshows.length, i;

        for (i = 0; i < countComponents; i++) {
            components[i].effect_duration(speed);
        }

        for (i = 0; i < countSlideshows; i++) {
            slideshows[i].effect_duration(speed);
        }
    };

    window.photoVisualizator.fade_duration_image = function(speed) {
        speed = speed || window.FADE_DURATION;

        var countComponents = components.length, countSlideshows = slideshows.length, i;

        for (i = 0; i < countComponents; i++) {
            components[i].fade_duration_image(speed);
        }

        for (i = 0; i < countSlideshows; i++) {
            slideshows[i].fade_duration_image(speed);
        }
    };

    window.photoVisualizator.fade_duration_transition = function(speed) {
        speed = speed || window.FADE_DURATION;

        var countComponents = components.length, countSlideshows = slideshows.length, i;

        for (i = 0; i < countComponents; i++) {
            components[i].fade_duration_transition(speed);
        }

        for (i = 0; i < countSlideshows; i++) {
            slideshows[i].fade_duration_transition(speed);
        }
    };

    window.photoVisualizator.play = function() {
        this.render = true;
    };
    window.photoVisualizator.stop = function() {
        this.render = false;
    };

    window.photoVisualizator.mouseMoved = function() {
        var countComponents = components.length, countSlideshows = slideshows.length, i;

        for (i = 0; i < countComponents; i++) {
            components[i].mx = window.photoVisualizator.mouseX;
            components[i].my = window.photoVisualizator.mouseY;
        }

        for (i = 0; i < countSlideshows; i++) {
            slideshows[i].mx = window.photoVisualizator.mouseX;
            slideshows[i].my = window.photoVisualizator.mouseY;
        }
    };

    window.photoVisualizator.mousePressed = function() {
        var countComponents = components.length, countSlideshows = slideshows.length, i;

        for (i = 0; i < countComponents; i++) {
            components[i].pressed(window.photoVisualizator.mouseX, window.photoVisualizator.mouseY);
        }

        for (i = 0; i < countSlideshows; i++) {
            slideshows[i].pressed(window.photoVisualizator.mouseX, window.photoVisualizator.mouseY);
        }
    };

    window.photoVisualizator.mouseReleased = function() {
        var countComponents = components.length, countSlideshows = slideshows.length, i;

        for (i = 0; i < countComponents; i++) {
            components[i].released(window.photoVisualizator.mouseX, window.photoVisualizator.mouseY);
        }

        for (i = 0; i < countSlideshows; i++) {
            slideshows[i].released(window.photoVisualizator.mouseX, window.photoVisualizator.mouseY);
        }
    };

    window.photoVisualizator.draw = function() {
        if (!this.render) {
            return;
        }

        var countComponents = components.length, countSlideshows = slideshows.length, i;

        for (i = 0; i < countSlideshows; i++) {
            slideshows[i].update();
        }

        for (i = 0; i < countComponents; i++) {
            components[i].update();
        }

        window.photoVisualizator.background(0);

        for (i = 0; i < countSlideshows; i++) {
            slideshows[i].draw();
        }

        for (i = 0; i < countComponents; i++) {
            components[i].draw();
        }
    };

    window.photoVisualizator.loop();

})(jQuery);