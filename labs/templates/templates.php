<?php

define('RELEASE', true);
define('PHOTO_PREFIX', 'http://mimo-us.cloudapp.net/');
define('SOUND_PREFIX', 'http://mimo-us.cloudapp.net/apidata/templates_data/songs/');

$PREFIX = RELEASE ? "http://mimo-us.cloudapp.net/apidata/templates_data/" : "templates_data/";

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Freedom of speech, freedom of thought</title>

        <meta name="description" content="Freedom of speech, freedom of thought" />
        <meta name="keywords" content="html5, css3, canvas, svg, webgl, audio, video, partition, waveform, api, universe, magic, 3d, threejs, animation" />

        <link rel="apple-touch-icon-precomposed" href="//dimetrix.ru/images/apple_touch_icon.png" />

        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">

        <meta name="application-name" content="Dimetrix" />
        <meta name="msapplication-TileColor" content="#ffffff" />
        <meta name="msapplication-TileImage" content="//dimetrix.ru/images/thumbnail.png" />

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale = 1.0, maximum-scale = 1.0">

        <link rel="shortcut icon" href="<?= $PREFIX ?>favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?= $PREFIX ?>favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="<?= $PREFIX ?>css/normalize.css">
        <link rel="stylesheet" href="<?= $PREFIX ?>css/main.css">

        <script src="<?= $PREFIX ?>js/vendor/modernizr-2.7.0.min.js"></script>
    </head>
    <body>
        <canvas id="graphics" width="0" height="0"></canvas>

        <div id="loading_button"></div>
        <div id="play_button"></div>

        <script>
            GLOBAL_PHOTOS = [];
            GLOBAL_SOUNDS = ['songs/Cosmic_Analog_Ensemble_-_33_-_Urban_Tropic_Pt_iii.mp3', 'songs/Fhernando_-_11_-_Love_Story.mp3', 'songs/Krestovsky_-_01_-_Rutter.mp3', 'songs/Tom Waits - Time.mp3', 'songs/Zee_Avi_-You_And_Me.mp3'];
        </script>

        <script src="<?= $PREFIX ?>js/plugins.js"></script>

        <script src="//code.jquery.com/jquery-1.9.1.js"></script>

        <script src="<?= $PREFIX ?>js/vendor/processing-1.4.1.js"></script>
        <script src="<?= $PREFIX ?>js/vendor/jquery.browser.js"></script>

        <script src="<?= $PREFIX ?>js/lib/config.js"></script>
        <script src="<?= $PREFIX ?>js/lib/utils.js"></script>
        <script src="<?= $PREFIX ?>js/lib/pixels.js"></script>
        <script src="<?= $PREFIX ?>js/lib/points.js"></script>

        <script src="<?= $PREFIX ?>js/lib/transitions.js"></script>

        <script src="<?= $PREFIX ?>js/lib/slideshows.js"></script>

        <script src="<?= $PREFIX ?>js/lib/components.js"></script>

        <script src="<?= $PREFIX ?>js/player.js"></script>
        <script src="<?= $PREFIX ?>js/main.js"></script>
        <script src="<?= $PREFIX ?>js/editor.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-45926920-1', 'dimetrix.ru');
            ga('send', 'pageview');

        </script>
    </body>
</html>
