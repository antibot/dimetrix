function rotate_point(pointX, pointY, originX, originY, angle) {
    angle = angle * Math.PI / 180.0;
    return {
        x: Math.cos(angle) * (pointX - originX) - Math.sin(angle) * (pointY - originY) + originX,
        y: Math.sin(angle) * (pointX - originX) + Math.cos(angle) * (pointY - originY) + originY
    };
}

// ---


var SCREEN_WIDTH = 1024, SCREEN_HEIGHT = 768;

var RADIUS = 200, RADIUS_SCALE = 1;

var QUANTITY = 100;

var canvas1, canvas2;
var context1, context2;
var particles;

var type = 1;

var positionX = SCREEN_WIDTH / 2, positionY = SCREEN_HEIGHT / 2;
var offsetX = 0, offsetY = 80;

function init() {

    // --- canvas1

    canvas1 = document.getElementById('graphics1');

    if (canvas1 && canvas1.getContext) {
        context1 = canvas1.getContext('2d');

        // Register event listeners
        //document.addEventListener('click', documentClickHandler, false);
        //document.addEventListener('mousemove', documentMouseMoveHandler, false);
        //document.addEventListener('mousewheel', documentMouseWheelHandler, false);
        //document.addEventListener('mousedown', documentMouseDownHandler, false);
        //document.addEventListener('mouseup', documentMouseUpHandler, false);
        //canvas.addEventListener('touchstart', canvasTouchStartHandler, false);
        //canvas.addEventListener('touchmove', canvasTouchMoveHandler, false);

        // ---

        particles = [];

        for (var i = 0; i < QUANTITY; i++) {
            particles[i] = {
                index: 0,
                position: {x: positionX, y: positionY},
                shift: {x: positionX, y: positionY},
                size: 1,
                angle: 0,
                speed: 0.005 + Math.random() * 0.005,
                targetSize: 1,
                fillColor: '#' + (Math.random() * 0x404040 + 0xaaaaaa | 0).toString(16),
                orbit: RADIUS * .8 + (RADIUS * .2 * Math.random())
            };
        }
    }

    // --- canvas2

    canvas2 = document.getElementById('graphics2');

    if (canvas2 && canvas1.getContext) {
        context2 = canvas1.getContext('2d');

    }

    // ---

    windowResizeHandler();

    setInterval(loop, 1000 / 60);

    window.addEventListener('resize', windowResizeHandler, false);
}

//function documentClickHandler(event) {
//mouseX = event.clientX - (window.innerWidth - SCREEN_WIDTH) * .5;
//mouseY = event.clientY - (window.innerHeight - SCREEN_HEIGHT) * .5;
//}

//function documentMouseMoveHandler(event) {
//mouseX = event.clientX - (window.innerWidth - SCREEN_WIDTH) * .5;
//mouseY = event.clientY - (window.innerHeight - SCREEN_HEIGHT) * .5;
//}

function documentMouseWheelHandler(event) {
    type = (type + 1) % 3;
}

function windowResizeHandler() {
    //SCREEN_WIDTH = window.innerWidth;
    //SCREEN_HEIGHT = window.innerHeight;

    canvas1.width = SCREEN_WIDTH;
    canvas1.height = SCREEN_HEIGHT;

    canvas1.style.left = (window.innerWidth - SCREEN_WIDTH) * .5 + 'px';
    canvas1.style.top = (window.innerHeight - SCREEN_HEIGHT) * .5 + 'px';

    // ---

    canvas2.width = SCREEN_WIDTH;
    canvas2.height = SCREEN_HEIGHT;

    canvas2.style.left = (window.innerWidth - SCREEN_WIDTH) * .5 + 'px';
    canvas2.style.top = (window.innerHeight - SCREEN_HEIGHT) * .5 + 'px';
}

function loop() {
    //context1.clearRect(0, 0, canvas1.width, canvas1.height);
    //context1.fillStyle = 'rgba(0,0,0,1)';
    //context1.fillRect(0, 0, canvas1.width, canvas1.height);

    context2.fillStyle = 'rgba(0, 0, 0, 1)';
    context2.fillRect(0, 0, canvas2.width, canvas2.height);

    for (i = 0, len = particles.length; i < len; i++) {
        var particle = particles[i];

        var lp = {x: particle.position.x, y: particle.position.y};

        // Offset the angle to keep the spin going
        particle.angle += particle.speed;

        // Follow mouse with some lag
        particle.shift.x += (positionX - particle.shift.x) * (particle.speed);
        particle.shift.y += (positionY - particle.shift.y) * (particle.speed);

        switch (type) {
            case 0: // --- Circle
                particle.position.x = particle.shift.x + (Math.cos(i + particle.angle) * (particle.orbit * RADIUS_SCALE));
                particle.position.y = particle.shift.y + (Math.sin(i + particle.angle) * (particle.orbit * RADIUS_SCALE));
                break;
            case 1: // -- Heart
                particle.position.x = particle.shift.x + (Math.cos(i + particle.angle) * 1.5 * (particle.orbit * RADIUS_SCALE));
                particle.position.y = particle.shift.y + ((Math.sin(i + particle.angle) + Math.pow(Math.abs(Math.cos(i + particle.angle)), 0.5)) * (particle.orbit * RADIUS_SCALE));

                var p = rotate_point(particle.position.x, particle.position.y, particle.shift.x, particle.shift.y, 180);
                particle.position.x = p.x;
                particle.position.y = p.y;
                break;
            case 2:
                particle.position.x = particle.shift.x + (Math.cos(i + particle.angle) * (particle.orbit * RADIUS_SCALE));
                particle.position.y = particle.shift.y + (Math.sin(i + particle.angle) * (particle.orbit * RADIUS_SCALE)) * 0.5;
                break;
            case 3:
                var x = Math.cos(i + particle.angle);
                var y = (i % 2 ? 1 : -1) * Math.pow(1 - Math.pow(x, 4), 1 / 4);

                particle.position.x = particle.shift.x + x * (particle.orbit * RADIUS_SCALE);
                particle.position.y = particle.shift.y + y * (particle.orbit * RADIUS_SCALE);

                break;
        }

        // --- 

        // Limit to screen bounds
        particle.position.x = Math.max(Math.min(particle.position.x + offsetX, SCREEN_WIDTH), 0);
        particle.position.y = Math.max(Math.min(particle.position.y + offsetY, SCREEN_HEIGHT), 0);

        particle.size += (particle.targetSize - particle.size) * 0.05;

        // If we're at the target size, set a new one. Think of it like a regular day at work.
        if (Math.round(particle.size) === Math.round(particle.targetSize)) {
            particle.targetSize = 1 + Math.random() * 7;
        }

        // --- draw

        context2.beginPath();
        context2.fillStyle = particle.fillColor;
        context2.strokeStyle = particle.fillColor;
        context2.lineWidth = particle.size;
        context2.moveTo(lp.x, lp.y);
        context2.lineTo(particle.position.x, particle.position.y);
        context2.stroke();
        context2.arc(particle.position.x, particle.position.y, particle.size / 2, 0, Math.PI * 2, true);
        context2.fill();
    }
}

// --- Test

init();
	