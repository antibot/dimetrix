var INTERVALS = [];

function GET_SLIDESHOWS(canvas, context) {
    if (!canvas || !context) {
        return [];
    }

    var loaded = [];

    if (GLOBAL_PHOTOS && GLOBAL_PHOTOS.length && GLOBAL_PHOTOS.length === 1) {
        GLOBAL_PHOTOS.push(GLOBAL_PHOTOS[0]);
    }

    $.each(GLOBAL_PHOTOS, function(index, url) {
        var image = new Image();
        image.src = url;

        console.log(image);

        loaded[index] = image;
    });

    $.each(INTERVALS, function(index, interval) {
        clearInterval(interval);
    });

    INTERVALS = [];

    var KenBurns = function() {
        var objects = [];

        $.each(GLOBAL_PHOTOS, function(index, url) {
            var object = {
                index: index,
                frame: 0,
                image: loaded[index],
                action: false,
                r1: [],
                r2: [],
                scale: 1.5,
                effect_time: 8000,
                fade_time: 2000,
                start_time: 0,
                update_time: 0,
                colors: COLORS[index] || []
            };

            objects.push(object);
        });

        var index = 0;
        var beforeChangeDispatched = false;
        var afterChangeDispatched = false;
        var startDispatched = false;
        var stopDispatched = false;

        var result = {
            mx: 0,
            my: 0,
            impulsX: 0,
            impulsY: 0,
            impulsToX: 0,
            impulsToY: 0,
            template: function(type) {

            },
            restart: function() {
                $.each(objects, function(index, object) {
                    object.action = false;
                });
            },
            effect_duration: function(speed) {
                $.each(objects, function(index, object) {
                    object.effect_time = speed;

                    console.log(speed);
                });
            },
            fade_duration_image: function(speed) {
                $.each(objects, function(index, object) {
                    object.fade_time = speed;

                    console.log(speed);
                });
            },
            fade_duration_transition: function(speed) {

            },
            start: function(objects, from, to) {

            },
            beforeChange: function(objects, from, to) {

            },
            change: function(objects, from, to) {

            },
            afterChange: function(objects, from, to) {

            },
            stop: function(objects, from, to) {

            },
            loaded: function() {

            },
            pressed: function(mouseX, mouseY) {

            },
            released: function(mouseX, mouseY) {

            },
            update: function() {

            },
            frame: function(position) {
                if (!objects || !objects.length || position < 0 || position >= objects.length) {
                    return;
                }

                var object = objects[position];
                var image = object.image;

                var from = position;
                var to = (position + 1) % objects.length;

                if (object.action) {
                    var update_time = Date.now() - object.start_time;

                    var alpha = 1;

                    var rect = INTERPOLATE_RECT(object.r1, object.r2, update_time / object.effect_time);

                    if (update_time > (object.effect_time - object.fade_time)) {
                        this.frame((index + 1) % objects.length);
                        if (update_time > object.effect_time) {
                            object.action = false;

                            object.frame = 0;

                            alpha = 0;

                            index = (index + 1) % objects.length;

                            if (this.change) {
                                console.log('change ' + (Date.now() - object.start_time));
                                this.change.call(this, objects, from, to);
                            }
                        } else {
                            if (this.beforeChange && !beforeChangeDispatched) {
                                console.log('beforeChange ' + (Date.now() - object.start_time));
                                beforeChangeDispatched = true;
                                this.beforeChange.call(this, objects, from, to);
                            }

                            alpha = Math.max(0, Math.min(1, 1 - (object.fade_time - (object.effect_time - update_time)) / object.fade_time));
                        }
                    } else {
                        if (update_time > object.fade_time) {
                            if (this.afterChange && !afterChangeDispatched) {
                                console.log('afterChange ' + (Date.now() - object.start_time));
                                afterChangeDispatched = true;
                                this.afterChange.call(this, objects, from, to);
                            }
                        }
                    }

                    if (alpha >= 0) {
                        if (alpha < 1) {
                            context.save();

                            context.globalAlpha = alpha;
                        }

                        context.drawImage(image, rect[0], rect[1], rect[2] - rect[0], rect[3] - rect[1]);

                        if (alpha < 1) {
                            context.restore();
                        }
                    }

                    object.frame++;

                    if (this.start && !startDispatched) {
                        startDispatched = true;

                        this.start.call(this, objects, from, to);
                    }
                } else {
                    console.log("check");
                    if (image.width > 0 && image.height > 0) {
                        var size = PROPORTION_POINTS([image.width, image.height], [GET_GRAPHICS_WIDTH(), GET_GRAPHICS_HEIGHT()], false);
                        var rect1 = [0, 0, size[0], size[1]];
                        var rect2 = SCALE_RECT(rect1, object.scale);

                        console.log(size);
                        console.log(rect1);
                        console.log(rect2);

                        // --- offset

                        var align_x = (Math.floor(Math.random() * 3) - 1) / 2;
                        var align_y = (Math.floor(Math.random() * 3) - 1) / 2;

                        var stepX = (rect2[2] - GET_GRAPHICS_WIDTH()) / 2;
                        var x = -stepX + (RANDOM(-stepX / 2, stepX / 2));
                        rect2[0] += x;
                        rect2[2] += x;

                        var stepY = (rect2[3] - GET_GRAPHICS_HEIGHT()) / 2;
                        var y = -stepY + (RANDOM(-stepY / 2, stepY / 2));
                        rect2[1] += y;
                        rect2[3] += y;

                        // --- scale in/out

                        if (object.index % 2) {
                            object.r1 = rect1;
                            object.r2 = rect2;
                        }
                        else {
                            object.r1 = rect2;
                            object.r2 = rect1;
                        }

                        // ---

                        object.action = true;
                        object.start_time = Date.now();

                        beforeChangeDispatched = false;
                        afterChangeDispatched = false;
                    }
                }
            },
            draw: function() {
                this.frame(index);
            }
        };

        INTERVALS.push(setInterval(function() {
            var loaded = 0;
            $.each(objects, function(index, object) {
                if (object.image.loaded) {
                    loaded++;
                }
            });

            if (loaded >= objects.length) {
                $.each(INTERVALS, function(index, interval) {
                    clearInterval(interval);
                });

                INTERVALS = [];

                result.loaded.call(result.loaded);
            }
        }, 100));

        return result;
    };

    return [new KenBurns()];
}