<!DOCTYPE html>
<html>
    <head prefix="og: http://ogp.me/ns#">
        <meta charset="utf-8">
        <title>Freedom of speech, freedom of thought</title>

        <meta name="description" content="Freedom of speech, freedom of thought" />
        <meta name="keywords" content="html5, css3, canvas, svg, webgl, audio, video, partition, waveform, api, universe, magic, 3d, threejs, animation" />

        <meta property="og:title" content="Freedom of speech, freedom of thought" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="http://dimetrix.ru/" />
        <meta property="og:image" content="http://dimetrix.ru/images/logo.png" />
        <meta property="og:description" content="Freedom of speech, freedom of thought" />
        <meta property="og:site_name" content="dimetrix.ru" />
        <meta prefix="fb: http://ogp.me/ns/fb#" property="fb:app_id" content="144073869062952">

        <link rel="apple-touch-icon-precomposed" href="//dimetrix.ru/images/apple_touch_icon.png" />

        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">

        <meta name="application-name" content="Dimetrix" />
        <meta name="msapplication-TileColor" content="#ffffff" />
        <meta name="msapplication-TileImage" content="//dimetrix.ru/images/thumbnail.png" />

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale = 1.0, maximum-scale = 1.0">

        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.7.0.min.js"></script>
    </head>
    <body>
        <canvas id="graphics" width="0" height="0"></canvas>

        <script src="js/plugins.js"></script>

        <script src="//code.jquery.com/jquery-1.9.1.js"></script>

        <script src="js/vendor/processing-1.4.1.js"></script>

        <script src="js/lib/utils.js"></script>
        <script src="js/lib/pixels.js"></script>
        <script src="js/lib/pixelizator.js"></script>

        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-45926920-1', 'dimetrix.ru');
            ga('send', 'pageview');

        </script>
    </body>
</html>
