function GET_PIXELS(count) {
    count = count || 0;

    var pixels = [];

    for (i = 0; i < count; i++) {
        pixels[i] = {
            i: 0,
            j: 0,
            x: 0,
            y: 0,
            toX: 0,
            toY: 0,
            size: 0,
            toSize: 0,
            color: {r: 0, g: 0, b: 0, a: 255},
            r: 0,
            g: 0,
            b: 0,
            toR: 0,
            toG: 0,
            toB: 0,
            speedX: 0,
            speedY: 0
        };
    }

    return pixels;
}