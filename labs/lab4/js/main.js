(function($) {

    window.requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.msRequestAnimationFrame || (function(callback) {
        window.setTimeout(callback, 0);
    });

    function rgba(num) {
        num >>>= 0;
        return {
            r: ((num & 0xFF0000) >>> 16),
            g: ((num & 0xFF00) >>> 8),
            b: (num & 0xFF),
            a: (((num & 0xFF000000) >>> 24))
        };
    }

    // ---

    var fpsCounter = new FPSCounter();
    var visualizator = new Processing("graphics");

    // ---

    var image = visualizator.loadImage('images/2_1.jpg');
    var width = window.innerWidth, height = window.innerHeight, i, j, ii, jj, iii, jjj, w, h, count;
    var impulsX, impulsY, impulsToX, impulsToY;
    var pixels = [];
    var scale = 4;

    var interval = setInterval(function() {
        if (image.loaded && image.width > 0 && image.height > 0) {
            clearInterval(interval);

            var size = PROPORTION_POINTS([image.width, image.height], [150, 150], true);

            image.resize(size[0], size[1]);

            w = image.width, h = image.height, count = w * h;

            for (i = 0; i < w; i++) {
                pixels[i] = GET_PIXELS(h);
                for (j = 0; j < h; j++) {
                    var pixel = pixels[i][j];
                    pixel.i = i;
                    pixel.j = j;
                    pixel.x = i * scale;
                    pixel.y = j * scale;
                    pixel.color = rgba(image.get(i, j));
                }
            }
        }
    }, 100);

    var canvas = document.getElementById('graphics');
    canvas.width = width;
    canvas.height = height;

    window.onresize = function(event) {
        width = window.innerWidth;
        height = window.innerHeight;
        canvas.width = width;
        canvas.height = height;
    };

    var context = canvas.getContext('2d');
    context.fillStyle = 'rgba(0, 0, 0, 1.0)';
    context.globalCompositeOperation = 'source-over';

    var imageContext, imageData;

    function color(posX, posY, size, color) {
        size = size || 1;
        color = color || {r: 0, g: 0, b: 0, a: 255};

        var index;

        for (ii = 0; ii < size; ii++) {
            for (jj = 0; jj < size; jj++) {
                index = (posY + jj) * width * 4 + (posX + ii) * 4;

                imageData[index] = color.r;
                imageData[index + 1] = color.g;
                imageData[index + 2] = color.b;
                imageData[index + 3] = color.a;
            }
        }

    }

    var movable = false, buildable = false, updatable = false, randomizable = false;
    var update_time = new Date().getTime(), build_time = new Date().getTime(), current_time;

    canvas.onclick = function() {
        updatable = true;
        movable = true;
        console.log('click');
    };
    
    var z = 500;
    var offsetX;
    var offsetY;
    

    function update() {
        current_time = new Date().getTime();

        if (current_time - update_time > z) {
            z = 500;
            update_time = current_time;

            randomizable = true;
        }

        if (current_time - build_time > 5000) {
            build_time = current_time;

            buildable = true;
            
            z = 10000;
        }

        for (i = 0; i < w; i++) {
            for (j = 0; j < h; j++) {
                var pixel = pixels[i][j];

                if (movable) {

                    if (buildable) {
                        pixel.toX = pixel.i * scale;
                        pixel.toY = pixel.j * scale;
                    }

                    if (randomizable) {
                        pixel.toX = Math.floor(Math.random() * width);
                        pixel.toY = Math.floor(Math.random() * height);
                    }
                }

                offsetX = Math.floor((pixel.toX - pixel.x) / 10);
                offsetY = Math.floor((pixel.toY - pixel.y) / 10);

                pixel.x = offsetX === 0 ? pixel.toX : offsetX + pixel.x;
                pixel.y = offsetY === 0 ? pixel.toY : offsetY + pixel.y;
            }
        }
        randomizable = false;
        buildable = false;
    }

    function draw() {
        context.fillRect(0, 0, width, height);

        imageContext = context.getImageData(0, 0, width, height);
        imageData = imageContext.data;

        // ---

        if (count) {
            if (updatable) {
                update();
            }

            for (i = 0; i < w; i++) {
                for (j = 0; j < h; j++) {
                    var pixel = pixels[i][j];
                    color(pixel.x, pixel.y, 2, pixel.color);
                }
            }
        }

        // ---

        context.putImageData(imageContext, 0, 0);

        fpsCounter.fps();

        requestAnimationFrame(draw);
    }

    requestAnimationFrame(draw);

})(jQuery);