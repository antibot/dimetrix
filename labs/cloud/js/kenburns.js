/*
 (function() {
 document.getElementById('slideshow').getElementsByTagName('canvas')
 
 var duration = 5 * 1000;
 var slideshow = document.getElementById('slideshow');
 
 if (slideshow) {
 var slides = slideshow.getElementsByTagName('canvas'), count = slides.length, loaded = 0, i = 1;
 var images = [];
 
 if (count > 0) {
 slides[0].className = "fx";
 for (i = 0; i < count; i++) {
 (function() {
 var slide = slides[i];
 var image = new Image();
 
 images.push(image);
 
 image.onload = function() {
 loaded++;
 
 image.width = 480;
 image.height = 360;
 
 slide.width = image.height;
 slide.height = image.height;
 
 slide.style['margin'] = (-image.height / 2) + ' 0 0 ' + (-image.width / 2);
 
 console.log(image.width, image.height);
 
 var context = slide.getContext('2d');
 context.drawImage(image, 0, 0, image.width, image.height);
 
 if (loaded >= count) {
 build();
 }
 
 console.log('image.load');
 };
 image.onerror = function() {
 loaded++;
 
 if (loaded >= count) {
 build();
 }
 
 console.log('image.error');
 };
 
 image.src = slide.getAttribute('data-src');
 })();
 
 
 }
 
 function loop() {
 if (i === count) {
 i = 0;
 }
 
 slides[i].className = "fx";
 
 if (i === 0) {
 slides[count - 2].className = "";
 }
 if (i === 1) {
 slides[count - 1].className = "";
 }
 if (i > 1) {
 slides[i - 2].className = "";
 }
 i++;
 
 }
 
 function build() {
 window.setInterval(loop, duration);
 }
 }
 }
 })();
 */

(function() {
    document.getElementById('slideshow').getElementsByTagName('img')[0].className = "fx";

    var duration = 7 * 1000;
    var slideshow = document.getElementById('slideshow');

    if (slideshow) {
        var images = slideshow.getElementsByTagName('img'), count = images.length, i = 1;

        var build = function() {
                if (i === count) {
                    i = 0;
                }

                images[i].className = "fx";

                if (i === 0) {
                    images[count - 2].className = "";
                }
                if (i === 1) {
                    images[count - 1].className = "";
                }
                if (i > 1) {
                    images[i - 2].className = "";
                }
                i++;
            };

        if (count > 0) {
            var interval = window.setInterval(function() {
                console.log('build');
                
                build();
            }, duration);
        }
    }
})();
