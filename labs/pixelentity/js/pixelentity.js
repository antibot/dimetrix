if (!jQuery.easing.easeInQuad) {
    jQuery.extend(jQuery.easing, {
        easeInQuad: function(e, f, a, h, g) {
            return h * (f /= g) * f + a
        },
        easeOutQuad: function(e, f, a, h, g) {
            return -h * (f /= g) * (f - 2) + a
        },
        easeInOutQuad: function(e, f, a, h, g) {
            if ((f /= g / 2) < 1) {
                return h / 2 * f * f + a
            }
            return -h / 2 * ((--f) * (f - 2) - 1) + a
        },
        easeInCubic: function(e, f, a, h, g) {
            return h * (f /= g) * f * f + a
        },
        easeOutCubic: function(e, f, a, h, g) {
            return h * ((f = f / g - 1) * f * f + 1) + a
        },
        easeInOutCubic: function(e, f, a, h, g) {
            if ((f /= g / 2) < 1) {
                return h / 2 * f * f * f + a
            }
            return h / 2 * ((f -= 2) * f * f + 2) + a
        },
        easeInOutQuint: function(e, f, a, h, g) {
            if ((f /= g / 2) < 1) {
                return h / 2 * f * f * f * f * f + a
            }
            return h / 2 * ((f -= 2) * f * f * f * f + 2) + a
        },
        easeInExpo: function(e, f, a, h, g) {
            return (f === 0) ? a : h * Math.pow(2, 10 * (f / g - 1)) + a
        },
        easeOutExpo: function(e, f, a, h, g) {
            return (f == g) ? a + h : h * (-Math.pow(2, -10 * f / g) + 1) + a
        },
        easeInOutExpo: function(e, f, a, h, g) {
            if (f === 0) {
                return a
            }
            if (f == g) {
                return a + h
            }
            if ((f /= g / 2) < 1) {
                return h / 2 * Math.pow(2, 10 * (f - 1)) + a
            }
            return h / 2 * (-Math.pow(2, -10 * --f) + 2) + a
        },
        easeInElastic: function(f, h, e, l, k) {
            var i = 1.70158;
            var j = 0;
            var g = l;
            if (h === 0) {
                return e
            }
            if ((h /= k) == 1) {
                return e + l
            }
            if (!j) {
                j = k * 0.3
            }
            if (g < Math.abs(l)) {
                g = l;
                i = j / 4
            } else {
                i = j / (2 * Math.PI) * Math.asin(l / g)
            }
            return -(g * Math.pow(2, 10 * (h -= 1)) * Math.sin((h * k - i) * (2 * Math.PI) / j)) + e
        },
        easeOutElastic: function(f, h, e, l, k) {
            var i = 1.70158;
            var j = 0;
            var g = l;
            if (h === 0) {
                return e
            }
            if ((h /= k) == 1) {
                return e + l
            }
            if (!j) {
                j = k * 0.3
            }
            if (g < Math.abs(l)) {
                g = l;
                i = j / 4
            } else {
                i = j / (2 * Math.PI) * Math.asin(l / g)
            }
            return g * Math.pow(2, -10 * h) * Math.sin((h * k - i) * (2 * Math.PI) / j) + l + e
        },
        easeInOutElastic: function(f, h, e, l, k) {
            var i = 1.70158;
            var j = 0;
            var g = l;
            if (h === 0) {
                return e
            }
            if ((h /= k / 2) == 2) {
                return e + l
            }
            if (!j) {
                j = k * (0.3 * 1.5)
            }
            if (g < Math.abs(l)) {
                g = l;
                i = j / 4
            } else {
                i = j / (2 * Math.PI) * Math.asin(l / g)
            }
            if (h < 1) {
                return -0.5 * (g * Math.pow(2, 10 * (h -= 1)) * Math.sin((h * k - i) * (2 * Math.PI) / j)) + e
            }
            return g * Math.pow(2, -10 * (h -= 1)) * Math.sin((h * k - i) * (2 * Math.PI) / j) * 0.5 + l + e
        },
        easeInBack: function(e, f, a, i, h, g) {
            if (g === undefined) {
                g = 1.70158
            }
            return i * (f /= h) * f * ((g + 1) * f - g) + a
        },
        easeOutBack: function(e, f, a, i, h, g) {
            if (g === undefined) {
                g = 1.70158
            }
            return i * ((f = f / h - 1) * f * ((g + 1) * f + g) + 1) + a
        },
        easeInOutBack: function(e, f, a, i, h, g) {
            if (g === undefined) {
                g = 1.70158
            }
            if ((f /= h / 2) < 1) {
                return i / 2 * (f * f * (((g *= (1.525)) + 1) * f - g)) + a
            }
            return i / 2 * ((f -= 2) * f * (((g *= (1.525)) + 1) * f + g) + 2) + a
        },
        easeInBounce: function(e, f, a, h, g) {
            return h - jQuery.easing.easeOutBounce(e, g - f, 0, h, g) + a
        },
        easeOutBounce: function(e, f, a, h, g) {
            if ((f /= g) < (1 / 2.75)) {
                return h * (7.5625 * f * f) + a
            } else {
                if (f < (2 / 2.75)) {
                    return h * (7.5625 * (f -= (1.5 / 2.75)) * f + 0.75) + a
                } else {
                    if (f < (2.5 / 2.75)) {
                        return h * (7.5625 * (f -= (2.25 / 2.75)) * f + 0.9375) + a
                    } else {
                        return h * (7.5625 * (f -= (2.625 / 2.75)) * f + 0.984375) + a
                    }
                }
            }
        },
        easeInOutBounce: function(e, f, a, h, g) {
            if (f < g / 2) {
                return jQuery.easing.easeInBounce(e, f * 2, 0, h, g) * 0.5 + a
            }
            return jQuery.easing.easeOutBounce(e, f * 2 - g, 0, h, g) * 0.5 + h * 0.5 + a
        }
    })
}
;
(function(c) {
    c.pixelentity = c.pixelentity || {
        version: "1.0.0"
    };
    if (c.pixelentity.ticker) {
        return
    }
    var i = [];
    var b = 0;

    function a() {
        return (new Date()).getTime()
    }
    var h, f, d;
    var g = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || false;

    function e() {
        var m, l;
        if (b > 0) {
            m = a();
            for (var k in i) {
                l = i[k];
                if (m - l.last >= l.each) {
                    l.callback(l.last, m);
                    l.last = m
                }
            }
            if (g) {
                g(e)
            }
        }
    }
    var j = c.pixelentity.ticker = {
        register: function(l, k) {
            b++;
            k = (typeof k == "undefined") ? 33 : k;
            if (k > 0) {
                k = parseInt(1000 / k, 10)
            } else {
                if (c.browser.mozilla) {
                    k = parseInt(1000 / 50, 10)
                }
            }
            i.push({
                callback: l,
                last: a(),
                each: k
            });
            if (b == 1) {
                if (g) {
                    g(e)
                } else {
                    h = setInterval(e, 16);
                    f = setInterval(e, 20);
                    d = setInterval(e, 30)
                }
            }
        },
        unregister: function(l) {
            for (var k in i) {
                if (i[k].callback == l) {
                    delete i[k];
                    b--
                }
            }
            if (b <= 0) {
                clearInterval(h);
                clearInterval(f);
                clearInterval(d)
            }
        }
    }
})(jQuery);
(function(c) {
    c.pixelentity = c.pixelentity || {
        version: "1.0.0"
    };
    if (c.pixelentity.preloader) {
        return
    }

    function b(i) {
        var h = c(i.currentTarget);
        var f = h.data("peUtilsLoader");
        if (--f.total <= 0) {
            var d = f.callback;
            var g = f.target;
            h.removeData("peUtilsLoader");
            h.unbind("load error", b);
            d(g)
        }
    }
    var a = c.pixelentity.preloader = {
        load: function(f, i) {
            f = (f instanceof jQuery) ? f : c(f);
            var d = (f[0].tagName.toLowerCase() == "img") ? [f[0]] : f.find("img").get();
            var h;
            var g = [];
            while ((h = d.shift())) {
                if (!(h.src && h.complete)) {
                    g.push(h)
                }
            }
            if (g.length > 0) {
                var e = {
                    target: f,
                    callback: i,
                    total: g.length
                };
                while ((h = g.shift())) {
                    h = c(h);
                    h.data("peUtilsLoader", e).one("load error", b);
                    if (h.attr("data-src")) {
                        h.attr("src", h.attr("data-src"));
                        h.removeAttr("data-src")
                    }
                }
            } else {
                i(f)
            }
        }
    }
})(jQuery);
(function(c) {
    c.pixelentity = c.pixelentity || {
        version: "1.0.0"
    };
    c.pixelentity.hilight = {
        conf: {
            width: 0,
            height: 0,
            transition: "vertbars",
            duration: 1500,
            elements: 30,
            maxSize: 250,
            minSize: 50,
            slideshow: false,
            images: [],
            delay: 3000,
            boost: 0,
            over: false,
            fallback: false
        }
    };
    var d = ["float", "display", "margin-top", "margin-right", "margin-bottom", "margin-left", "position", "top", "left"];
    var a = ["vertbars", "circles", "horizbars", "squares"];

    function b(e, X) {
        var q = this;
        var k = c(this);
        var P = false;
        var t;
        var R, ab;
        var z, ag;
        var I, F, o, x, K;
        var C, Y;
        var S, s, H, N, ah = 0,
                l = 0;
        var af = false;
        var u = false;
        var ad = true;
        var j, ae = 0,
                ai = 1;
        var Q;
        var T = false,
                Z = false,
                B = true;
        var g = 0;

        function G() {
            R = X.width;
            ab = X.height;
            ad = true;
            j = X.images;
            X.boost = Math.min(1, parseFloat(X.boost));
            c.pixelentity.preloader.load(e, L)
        }

        function L() {
            R = R || e.width();
            ab = ab || e.height();
            if (e[0].tagName.toLowerCase() == "img") {
                z = e
            } else {
                z = e.find("img:eq(0)")
            }
            t = c("<span>").width(R).height(ab);
            var n = d.length;
            while (n--) {
                t.css(d[n], e.css(d[n]))
            }
            e.css({
                position: "relative",
                "float": "none",
                margin: 0,
                top: 0,
                left: 0
            });
            e.wrap(t);
            t = e.parent();
            if (X.slideshow) {
                if (j.length === 0 && e.attr("data-destination")) {
                    j = e.attr("data-destination").split("|")
                }
                if (j.length > 0) {
                    ae = setInterval(A, Math.max(X.duration + 200, X.delay))
                } else {
                    X.slideshow = false
                }
            }
            var h = false;
            if (!(X.slideshow || X.over)) {
                h = O()
            }
            ad = false;
            if (h) {
                f(h)
            } else {
                setTimeout(m, 50);
                if (u) {
                    u()
                }
                if (X.over) {
                    t.bind("mouseenter mouseleave", V)
                }
            }
        }

        function O() {
            return X.destination || e.attr("data-destination")
        }

        function m() {
            k.triggerHandler("ready.pixelentity")
        }

        function f(h) {
            if (!h || z == ag) {
                return
            }
            ad = true;
            if (typeof h == "string") {
                ag = c("<img>").attr("src", h)
            } else {
                ag = h
            }
            c.pixelentity.preloader.load(ag, ac)
        }

        function ac(h) {
            ad = false;
            ag = h[0];
            aa();
            if (c.browser.msie && c.browser.version >= 9) {
                setTimeout(E, 50)
            } else {
                E()
            }
        }

        function aa() {
            if (P) {
                if (af) {
                    y()
                }
                return
            }
            P = true;
            if (X.fallback || (c.browser.mozilla && c.browser.version.substr(0, 1) == "0")) {
                y()
            } else {
                I = document.createElement("canvas");
                if (I.getContext) {
                    W()
                } else {
                    y()
                }
            }
        }

        function W() {
            p();
            F = document.createElement("canvas");
            F.width = R;
            F.height = ab;
            o = document.createElement("canvas");
            o.width = R;
            o.height = ab;
            x = document.createElement("canvas");
            x.width = R;
            x.height = ab;
            K = document.createElement("canvas");
            K.width = R;
            K.height = ab;
            I = I.getContext("2d");
            F = F.getContext("2d");
            x = x.getContext("2d");
            K = K.getContext("2d");
            C = [];
            Y = parseInt(X.elements, 10);
            var w = parseInt(X.minSize, 10);
            var h = parseInt(X.maxSize, 10) - w;
            for (var n = 0; n < Y; n++) {
                C.push({
                    x: Math.round(Math.random() * R),
                    y: Math.round(Math.random() * ab),
                    d: Math.round(Math.random() * H),
                    r: Math.round(Math.random() * h + w)
                })
            }
        }

        function y() {
            af = true;
            I = ag;
            p()
        }

        function p() {
            I.width = R;
            I.height = ab;
            S = S || e.offset();
            t.prepend(I);
            c(I).css("position", "absolute").css("z-index", (parseInt(e.css("z-index"), 10) || 0) + 1).offset(S).show();
            H = parseInt(X.duration, 10);
            N = Math.round(1 * H / 3)
        }

        function E() {
            ah = c.now();
            l = 0;
            c.pixelentity.ticker.unregister(v);
            c.pixelentity.ticker.register(v);
            s = X.transition;
            if (s == "random") {
                s = a[Math.round(Math.random() * a.length)]
            } else {
                if (s == "all") {
                    s = a[g++ % a.length]
                }
            }
            if (z instanceof jQuery) {
                if (!af) {
                    z.hide()
                }
                z = z[0]
            }
            if (!af) {
                DRAW_IMAGE(K, z);
                DRAW_IMAGE(x, ag);
            }
            v()
        }

        function D() {
            var r = 0; //Math.random() * 200 + 55;
            var g = 0; //Math.random() * 200 + 55;
            var b = 255; //Math.random() * 200 + 55;

            var w = 0;
            I.globalCompositeOperation = F.globalCompositeOperation = "source-over";
            F.clearRect(0, 0, R, ab);
            F.fillStyle = "rgb(" + r + ", " + g + ", " + b + ")";
            F.lineWidth = 1;
            if (l > N) {
                w = jQuery.easing.easeOutQuad(0, l - N, 0, 1, H - N);
                F.fillStyle = "rgba(" + r + ", " + g + ", " + b + "," + w + ")";
                F.fillRect(0, 0, R, ab);
                I.clearRect(0, 0, R, ab);
                I.globalAlpha = 1 - w;
                DRAW_IMAGE(I, K.canvas);
            } else {
                I.globalAlpha = 1;
                DRAW_IMAGE(I, K.canvas);
            }
            var h, am, al, n;
            var ak = Math.PI * 2;
            var aj;
            for (i = 0; i < Y; i++) {
                aj = C[i];
                if (l > aj.d) {
                    w = jQuery.easing.easeOutQuad(0, l - aj.d, 0, 1, H - aj.d);
                    if (w >= 1) {
                        continue
                    }

                    F.fillStyle = "rgba(" + r + ", " + g + ", " + b + "," + w / 2 + ")";
                    F.strokeStyle = "rgba(" + r + ", " + g + ", " + b + "," + w + ")";
                    switch (s) {
                        case "circles":
                            F.beginPath();
                            F.arc(aj.x, aj.y, Math.round(aj.r * w), 0, ak, false);
                            F.fill();
                            F.stroke();
                            break;
                        case "squares":
                            al = Math.round(aj.r * w);
                            h = aj.x - al;
                            am = aj.y - al;
                            n = al << 1;
                            F.fillRect(h, am, n, n);
                            F.strokeRect(h - 0.5, am - 0.5, n, n);
                            break;
                        case "vertbars":
                            al = Math.round(aj.r * w);
                            h = aj.x - al;
                            n = al << 1;
                            F.fillRect(h, 0, n, ab);
                            F.beginPath();
                            F.moveTo(h + 0.5, 0);
                            F.lineTo(h + 0.5, ab);
                            F.moveTo(h + n + 0.5, 0);
                            F.lineTo(h + n + 0.5, ab);
                            F.stroke();
                            break;
                        case "horizbars":
                            al = Math.round(aj.r * w);
                            am = aj.x - al;
                            n = al << 1;
                            F.fillRect(0, am, R, n);
                            F.beginPath();
                            F.moveTo(0, am + 0.5);
                            F.lineTo(R, am + 0.5);
                            F.moveTo(0, am + n + 0.5);
                            F.lineTo(R, am + n + 0.5);
                            F.stroke();
                            break
                    }
                }
            }
            if (X.boost > 0) {
                w = jQuery.easing.easeOutQuad(0, l - N, 0, 1, H - N);
                I.globalCompositeOperation = "lighter";
                I.globalAlpha = X.boost * (1 - w);
                DRAW_IMAGE(I, F.canvas);
            }
            F.globalCompositeOperation = "source-atop";
            F.globalAlpha = 1;
            DRAW_IMAGE(F, x.canvas);
            I.globalCompositeOperation = "lighter";
            I.globalAlpha = 1;
            DRAW_IMAGE(I, F.canvas);
        }

        function r() {
            ratio = jQuery.easing.easeOutQuad(0, l, 0, 1, H);
            c(I).css("opacity", ratio)
        }

        function v() {
            var h;
            l = c.now() - ah;
            if (l > H) {
                l = H
            }
            if (af) {
                r()
            } else {
                D()
            }
            if (l == H) {
                c.pixelentity.ticker.unregister(v);
                U()
            }
        }

        function U() {
            var h;
            if (af) {
                h = c(z).attr("style");
                c(ag).attr("style", h).show();
                c(z).replaceWith(ag[0])
            }
            h = z;
            z = ag;
            ag = h;
            ah = 0;
            if (u) {
                u();
                u = false
            }
        }

        function J() {
            Z = !Z;
            f(ag)
        }

        function M(h) {
            if (ad || ah !== 0) {
                u = h
            } else {
                h()
            }
        }

        function A() {
            M(function() {
                f(j[ai++ % j.length])
            })
        }

        function V(h) {
            if (!B) {
                T = h.type == "mouseenter";
                if (T != Z) {
                    M(J)
                }
            } else {
                T = true;
                B = false;
                Z = true;
                f(O())
            }
        }
        c.extend(q, {
            bind: function(n, h) {
                k.bind(n, h)
            },
            load: function(h) {
                M(function() {
                    f(h)
                })
            },
            reverse: function() {
                M(J)
            },
            destroy: function() {
                clearInterval(ae);
                c.pixelentity.ticker.unregister(v);
                if (X.over) {
                    t.unbind("mouseenter mouseleave", V)
                }
                if (k) {
                    k.remove()
                }
                if (I) {
                    c(I).remove()
                }
                k = q = undefined;
                e.data("peTransitionHilight", null);
                e = undefined
            }
        });
        G()
    }
    c.fn.peTransitionHilight = function(e) {
        var f = this.data("peTransitionHilight");
        if (f) {
            return f
        }
        e = c.extend(true, {}, c.pixelentity.hilight.conf, e);
        this.each(function() {
            f = new b(c(this), e);
            c(this).data("peTransitionHilight", f)
        });
        return e.api ? f : this
    }
})(jQuery);