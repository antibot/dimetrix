(function(window) {
    window = window || {};

    // --- styleTemplate

    var styleTemplate = [
        '.top_share {',
        'margin: 0 auto;',
        'width: 760px;',
        'color: #fff;',
        '}',
        '.resize_container {',
        'z-index: -1000;',
        'position: absolute;',
        'overflow: hidden;',
        'left: 0;',
        'right: 0;',
        'top: 0;',
        'bottom: 0;',
        '}',
        '.resize_container table {',
        'border: 0px;',
        'border-collapse: collapse;',
        '}',
        '.resize_container table td {',
        'padding: 0px;',
        '}',
        '.resize_container .resize_table {',
        'width: 1970px;',
        'height: 1050px;',
        'position: absolute;',
        'left: 50%;',
        'margin-left: -990px;',
        '}',
        '.resize_container .resize_table .resize_row {',
        '}',
        '.resize_container .resize_table .resize_row td {',
        'background-repeat: no-repeat;',
        'background-position: left top;',
        '}',
        '.resize_container .resize_table .resize_row td.resize_col0 {',
        'width: 760px;',
        'min-width: 760px;',
        '}',
        '.resize_container .resize_table .resize_row td.resize_col1 {',
        'width: 140px;',
        'min-width: 140px;',
        '}',
        '.resize_container .resize_table .resize_row td.resize_col2 {',
        'width: 171px;',
        'min-width: 171px;',
        '}',
        '.resize_container .resize_table .resize_row td.resize_col3 {',
        'width: 117px;',
        'min-width: 117px;',
        '}',
        '.resize_container .resize_table .resize_row td.resize_col4 {',
        'width: 181px;',
        'min-width: 181px;',
        '}'
    ].join('');

    var styleElement = null;

    try {
        styleElement = document.createElement('style');
        styleElement.innerHTML = styleTemplate;
        document.body.appendChild(styleElement);
    } catch (e) {

    }

    if (!styleElement) {
        return;
    }

    // --- htmlTemplate

    var htmlTemplate = [
        '<div class="resize_container">',
        '<table class="resize_table">',
        '<tr class="resize_row">',
        '<td></td>',
        '<td class="resize_col4" id="left_resize_col4"></td>',
        '<td class="resize_col3" id="left_resize_col3"></td>',
        '<td class="resize_col2" id="left_resize_col2"></td>',
        '<td class="resize_col1" id="left_resize_col1"></td>',
        '<td class="resize_col0" id="center_resize_col0"></td>',
        '<td class="resize_col1" id="right_resize_col1"></td>',
        '<td class="resize_col2" id="right_resize_col2"></td>',
        '<td class="resize_col3" id="right_resize_col3"></td>',
        '<td class="resize_col4" id="right_resize_col4"></td>',
        '<td></td>',
        '</tr>',
        '</table>',
        '</div>'
    ].join('');

    var htmlElement = null;

    try {
        htmlElement = document.createElement('div');
        htmlElement.style['z-index'] = -1000;
        htmlElement.innerHTML = htmlTemplate;
        document.body.appendChild(htmlElement);
    } catch (e) {

    }

    if (!htmlElement) {
        return;
    }

    // --- Core

    var prefix = '';

    var imagesLeft = [prefix + 'left1.jpg', prefix + 'left2.jpg', prefix + 'left3.jpg', prefix + 'left4.jpg'];
    var imagesRight = [prefix + 'right1.jpg', prefix + 'right2.jpg', prefix + 'right3.jpg', prefix + 'right4.jpg'];
    var imageCenter = prefix + 'center.jpg';

    var sizes = [];
    sizes[0] = 760;
    sizes[1] = sizes[0] + 140 * 2;
    sizes[2] = sizes[1] + 171 * 2;
    sizes[3] = sizes[2] + 117 * 2;
    sizes[4] = sizes[3] + 181 * 2;

    var cols = [];

    try {
        cols[0] = document.getElementById('center_resize_col0');
        cols[1] = [document.getElementById('left_resize_col1'), document.getElementById('right_resize_col1')];
        cols[2] = [document.getElementById('left_resize_col2'), document.getElementById('right_resize_col2')];
        cols[3] = [document.getElementById('left_resize_col3'), document.getElementById('right_resize_col3')];
        cols[4] = [document.getElementById('left_resize_col4'), document.getElementById('right_resize_col4')];
    } catch (e) {

    }

    if (cols.length < 5) {
        return;
    }

    var added = [];
    added[0] = false;
    added[1] = false;
    added[2] = false;
    added[3] = false;
    added[4] = false;

    var oldWidth = 0;
    var oldHeight = 0;

    var check = function() {
        try {
            var newWidth = window.innerWidth;
            var newHeight = window.innerHeight;

            if (newWidth > oldWidth || newHeight > oldHeight) {
                for (var i = 0, count = cols.length; i < count; i++) {
                    if (i === 0) {
                        if (!added[i]) {
                            added[i] = true;
                            cols[i].style['backgroundImage'] = 'url(' + imageCenter + ')';
                        }
                    } else {
                        if (!added[i] && newWidth > sizes[i - 1]) {
                            added[i] = true;
                            cols[i][0].style['backgroundImage'] = 'url(' + imagesLeft[i - 1] + ')';
                            cols[i][1].style['backgroundImage'] = 'url(' + imagesRight[i - 1] + ')';
                        }
                    }
                }
                oldWidth = newWidth;
                oldHeight = newHeight;
            }
        } catch (e) {

        }
    };

    try {
        window.onresize = function(event) {
            check();
        };

        check();
    } catch (e) {

    }
})(window);