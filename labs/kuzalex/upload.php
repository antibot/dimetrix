<?php

function startsWith($haystack, $needle) {
    return !strncmp($haystack, $needle, strlen($needle));
}

function endsWith($haystack, $needle) {
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}

if(isset($_FILES) && is_array($_FILES) && isset($_FILES["file"]) && is_array($_FILES["file"])) {
    $file = $_FILES["file"];
    
    if($file["error"] == UPLOAD_ERR_OK) {
        $name = strtolower($file["name"]);
        $tmp_name = $file["tmp_name"];
        
        if(endsWith($name, ".mp3")) {
            if(move_uploaded_file($tmp_name, "songs/$name")) {
                echo json_encode(array("error" => 0, "file"=>"songs/$name"));
                
                return;
            }  
        }
    }
}  

echo json_encode(array("error" => 1));
    
?>