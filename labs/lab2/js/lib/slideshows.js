var IMAGES = [
    'images/photo 1.JPG',
    'images/photo 2.JPG',
    'images/photo 3.JPG',
    'images/photo 4.JPG',
    'images/photo 5.JPG'
];

var LOADED = {};

function GET_SLIDESHOWS(processing, transitions_base, transitions_images, transitions_waveform, pixels) {
    if (!processing || !transitions_base || !transitions_images || !transitions_waveform || !pixels) {
        return [];
    }

    $.each(IMAGES, function(index, url) {
        LOADED[url] = processing.loadImage(url);
    });

    var KenBurns = function() {
        var objects = [];
 
        $.each(IMAGES, function(index, url) {
            var object = {
                url: url,
                index: index,
                frame: 0,
                image: LOADED[url],
                action: false,
                r1: [],
                r2: [],
                scale: 2,
                display_time: 8000,
                fade_time: 2000,
                start_time: 0,
                update_time: 0
            };

            objects.push(object);
        });

        var index = 0;

        return {
            mx: 0,
            my: 0,
            impulsX: 0,
            impulsY: 0,
            impulsToX: 0,
            impulsToY: 0,
            change: function(from, to) {

            },
            pressed: function(mouseX, mouseY) {

            },
            released: function(mouseX, mouseY) {

            },
            update: function() {

            },
            frame: function(position) {
                if (position < 0) {
                    return;
                }

                var object = objects[position];
                var image = object.image;

                if (object.action) {
                    if (!object.frame) {
                        this.start();
                    }

                    var update_time = new Date().getTime() - object.start_time;

                    var alpha = 1;

                    var rect = INTERPOLATE_RECT(object.r1, object.r2, update_time / object.display_time);

                    if (update_time > (object.display_time - object.fade_time)) {
                        this.frame((index + 1) % objects.length);
                        if (update_time > object.display_time) {
                            object.action = false;

                            object.frame = 0;

                            alpha = 0;

                            var from = index;

                            index = (index + 1) % objects.length;

                            var to = index;

                            if (this.change) {
                                this.change.call(this, from, to);
                            }
                        } else {
                            alpha = Math.max(0, Math.min(1, 1 - (object.fade_time - (object.display_time - update_time)) / object.fade_time));
                        }
                    } else {

                    }

                    if (alpha >= 0) {
                        if (alpha < 1) {
                            processing.saveContext();
                            processing.globalAlpha(alpha);
                        }

                        processing.image(image, rect[0], rect[1], rect[2] - rect[0], rect[3] - rect[1]);

                        if (alpha < 1) {
                            processing.restoreContext();
                        }
                    }

                    object.frame++;
                } else {
                    if (image.width > 0 && image.height > 0) {
                        var size = PROPORTION_POINTS([image.width, image.height], [window.innerWidth, window.innerHeight], false);

                        var rect1 = [0, 0, size[0], size[1]];
                        var rect2 = SCALE_RECT(rect1, object.scale);

                        var align_x = Math.floor(Math.random() * 3) - 1;
                        var align_y = Math.floor(Math.random() * 3) - 1;

                        align_x /= 2;
                        align_y /= 2;

                        var x = rect2[0];
                        rect2[0] += x * align_x;
                        rect2[2] += x * align_x;

                        var y = rect2[1];
                        rect2[1] += y * align_y;
                        rect2[3] += y * align_y;

                        if (object.index % 2) {
                            object.r1 = rect1;
                            object.r2 = rect2;
                        }
                        else {
                            object.r1 = rect2;
                            object.r2 = rect1;
                        }

                        object.action = true;
                        object.start_time = new Date().getTime();
                    }
                }
            },
            draw: function() {
                this.frame(index);
            }
        };
    };

    return [new KenBurns()];
}