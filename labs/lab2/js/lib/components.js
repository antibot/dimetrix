function GET_COMPONENTS(processing, transitions_base, transitions_images, transitions_waveform, pixels) {
    if (!processing || !transitions_base || !transitions_images || !transitions_waveform || !pixels) {
        return [];
    }

    var Universe = function() {
        return {
            TYPE_NONE: 0,
            TYPE_CIRCLE: 1,
            TYPE_STAR: 2,
            TYPE_HEART: 3,
            type: 2,
            mx: 0,
            my: 0,
            impulsX: 0,
            impulsY: 0,
            impulsToX: 0,
            impulsToY: 0,
            startSlideshowTime: new Date(),
            startEffectsTime: new Date(),
            startEffectsWaveformTime: new Date(),
            indexesForBaseEffects: [0, 1, 3, 4, 6, 7],
            indexesForImagesEffects: [2, 5],
            indexesForWaveformEffects: FILL_ARRAY(8, 200),
            effectsIndex: 0,
            hasWaveform: function() {
                var audioVisualizator = window.audioVisualizator || {};
                var waveform = "getWaveform" in audioVisualizator ? (audioVisualizator.getWaveform() || []) : [];
                return !IS_NULL_ARRAY(waveform);
            },
            start: function() {
                this.startSlideshowTime = new Date();

                if (this.hasWaveform() && this.indexesForWaveformEffects.indexOf(this.effectsIndex) !== -1) {

                } else {
                    //if (transitions_base[0]()) {
                    //this.impulsX = 0;
                    //this.impulsY = 0;
                    //}
                }
            },
            change: function(from, to) {
                this.startSlideshowTime = new Date();

                if (this.hasWaveform() && this.indexesForWaveformEffects.indexOf(this.effectsIndex) !== -1) {

                } else {
                    if (transitions_base[0]()) {
                        this.impulsX = 0;
                        this.impulsY = 0;
                    }
                }
            },
            pressed: function(mouseX, mouseY) {
                var count = pixels.length, i;

                var part = Math.min(window.innerWidth, window.innerHeight) / 4;
                var r = Math.floor(Math.random() * part + part);

                for (i = 0; i < count; i++) {
                    var pixel = pixels[i];

                    if (pixel.flightMode === 1) {
                        pixel.toX = Math.cos(i * 3.6 * Math.PI / 180) * r + mouseX;
                        pixel.toY = Math.sin(i * 3.6 * Math.PI / 180) * r + mouseY;
                        pixel.speedX = Math.cos(pixel.angle) * Math.random() * 3;
                        pixel.speedY = Math.sin(pixel.angle) * Math.random() * 3;
                        pixel.toSize = Math.random() * 10 + 1;
                        pixel.toR = Math.random() * 255;
                        pixel.toG = Math.random() * 255;
                        pixel.toB = Math.random() * 255;
                    }
                }
            },
            released: function(mouseX, mouseY) {

            },
            update: function() {
                var count = pixels.length, i;

                this.impulsX = this.impulsX + (this.impulsToX - this.impulsX) / 30;
                this.impulsY = this.impulsY + (this.impulsToY - this.impulsY) / 30;

                // move to tox
                for (i = 0; i < count; i++) {
                    var pixel = pixels[i];
                    pixel.x = pixel.x + (pixel.toX - pixel.x) / 10;
                    pixel.y = pixel.y + (pixel.toY - pixel.y) / 10;
                    pixel.size = pixel.size + (pixel.toSize - pixel.size) / 10;

                    pixel.r = pixel.r + (pixel.toR - pixel.r) / 10;
                    pixel.g = pixel.g + (pixel.toG - pixel.g) / 10;
                    pixel.b = pixel.b + (pixel.toB - pixel.b) / 10;
                }

                // update speed
                for (i = 0; i < count; i++) {
                    var pixel = pixels[i];
                    // check for flightmode
                    var a = Math.abs(pixel.toX - this.mx) * Math.abs(pixel.toX - this.mx);
                    var b = Math.abs(pixel.toY - this.my) * Math.abs(pixel.toY - this.my);
                    var c = Math.sqrt(a + b);

                    if (pixel.flightMode !== 2) {
                        if (c < 120) {
                            if (pixel.flightMode === 0) {
                                var alpha = Math.atan2(pixel.y - this.my, pixel.x - this.mx) * 180 / Math.PI + Math.random() * 180 - 90;
                                pixel.degree = alpha;
                                pixel.degreeSpeed = Math.random() * 1 + 0.5;
                                pixel.frame = 0;
                            }
                            pixel.flightMode = 1;
                        } else {
                            pixel.flightMode = 0;
                        }
                    }

                    // random movement
                    if (pixel.flightMode === 0) {
                        // change position
                        pixel.toX += pixel.speedX;
                        pixel.toY += pixel.speedY;

                        // check for bounds
                        if (pixel.x < 0) {
                            pixel.x = window.innerWidth;
                            pixel.toX = window.innerWidth;
                        }
                        if (pixel.x > window.innerWidth) {
                            pixel.x = 0;
                            pixel.toX = 0;
                        }

                        if (pixel.y < 0) {
                            pixel.y = window.innerHeight;
                            pixel.toY = window.innerHeight;
                        }
                        if (pixel.y > window.innerHeight) {
                            pixel.y = 0;
                            pixel.toY = 0;
                        }
                    }

                    // seek mouse
                    if (pixel.flightMode === 1) {
                        pixel.toX = this.mx + Math.cos((pixel.degree + pixel.frame) % 360 * Math.PI / 180) * c;
                        pixel.toY = this.my + Math.sin((pixel.degree + pixel.frame) % 360 * Math.PI / 180) * c;
                        pixel.frame += pixel.degreeSpeed;
                        pixel.degreeSpeed += 0.01;
                    }

                    if (pixel.flightMode !== 2) {
                        // add impuls
                        pixel.toX += Math.floor(this.impulsX * pixel.size / 30);
                        pixel.toY += Math.floor(this.impulsY * pixel.size / 30);
                    }
                }

                // set an choord
                var r1 = Math.floor(Math.random() * pixels.length);
                var r2 = Math.floor(Math.random() * pixels.length);

                if (pixels[r1].flightMode !== 2) {
                    pixels[r1].size = Math.random() * 30;

                }
                if (pixels[r2].flightMode !== 2) {
                    pixels[r2].size = Math.random() * 30;
                }

                if (this.hasWaveform() && this.indexesForWaveformEffects.indexOf(this.effectsIndex) !== -1) {
                    this.type = this.TYPE_CIRCLE;

                    if (new Date().getTime() - this.startEffectsWaveformTime.getTime() > (this.effectsIndex === this.indexesForWaveformEffects[0] ? 2500 : 50)) {
                        this.startEffectsWaveformTime = new Date();

                        //console.log(waveform);

                        if (transitions_waveform[Math.floor(Math.random() * transitions_waveform.length)]()) {
                            this.impulsX = 0;
                            this.impulsY = 0;
                        }

                        this.startEffectsTime = new Date();

                        this.effectsIndex++;
                    }
                } else {
                    if (new Date().getTime() - this.startSlideshowTime.getTime() > 4000) {
                        this.startSlideshowTime = new Date();

                        if (new Date().getTime() - this.startEffectsTime.getTime() > 2000) {
                            this.type = this.TYPE_STAR;

                            this.effectsIndex = (this.effectsIndex > (this.indexesForBaseEffects.length + this.indexesForImagesEffects.length + this.indexesForWaveformEffects.length) ? 0 : this.effectsIndex);

                            this.startEffectsTime = new Date();

                            this.effectsIndex = (this.hasWaveform() ? this.effectsIndex : ((this.effectsIndex > this.indexesForBaseEffects.length && this.effectsIndex > this.indexesForImagesEffects.length) ? 0 : this.effectsIndex));

                            this.impulsX = Math.random() * 800 - 400;
                            this.impulsY = -Math.random() * 400;

                            if (this.indexesForBaseEffects.indexOf(this.effectsIndex) !== -1) {
                                if (transitions_base[Math.floor(Math.random() * transitions_base.length)]()) {
                                    this.impulsX = 0;
                                    this.impulsY = 0;
                                }
                            } else if (this.indexesForImagesEffects.indexOf(this.effectsIndex) !== -1) {
                                if (transitions_images[Math.floor(Math.random() * transitions_images.length)]()) {
                                    this.impulsX = 0;
                                    this.impulsY = 0;
                                }
                            }

                            this.startEffectsWaveformTime = new Date();

                            this.effectsIndex++;
                        }
                    }
                }

                this.effectsIndex = (this.effectsIndex > (this.indexesForBaseEffects.length + this.indexesForImagesEffects.length + this.indexesForWaveformEffects.length) ? 0 : this.effectsIndex);
            },
            draw: function() {
                var count = pixels.length, i;
                for (i = 0; i < count; i++) {
                    var pixel = pixels[i];

                    processing.fill(pixel.r, pixel.g, pixel.b);

                    // ---

                    switch (this.type) {
                        case this.TYPE_NONE:
                            break;
                        case this.TYPE_CIRCLE:
                            processing.ellipse(pixel.x, pixel.y, pixel.size, pixel.size);
                            break;
                        case this.TYPE_STAR: 
                            processing.beginShape();

                            var j;
                            var angleIncrement = Math.PI / 5;
                            var ninety = Math.PI * .5;

                            for (j = 0; j <= 10; j++) {
                                var radius = (j % 2 > 0 ? pixel.size : pixel.size * .5);
                                var px = Math.cos(pixel.angle + ninety + angleIncrement * j) * radius + pixel.angle;
                                var py = Math.sin(pixel.angle + ninety + angleIncrement * j) * radius + pixel.angle;
                                processing.vertex(px + pixel.x, py + pixel.y);
                            }

                            processing.endShape(processing.CLOSE);
                            break;
                        case this.TYPE_HEART:
                            //processing.smooth();
                            processing.beginShape();
                            processing.vertex(50, 15);
                            processing.bezierVertex(50, -5, 90, 5, 50, 40);
                            processing.vertex(50, 15);
                            processing.bezierVertex(50, -5, 10, 5, 50, 40);
                            processing.endShape(processing.CLOSE);
                            break;
                        default:

                            break;
                    }
                }
            }
        };
    };

    return [new Universe()];
}