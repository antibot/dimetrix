jQuery(function($) {

    window.scrollTo(0, 1);

    // ---

    //alert("Dancer.isSupported = " + Dancer.isSupported() + "!");
    //alert("Float32Array = " + (window.Float32Array ? 1 : 0) + " Uint32Array = " + (window.Uint32Array ? 1 : 0) + "!");
    //alert("AudioContext = " + (window.AudioContext ? 1 : 0) + " webkitAudioContext = " + (window.webkitAudioContext ? 1 : 0) + "!");

    // ---

    var PROCESSING = new Processing("graphics");
   
    // ---

    var pixels = GET_PIXELS(160);

    var transitions_base = GET_TRANSITIONS_BASE(pixels);
    var transitions_images = GET_TRANSITIONS_IMAGES(pixels);
    var transitions_waveform = GET_TRANSITIONS_WAVEFORM(pixels);

    var components = GET_COMPONENTS(PROCESSING, transitions_base, transitions_images, transitions_waveform, pixels);
    var slideshows = GET_SLIDESHOWS(PROCESSING, transitions_base, transitions_images, transitions_waveform, pixels);

    console.log("pixels", pixels.length);
    console.log("PROCESSING", PROCESSING);
 
    // ---

    $(window).resize(function() {
        PROCESSING.size(window.innerWidth, window.innerHeight);
    });

    PROCESSING.size(window.innerWidth, window.innerHeight);
    PROCESSING.background(0);
    PROCESSING.noStroke();
    PROCESSING.frameRate(60);
    PROCESSING.fill(0, 0, 0);
    PROCESSING.background(0);

    PROCESSING.mouseMoved = function() {
        var countComponents = components.length, countSlideshows = slideshows.length, i;

        for (i = 0; i < countComponents; i++) {
            components[i].mx = PROCESSING.mouseX;
            components[i].my = PROCESSING.mouseY;
        }

        for (i = 0; i < countSlideshows; i++) {
            slideshows[i].mx = PROCESSING.mouseX;
            slideshows[i].my = PROCESSING.mouseY;
        }
    };

    PROCESSING.mousePressed = function() {
        var countComponents = components.length, countSlideshows = slideshows.length, i;

        for (i = 0; i < countComponents; i++) {
            components[i].pressed(PROCESSING.mouseX, PROCESSING.mouseY);
        }

        for (i = 0; i < countSlideshows; i++) {
            slideshows[i].pressed(PROCESSING.mouseX, PROCESSING.mouseY);
        }
    };

    PROCESSING.mouseReleased = function() {
        var countComponents = components.length, countSlideshows = slideshows.length, i;

        for (i = 0; i < countComponents; i++) {
            components[i].released(PROCESSING.mouseX, PROCESSING.mouseY);
        }

        for (i = 0; i < countSlideshows; i++) {
            slideshows[i].released(PROCESSING.mouseX, PROCESSING.mouseY);
        }
    };

    PROCESSING.draw = function() {
        var countComponents = components.length, countSlideshows = slideshows.length, i;

        for (i = 0; i < countSlideshows; i++) {
            slideshows[i].update();
        }

        for (i = 0; i < countComponents; i++) {
            components[i].update();
        }

        PROCESSING.background(0);

        for (i = 0; i < countSlideshows; i++) {
            slideshows[i].draw();
        }

        for (i = 0; i < countComponents; i++) {
            components[i].draw();
        }
    };

    for (var i = 0; i < slideshows.length; i++) {
        slideshows[i].change = function(from, to) {
            for (var j = 0; j < slideshows.length; j++) {
                components[j].change(from, to);
            }
        };
        slideshows[i].start = function() {
            for (var j = 0; j < slideshows.length; j++) {
                components[j].start();
            }
        };
    }

    PROCESSING.loop();
});